// ----------------------------------------------------------------------------
// Custom.js - ngPaws
// Copyright (C) 2022 teksait
// ----------------------------------------------------------------------------

function refreshScreen() {
	document.getElementById('SBL').innerHTML = getMessageText(1100);
	document.getElementById('SBR').innerHTML = getMessageText(1101);

	const blocks = [
		sidebarTitle(getMessageText(1104)) + tasks() + STR_NEWLINE,
		/*sidebarTitle(getMessageText(1104)) + conditionsX() + STR_NEWLINE,*/
		sidebarTitle(getMessageText(1102)) + invenX() + STR_NEWLINE,
		sidebarTitle(getMessageText(1103)) + exitsX() + STR_NEWLINE,
		sidebarTitle(getMessageText(2000)) + getMessageText(2001) + STR_NEWLINE
	];

	if (getFlag(FLAG_LOCATION) >= getFlag(68)) {
		for (i = 0; i < blocks.length; i++) {
			if (i == 0) {
				document.getElementById('xtext').innerHTML = blocks[i];
			} else {
				document.getElementById('xtext').innerHTML = document.getElementById('xtext').innerHTML + blocks[i];
			}
		}
	}
	else {
		document.getElementById('xtext').innerHTML = '<span class="loc">' + getMessageText(3000) + '</span>' + STR_NEWLINE + getMessageText(3001);
	}

	if (getFlag(FLAG_LOCATION) < getFlag(68)) {
		document.getElementById('text').style.width = '100%';
		document.getElementById('text').style.textAlign = 'center';
		document.getElementById('xtext').style.display = 'none';
		document.getElementById('statusbar').style.display = 'none';
	} else {
		document.getElementById('text').style.width = '75%';
		document.getElementById('text').style.textAlign = 'left';
		document.getElementById('xtext').style.display = 'block';
		document.getElementById('statusbar').style.display = 'block';
	}
}

function sidebarTitle(text) {
	return '<span class="loc">' + text + '</span>' + STR_NEWLINE;
}

function exitsX() {
	var vExits = '';
	if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent())) {
		var exitcount = 0;
		for (i = 0; i < NUM_CONNECTION_VERBS; i++) if (getConnection(getFlag(38), i) != -1) exitcount++;
		if (exitcount) {
			var exitcountprogress = 0;
			for (i = 0; i < NUM_CONNECTION_VERBS; i++) if (getConnection(getFlag(38), i) != -1) {
				exitcountprogress++;
				vExits = vExits + filterText(messages[1000 + 2 + i]) + STR_NEWLINE;
			}
		} else vExits = vExits + filterText(messages[1000 + 1]);
	} else vExits = vExits + filterText(messages[1000 + 1]);
	return vExits;
}

function invenX() {
	var vText = '';
	var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS), 3);
	var i;
	var count = 0;
	for (i = 0; i < num_objects; i++) {
		if (objectsLocation[i] == LOCATION_CARRIED) {
			if ((listnpcs_with_objects) || (!objectIsNPC(i))) {
				vText = vText + filterText(objects[i]);
				//if (i === 4) { vText = vText + ' ['+ reloj(4)+']'; }
				vText = vText + STR_NEWLINE;
				count++;
			}
		}
		if (objectsLocation[i] == LOCATION_WORN) {
			vText = vText + filterText(objects[i]) + ' ' + filterText(sysmessages[SYSMESS_WORN]) + STR_NEWLINE;
			count++;
		}
	}
	if (!count) {
		vText = vText + filterText(sysmessages[SYSMESS_CARRYING_NOTHING]) + STR_NEWLINE;
	}
	return vText;
}

function invenImageX() {
	var invenHTML = '<div class="invenGrid">';
	var emptyInven = getFlag(37);
	for (i = 0; i < num_objects; i++) {
		if (objectsLocation[i] == LOCATION_CARRIED || objectsLocation[i] == LOCATION_WORN) {
			var oText = filterText(objects[i]);
			if (objectsLocation[i] == LOCATION_WORN) oText = filterText(objects[i]) + ' ' + filterText(sysmessages[SYSMESS_WORN]);
			invenHTML = invenHTML + '<div class="invenItem tooltip-container">';
			invenHTML = invenHTML + '<img class="tooltip-img" src="dat/obj' + i + '.png" />';
			invenHTML = invenHTML + '<span class="tooltip-text">' + oText + '</span>';
			invenHTML = invenHTML + '</div>';
			emptyInven--;
		}
	}
	for (i = 0; i < emptyInven; i++) {
		invenHTML = invenHTML + '<div class="invenItem"><img src="dat/objEmpty.png" /></div>';
	}
	invenHTML = invenHTML + '</div>';
	return invenHTML;
}

function examineItem(i) {
	setInputPlaceHolder();
	player_order = 'x ' + objects[i];
	if (player_order.charAt(0) == '#') {
		addToTranscript(player_order + STR_NEWLINE);
		clearInputWindow();
	}
	else
		if (player_order != '')
			orderEnteredLoop(player_order);
	focusInput();
}

function conditionsX() {
	var invenHTML = '<div class="conditionGrid">';
	invenHTML = invenHTML + '<div class="conditionItem bghealth"><span class="conditionText">' + getMessageText(1105) + '</span><span class="conditionValue">' + getFlag(70) + '</span></div>';
	invenHTML = invenHTML + '<div class="conditionItem bgsanity"><span class="conditionText">' + getMessageText(1106) + '</span><span class="conditionValue">' + getFlag(71) + '</span></div>';
	invenHTML = invenHTML + '</div>';

	return invenHTML;
}

function showOverlay(text, action, align = 'center', width) {
	var overlay = document.getElementById('overlay');
	var overlayContent = document.getElementById('overlayContent');
	var overlayText = document.getElementById('overlayText');
	var overlayAction = document.getElementById('overlayAction');

	var flexAlign = 'center';
	switch (align) {
		case 'left':
			flexAlign = 'flex-start';
			break;
		case 'right':
			flexAlign = 'flex-end';
			break;
		default:
			flexAlign = 'center';
			break;
	}

	overlayText.textContent = text;
	if (action == null) {
		overlayAction.style.display = 'none';
	} else {
		overlayAction.textContent = action;
	}
	overlay.style.display = 'block';
	overlayContent.style.width = width + 'px';
	overlayContent.style.alignItems = flexAlign;
	overlayContent.style.textAlign = align;
}

function showOverlayDialog(width, intro, a, b, c, d, e, f, g, h, i) {
	var overlay = document.getElementById('overlay');
	overlay.style.display = 'block';

	var overlayContent = document.getElementById('overlayContent');
	overlayContent.style.width = width + 'px';
	overlayContent.style.alignItems = 'flex-start';
	overlayContent.style.textAlign = 'left';

	var overlayText = document.getElementById('overlayText');
	if (intro != undefined) overlayText.innerHTML = '<span>' + intro + '</span><br /><br />';
	if (a != undefined) overlayText.innerHTML += '<span>' + a + '</span><br />';
	if (b != undefined) overlayText.innerHTML += '<span>' + b + '</span><br />';
	if (c != undefined) overlayText.innerHTML += '<span>' + c + '</span><br />';
	if (d != undefined) overlayText.innerHTML += '<span>' + d + '</span><br />';
	if (e != undefined) overlayText.innerHTML += '<span>' + e + '</span><br />';
	if (f != undefined) overlayText.innerHTML += '<span>' + f + '</span><br />';
	if (g != undefined) overlayText.innerHTML += '<span>' + g + '</span><br />';
	if (h != undefined) overlayText.innerHTML += '<span>' + h + '</span><br />';
	if (i != undefined) overlayText.innerHTML += '<span>' + i + '</span><br />';

	var overlayAction = document.getElementById('overlayAction');
	overlayAction.style.display = 'none';
}

function closeOverlay() {
	var overlay = document.getElementById('overlay');
	overlay.style.display = 'none';
	focusInput();
}

function showNotification(text, delay, speed) {
	var notification = document.getElementById('notification');
	var originalPosition = notification.style.top;
	notification.textContent = text;
	notification.style.top = '10px';
	notification.style.transition = 'top ' + speed + 's ease-in-out';
	setTimeout(() => {
		notification.style.top = originalPosition;
	}, delay);
}

function tasks() {
	var tasksText = '';
	for (i = 101; i < 200; i++) {
		if (getFlag(i) == 1) {
			var taskText = i + 4000;
			tasksText = tasksText + '<span>' + getMessageText(taskText) + '</span>' + STR_NEWLINE;
		}
	}
	return tasksText;
}