// This file is (C) Carlos Sanchez 2014, released under the MIT license


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES AND CONSTANTS ///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// CONSTANTS 
VOCABULARY_ID = 0;
VOCABULARY_WORD = 1;
VOCABULARY_TYPE = 2;

WORDTYPE_VERB = 0;
WORDTYPE_NOUN = 1
WORDTYPE_ADJECT = 2;
WORDTYPE_ADVERB = 3;
WORDTYPE_PRONOUN = 4;
WORDTYPE_CONJUNCTION = 5;
WORDTYPE_PREPOSITION = 6;

TIMER_MILLISECONDS  = 40;

RESOURCE_TYPE_IMG = 1;
RESOURCE_TYPE_SND = 2;

PROCESS_RESPONSE = 0;
PROCESS_DESCRIPTION = 1;
PROCESS_TURN = 2;

DIV_TEXT_SCROLL_STEP = 40;


// Aux
SET_VALUE = 255; // Value assigned by SET condact
EMPTY_WORD = 255; // Value for word types when no match is found (as for  sentences without adjective or name)
MAX_WORD_LENGHT = 10;  // Number of characters considered per word
FLAG_COUNT = 256;  // Number of flags
NUM_CONNECTION_VERBS = 14; // Number of verbs used as connection, from 0 to N - 1
NUM_CONVERTIBLE_NOUNS = 20;
NUM_PROPER_NOUNS = 50; // Number of proper nouns, can't be used as pronoun reference
EMPTY_OBJECT = 255; // To remark there is no object when the action requires a objno parameter
NO_EXIT = 255;  // If an exit does not exist, its value is this value
MAX_CHANNELS = 17; // Number of SFX channels
RESOURCES_DIR='dat/';


//Attributes
ATTR_LIGHT=0;	
ATTR_WEARABLE=1;	
ATTR_CONTAINER=2; 
ATTR_NPC=3;           
ATTR_CONCEALED=4;
ATTR_EDIBLE=5;   
ATTR_DRINKABLE=6;
ATTR_ENTERABLE=7;
ATTR_FEMALE=8;
ATTR_LOCKABLE=9;
ATTR_LOCKED=10;
ATTR_MALE=11;
ATTR_NEUTER=12;
ATTR_OPENABLE=13;
ATTR_OPEN=14;
ATTR_PLURALNAME=15;
ATTR_TRANSPARENT=16;
ATTR_SCENERY=17;
ATTR_SUPPORTER=18;
ATTR_SWITCHABLE=19;
ATTR_ON=20;
ATTR_STATIC=21;
ATTR_VOCAL=22;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// INTERNAL STRINGS ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General messages & strings
STR_PROMPT_SYMBOL = '&gt;';
STR_NEWLINE = '<br />';
STR_PROMPT_START = '<br /><span class="feedback">' + STR_PROMPT_SYMBOL + ' ';
STR_PROMPT_END = '</span>';
STR_RAMSAVE_FILENAME = 'RAMSAVE_SAVEGAME';



// Runtime error messages
STR_WRONG_SYSMESS = 'ATENCIÓ: El missatge del sistema no existeix.'; 
STR_WRONG_LOCATION = 'ATENCIÓ: La localització no existeix.'; 
STR_WRONG_MESSAGE = 'ATENCIÓ: El missatge no existeix.'; 
STR_WRONG_PROCESS = 'ATENCIÓ: El procés no existeix.' 
STR_RAMLOAD_ERROR= 'ATENCIÓ: No pots restaurar una partida que no s\'ha gravat.'; 
STR_RUNTIME_VERSION  = 'ngPAWS runtime (C) 2014 Carlos Sanchez. Sota {URL|http://www.opensource.org/licenses/MIT| llicència MIT}.\nAdaptació ngPAWS Català (C) @teksait.\nBuzz Sound library (C) Jay Salvat. Sota {URL|http://www.opensource.org/licenses/MIT| llicència MIT.} \njQuery (C) jQuery Foundation. Sota {URL|https://jquery.org/license/| llicència MIT}.';
STR_TRANSCRIPT = 'Per copiar la transcripció al portapapers, prem CTRL+C i Enter.';

STR_INVALID_TAG_SEQUENCE = 'Tag de seqüència invàlid: ';
STR_INVALID_TAG_SEQUENCE_EMPTY = 'Tag de seqüencia invàlid.';
STR_INVALID_TAG_SEQUENCE_BADPARAMS = 'Tag de seqüència invàlid: Paràmetres incorrectes.';
STR_INVALID_TAG_SEQUENCE_BADTAG = 'Tag de seqüència invàlid: Tag desconegut.';
STR_BADIE = 'You are using a very old version of Internet Explorer. Some features of this product won\'t be avaliable, and other may not work properly. For a better experience please upgrade your browser or install some other one like Firefox, Chrome or Opera.\n\nIt\'s up to you to continue but be warned your experience may be affected.';
STR_INVALID_OBJECT = 'WARNING: Trying to access object that does not exist'


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////     FLAGS     ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


FLAG_LIGHT = 0;
FLAG_OBJECTS_CARRIED_COUNT = 1;
FLAG_AUTODEC2 = 2; 
FLAG_AUTODEC3 = 3;
FLAG_AUTODEC4 = 4;
FLAG_AUTODEC5 = 5;
FLAG_AUTODEC6 = 6;
FLAG_AUTODEC7 = 7;
FLAG_AUTODEC8 = 8;
FLAG_AUTODEC9 = 9;
FLAG_AUTODEC10 = 10;
FLAG_ESCAPE = 11;
FLAG_PARSER_SETTINGS = 12;
FLAG_PICTURE_SETTINGS = 29
FLAG_SCORE = 30;
FLAG_TURNS_LOW = 31;
FLAG_TURNS_HIGH = 32;
FLAG_VERB = 33;
FLAG_NOUN1 =34;
FLAG_ADJECT1 = 35;
FLAG_ADVERB = 36;
FLAG_MAXOBJECTS_CARRIED = 37;
FLAG_LOCATION = 38;
FLAG_TOPLINE = 39;   // deprecated
FLAG_MODE = 40;  // deprecated
FLAG_PROTECT = 41;   // deprecated
FLAG_PROMPT = 42; 
FLAG_PREP = 43;
FLAG_NOUN2 = 44;
FLAG_ADJECT2 = 45;
FLAG_PRONOUN = 46;
FLAG_PRONOUN_ADJECT = 47;
FLAG_TIMEOUT_LENGTH = 48;
FLAG_TIMEOUT_SETTINGS = 49; 
FLAG_DOALL_LOC = 50;
FLAG_REFERRED_OBJECT = 51;
FLAG_MAXWEIGHT_CARRIED = 52;
FLAG_OBJECT_LIST_FORMAT = 53;
FLAG_REFERRED_OBJECT_LOCATION = 54;
FLAG_REFERRED_OBJECT_WEIGHT = 55;
FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES = 56;
FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES = 57;
FLAG_EXPANSION1 = 58;
FLAG_EXPANSION2 = 59;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// SPECIAL LOCATIONS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

LOCATION_WORN = 253;
LOCATION_CARRIED = 254;
LOCATION_NONCREATED = 252;
LOCATION_HERE = 255;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////  SYSTEM MESSAGES  ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



SYSMESS_ISDARK = 0;
SYSMESS_YOUCANSEE = 1;
SYSMESS_PROMPT0 = 2;
SYSMESS_PROMPT1 = 3;
SYSMESS_PROMPT2 = 4
SYSMESS_PROMPT3= 5;
SYSMESS_IDONTUNDERSTAND = 6;
SYSMESS_WRONGDIRECTION = 7
SYSMESS_CANTDOTHAT = 8;
SYSMESS_YOUARECARRYING = 9;
SYSMESS_WORN = 10;
SYSMESS_CARRYING_NOTHING = 11;
SYSMESS_AREYOUSURE = 12;
SYSMESS_PLAYAGAIN = 13;
SYSMESS_FAREWELL = 14;
SYSMESS_OK = 15;
SYSMESS_PRESSANYKEY = 16;
SYSMESS_TURNS_START = 17;
SYSMESS_TURNS_CONTINUE = 18;
SYSMESS_TURNS_PLURAL = 19;
SYSMESS_TURNS_END = 20;
SYSMESS_SCORE_START= 21;
SYSMESS_SCORE_END =22;
SYSMESS_YOURENOTWEARINGTHAT = 23;
SYSMESS_YOUAREALREADYWEARINGTHAT = 24;
SYSMESS_YOUALREADYHAVEOBJECT = 25;
SYSMESS_CANTSEETHAT = 26;
SYSMESS_CANTCARRYANYMORE = 27;
SYSMESS_YOUDONTHAVETHAT = 28;
SYSMESS_YOUAREALREADYWAERINGOBJECT = 29;
SYSMESS_YES = 30;
SYSMESS_NO = 31;
SYSMESS_MORE = 32;
SYSMESS_CARET = 33;
SYSMESS_TIMEOUT=35;
SYSMESS_YOUTAKEOBJECT = 36;
SYSMESS_YOUWEAROBJECT = 37;
SYSMESS_YOUREMOVEOBJECT = 38;
SYSMESS_YOUDROPOBJECT = 39;
SYSMESS_YOUCANTWEAROBJECT = 40;
SYSMESS_YOUCANTREMOVEOBJECT = 41;
SYSMESS_CANTREMOVE_TOOMANYOBJECTS = 42;
SYSMESS_WEIGHSTOOMUCH = 43;
SYSMESS_YOUPUTOBJECTIN = 44;
SYSMESS_YOUCANTTAKEOBJECTOUTOF = 45;
SYSMESS_LISTSEPARATOR = 46;
SYSMESS_LISTLASTSEPARATOR = 47;
SYSMESS_LISTEND = 48;
SYSMESS_YOUDONTHAVEOBJECT = 49;
SYSMESS_YOUARENOTWEARINGOBJECT = 50;
SYSMESS_PUTINTAKEOUTTERMINATION = 51;
SYSMESS_THATISNOTIN = 52;
SYSMESS_EMPTYOBJECTLIST = 53;
SYSMESS_FILENOTFOUND = 54;
SYSMESS_CORRUPTFILE = 55;
SYSMESS_IOFAILURE = 56;
SYSMESS_DIRECTORYFULL = 57;
SYSMESS_LOADFILE = 58;
SYSMESS_FILENOTFOUND = 59;
SYSMESS_SAVEFILE = 60;
SYSMESS_SORRY = 61;
SYSMESS_NONSENSE_SENTENCE = 62;
SYSMESS_NPCLISTSTART = 63;
SYSMESS_NPCLISTCONTINUE = 64;
SYSMESS_NPCLISTCONTINUE_PLURAL = 65;
SYSMESS_INSIDE_YOUCANSEE = 66;
SYSMESS_OVER_YOUCANSEE = 67;
SYSMESS_YOUPUTOBJECTON = 68;
SYSMESS_YOUCANTTAKEOBJECTFROM = 69;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// GLOBAL VARS //////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Parser vars
var last_player_orders = [];   // Store last player orders, to be able to restore it when pressing arrow up
var last_player_orders_pointer = 0;
var parser_word_found;
var player_order_buffer = '';
var player_order = ''; // Current player order
var previous_verb = EMPTY_WORD;
var previous_noun = EMPTY_WORD;
var previous_adject = EMPTY_WORD;
var pronoun_suffixes = [];


//Settings
var graphicsON = true; 
var soundsON = true; 
var interruptDisabled = false;
var showWarnings = true;

// waitkey commands callback function
var waitkey_callback_function = [];

//PAUSE
var inPause=false;
var pauseRemainingTime = 0;



// Transcript
var inTranscript = false;
var transcript = '';


// Block
var inBlock = false;
var unblock_process = null;


// END
var inEND = false;

//QUIT
var inQUIT = false;

//ANYKEY
var inAnykey = false;

//GETKEY
var inGetkey = false;
var getkey_return_flag = null;

// Status flags
var done_flag;
var describe_location_flag;
var in_response;
var success;

// doall control
var doall_flag;
var process_in_doall;
var entry_for_doall	= '';
var current_process;


var timeout_progress = 0;
var ramsave_value = null;
var num_objects;


// The flags
var flags = new Array();


// The sound channels
var soundChannels = [];
var soundLoopCount = [];

//The last free object attribute
var nextFreeAttr = 22;

//Autocomplete array
var autocomplete = new Array();
var autocompleteStep = 0;
var autocompleteBaseWord = '';
// PROCESSES

interruptProcessExists = false;

function pro000()
{
process_restart=true;
pro000_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p000e0000:
	{
 		if (skipdoall('p000e0000')) break p000e0000;
 		ACChook(0);
		if (done_flag) break pro000_restart;
		{}

	}

	// OPCIONES _
	p000e0001:
	{
 		if (skipdoall('p000e0001')) break p000e0001;
 		if (in_response)
		{
			if (!CNDverb(16)) break p000e0001;
 		}
 		ACCminus(31,1);
 		ACClet(98,100);
 		ACCprocess(3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CREDITOS _
	p000e0002:
	{
 		if (skipdoall('p000e0002')) break p000e0002;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0002;
 		}
 		ACCminus(31,1);
 		ACCgoto(2);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// AYUDA _
	p000e0003:
	{
 		if (skipdoall('p000e0003')) break p000e0003;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0003;
 		}
 		ACCminus(31,1);
 		ACCgoto(3);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// VOLVER _
	p000e0004:
	{
 		if (skipdoall('p000e0004')) break p000e0004;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0004;
 		}
		if (!CNDle(38,10)) break p000e0004;
 		ACCminus(31,1);
 		ACCgoto(getFlag(62));
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0005:
	{
 		if (skipdoall('p000e0005')) break p000e0005;
		if (!CNDle(38,20)) break p000e0005;
 		ACCwriteln(1);
 		ACCminus(31,1);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COGE TODO
	p000e0006:
	{
 		if (skipdoall('p000e0006')) break p000e0006;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0006;
			if (!CNDnoun1(20)) break p000e0006;
 		}
		if (!CNDprep(3)) break p000e0006;
 		ACCsynonym(75,255);
		{}

	}

	// PON TODO
	p000e0007:
	{
 		if (skipdoall('p000e0007')) break p000e0007;
 		if (in_response)
		{
			if (!CNDverb(71)) break p000e0007;
			if (!CNDnoun1(20)) break p000e0007;
 		}
		if (!CNDprep(4)) break p000e0007;
 		ACCsynonym(74,255);
		{}

	}

	// COGE TODO
	p000e0008:
	{
 		if (skipdoall('p000e0008')) break p000e0008;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0008;
			if (!CNDnoun1(20)) break p000e0008;
 		}
		if (!CNDisnotlight()) break p000e0008;
 		ACCwriteln(2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COGE TODO
	p000e0009:
	{
 		if (skipdoall('p000e0009')) break p000e0009;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0009;
			if (!CNDnoun1(20)) break p000e0009;
 		}
 		ACCobjat(getFlag(38),13);
		if (!CNDzero(13)) break p000e0009;
 		ACCwriteln(3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COGE TODO
	p000e0010:
	{
 		if (skipdoall('p000e0010')) break p000e0010;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0010;
			if (!CNDnoun1(20)) break p000e0010;
 		}
		entry_for_doall = 'p000e0011';
		process_in_doall = 0;
 		ACCdoall(255);
		break pro000_restart;
		{}

	}

	// DEJA TODO
	p000e0011:
	{
 		if (skipdoall('p000e0011')) break p000e0011;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0011;
			if (!CNDnoun1(20)) break p000e0011;
 		}
 		ACCobjat(254,13);
		if (!CNDzero(13)) break p000e0011;
 		ACCwriteln(4);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA TODO
	p000e0012:
	{
 		if (skipdoall('p000e0012')) break p000e0012;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0012;
			if (!CNDnoun1(20)) break p000e0012;
 		}
		entry_for_doall = 'p000e0013';
		process_in_doall = 0;
 		ACCdoall(254);
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0013:
	{
 		if (skipdoall('p000e0013')) break p000e0013;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0013;
 		}
		if (!CNDnoteq(51,255)) break p000e0013;
		if (!CNDpresent(getFlag(51))) break p000e0013;
		if (!CNDonotzero(getFlag(51),2)) break p000e0013;
 		ACCcopyff(34,44);
 		ACCcopyff(35,45);
 		ACCsynonym(75,20);
 		ACClet(43,3);
		{}

	}

	// EXTRAE _
	p000e0014:
	{
 		if (skipdoall('p000e0014')) break p000e0014;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0014;
 		}
 		ACCwhatox2(13);
		{}

	}

	// EXTRAE TODO
	p000e0015:
	{
 		if (skipdoall('p000e0015')) break p000e0015;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0015;
			if (!CNDnoun1(20)) break p000e0015;
 		}
		if (!CNDprep(3)) break p000e0015;
		if (!CNDnoteq(13,255)) break p000e0015;
		if (!CNDonotzero(getFlag(13),2)) break p000e0015;
		if (!CNDabsent(getFlag(13))) break p000e0015;
 		ACCwriteln(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE TODO
	p000e0016:
	{
 		if (skipdoall('p000e0016')) break p000e0016;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0016;
			if (!CNDnoun1(20)) break p000e0016;
 		}
		if (!CNDprep(3)) break p000e0016;
		if (!CNDnoteq(13,255)) break p000e0016;
		if (!CNDonotzero(getFlag(13),18)) break p000e0016;
		if (!CNDabsent(getFlag(13))) break p000e0016;
 		ACCwriteln(6);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE TODO
	p000e0017:
	{
 		if (skipdoall('p000e0017')) break p000e0017;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0017;
			if (!CNDnoun1(20)) break p000e0017;
 		}
		if (!CNDprep(3)) break p000e0017;
		if (!CNDnoteq(13,255)) break p000e0017;
		if (!CNDonotzero(getFlag(13),2)) break p000e0017;
		if (!CNDpresent(getFlag(13))) break p000e0017;
 		ACCobjat(getFlag(13),14);
		if (!CNDzero(14)) break p000e0017;
 		ACCwriteln(7);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE TODO
	p000e0018:
	{
 		if (skipdoall('p000e0018')) break p000e0018;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0018;
			if (!CNDnoun1(20)) break p000e0018;
 		}
		if (!CNDprep(3)) break p000e0018;
		if (!CNDnoteq(13,255)) break p000e0018;
		if (!CNDonotzero(getFlag(13),18)) break p000e0018;
		if (!CNDpresent(getFlag(13))) break p000e0018;
 		ACCobjat(getFlag(13),14);
		if (!CNDzero(14)) break p000e0018;
 		ACCwriteln(8);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE TODO
	p000e0019:
	{
 		if (skipdoall('p000e0019')) break p000e0019;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0019;
			if (!CNDnoun1(20)) break p000e0019;
 		}
		if (!CNDprep(3)) break p000e0019;
		if (!CNDnoteq(13,255)) break p000e0019;
		if (!CNDonotzero(getFlag(13),2)) break p000e0019;
		if (!CNDpresent(getFlag(13))) break p000e0019;
		entry_for_doall = 'p000e0020';
		process_in_doall = 0;
 		ACCdoall(getFlag(13));
		break pro000_restart;
		{}

	}

	// EXTRAE TODO
	p000e0020:
	{
 		if (skipdoall('p000e0020')) break p000e0020;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0020;
			if (!CNDnoun1(20)) break p000e0020;
 		}
		if (!CNDprep(3)) break p000e0020;
		if (!CNDnoteq(13,255)) break p000e0020;
		if (!CNDonotzero(getFlag(13),18)) break p000e0020;
		if (!CNDpresent(getFlag(13))) break p000e0020;
		entry_for_doall = 'p000e0021';
		process_in_doall = 0;
 		ACCdoall(getFlag(13));
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0021:
	{
 		if (skipdoall('p000e0021')) break p000e0021;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0021;
 		}
		if (!CNDnoteq(51,255)) break p000e0021;
		if (!CNDpresent(getFlag(51))) break p000e0021;
		if (!CNDprep(4)) break p000e0021;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0021;
		if (!CNDpresent(getFlag(13))) break p000e0021;
 		ACCsynonym(74,255);
 		ACClet(43,4);
		{}

	}

	// PON _
	p000e0022:
	{
 		if (skipdoall('p000e0022')) break p000e0022;
 		if (in_response)
		{
			if (!CNDverb(71)) break p000e0022;
 		}
		if (!CNDnoteq(51,255)) break p000e0022;
		if (!CNDpresent(getFlag(51))) break p000e0022;
		if (!CNDprep(4)) break p000e0022;
 		ACCsynonym(74,255);
		{}

	}

	// ECHA _
	p000e0023:
	{
 		if (skipdoall('p000e0023')) break p000e0023;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0023;
 		}
 		ACCwhatox2(13);
		{}

	}

	// ECHA TODO
	p000e0024:
	{
 		if (skipdoall('p000e0024')) break p000e0024;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0024;
			if (!CNDnoun1(20)) break p000e0024;
 		}
		if (!CNDprep(4)) break p000e0024;
		if (!CNDnoteq(13,255)) break p000e0024;
		if (!CNDonotzero(getFlag(13),2)) break p000e0024;
		if (!CNDabsent(getFlag(13))) break p000e0024;
 		ACCwriteln(9);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA TODO
	p000e0025:
	{
 		if (skipdoall('p000e0025')) break p000e0025;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0025;
			if (!CNDnoun1(20)) break p000e0025;
 		}
		if (!CNDprep(4)) break p000e0025;
		if (!CNDnoteq(13,255)) break p000e0025;
		if (!CNDonotzero(getFlag(13),18)) break p000e0025;
		if (!CNDabsent(getFlag(13))) break p000e0025;
 		ACCwriteln(10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA TODO
	p000e0026:
	{
 		if (skipdoall('p000e0026')) break p000e0026;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0026;
			if (!CNDnoun1(20)) break p000e0026;
 		}
		if (!CNDprep(4)) break p000e0026;
		if (!CNDnoteq(13,255)) break p000e0026;
		if (!CNDonotzero(getFlag(13),2)) break p000e0026;
		if (!CNDcarried(getFlag(13))) break p000e0026;
 		ACCobjat(254,14);
		if (!CNDeq(14,1)) break p000e0026;
 		ACCwriteln(11);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA TODO
	p000e0027:
	{
 		if (skipdoall('p000e0027')) break p000e0027;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0027;
			if (!CNDnoun1(20)) break p000e0027;
 		}
		if (!CNDprep(4)) break p000e0027;
		if (!CNDnoteq(13,255)) break p000e0027;
		if (!CNDonotzero(getFlag(13),18)) break p000e0027;
		if (!CNDcarried(getFlag(13))) break p000e0027;
 		ACCobjat(254,14);
		if (!CNDeq(14,1)) break p000e0027;
 		ACCwriteln(12);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA TODO
	p000e0028:
	{
 		if (skipdoall('p000e0028')) break p000e0028;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0028;
			if (!CNDnoun1(20)) break p000e0028;
 		}
		if (!CNDprep(4)) break p000e0028;
		if (!CNDnoteq(13,255)) break p000e0028;
		if (!CNDonotzero(getFlag(13),2)) break p000e0028;
		if (!CNDisat(getFlag(13),getFlag(38))) break p000e0028;
 		ACCobjat(254,14);
		if (!CNDzero(14)) break p000e0028;
 		ACCwriteln(13);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA TODO
	p000e0029:
	{
 		if (skipdoall('p000e0029')) break p000e0029;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0029;
			if (!CNDnoun1(20)) break p000e0029;
 		}
		if (!CNDprep(4)) break p000e0029;
		if (!CNDnoteq(13,255)) break p000e0029;
		if (!CNDonotzero(getFlag(13),18)) break p000e0029;
		if (!CNDisat(getFlag(13),getFlag(38))) break p000e0029;
 		ACCobjat(254,14);
		if (!CNDzero(14)) break p000e0029;
 		ACCwriteln(14);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA TODO
	p000e0030:
	{
 		if (skipdoall('p000e0030')) break p000e0030;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0030;
			if (!CNDnoun1(20)) break p000e0030;
 		}
		if (!CNDprep(4)) break p000e0030;
		if (!CNDnoteq(13,255)) break p000e0030;
		if (!CNDonotzero(getFlag(13),2)) break p000e0030;
		if (!CNDpresent(getFlag(13))) break p000e0030;
		entry_for_doall = 'p000e0031';
		process_in_doall = 0;
 		ACCdoall(254);
		break pro000_restart;
		{}

	}

	// ECHA TODO
	p000e0031:
	{
 		if (skipdoall('p000e0031')) break p000e0031;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0031;
			if (!CNDnoun1(20)) break p000e0031;
 		}
		if (!CNDprep(4)) break p000e0031;
		if (!CNDnoteq(13,255)) break p000e0031;
		if (!CNDonotzero(getFlag(13),18)) break p000e0031;
		if (!CNDpresent(getFlag(13))) break p000e0031;
		entry_for_doall = 'p000e0032';
		process_in_doall = 0;
 		ACCdoall(254);
		break pro000_restart;
		{}

	}

	// DESVESTIR TODO
	p000e0032:
	{
 		if (skipdoall('p000e0032')) break p000e0032;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0032;
			if (!CNDnoun1(20)) break p000e0032;
 		}
 		ACCobjat(253,13);
		if (!CNDzero(13)) break p000e0032;
 		ACCwriteln(15);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESVESTIR TODO
	p000e0033:
	{
 		if (skipdoall('p000e0033')) break p000e0033;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0033;
			if (!CNDnoun1(20)) break p000e0033;
 		}
		entry_for_doall = 'p000e0034';
		process_in_doall = 0;
 		ACCdoall(253);
		break pro000_restart;
		{}

	}

	// PON TODO
	p000e0034:
	{
 		if (skipdoall('p000e0034')) break p000e0034;
 		if (in_response)
		{
			if (!CNDverb(71)) break p000e0034;
			if (!CNDnoun1(20)) break p000e0034;
 		}
 		ACCobjat(254,13);
		if (!CNDzero(13)) break p000e0034;
 		ACCwriteln(16);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PON TODO
	p000e0035:
	{
 		if (skipdoall('p000e0035')) break p000e0035;
 		if (in_response)
		{
			if (!CNDverb(71)) break p000e0035;
			if (!CNDnoun1(20)) break p000e0035;
 		}
		entry_for_doall = 'p000e0036';
		process_in_doall = 0;
 		ACCdoall(254);
		break pro000_restart;
		{}

	}

	// ARROJA TODO
	p000e0036:
	{
 		if (skipdoall('p000e0036')) break p000e0036;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0036;
			if (!CNDnoun1(20)) break p000e0036;
 		}
 		ACCobjat(254,13);
		if (!CNDzero(13)) break p000e0036;
 		ACCwriteln(17);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA TODO
	p000e0037:
	{
 		if (skipdoall('p000e0037')) break p000e0037;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0037;
			if (!CNDnoun1(20)) break p000e0037;
 		}
		if (!CNDeq(1,1)) break p000e0037;
		entry_for_doall = 'p000e0038';
		process_in_doall = 0;
 		ACCdoall(254);
		break pro000_restart;
		{}

	}

	// ARROJA TODO
	p000e0038:
	{
 		if (skipdoall('p000e0038')) break p000e0038;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0038;
			if (!CNDnoun1(20)) break p000e0038;
 		}
		if (!CNDgt(1,1)) break p000e0038;
 		ACCwriteln(18);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ TODO
	p000e0039:
	{
 		if (skipdoall('p000e0039')) break p000e0039;
 		if (in_response)
		{
			if (!CNDnoun1(20)) break p000e0039;
 		}
 		ACCwriteln(19);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCENDER _
	p000e0040:
	{
 		if (skipdoall('p000e0040')) break p000e0040;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0040;
 		}
 		ACCsynonym(90,10);
		{}

	}

	// BAJA _
	p000e0041:
	{
 		if (skipdoall('p000e0041')) break p000e0041;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0041;
 		}
 		ACCsynonym(90,11);
		{}

	}

	// ANDA _
	p000e0042:
	{
 		if (skipdoall('p000e0042')) break p000e0042;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0042;
 		}
		if (!CNDprep(13)) break p000e0042;
 		ACCsynonym(13,255);
		{}

	}

	// ANDA _
	p000e0043:
	{
 		if (skipdoall('p000e0043')) break p000e0043;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0043;
 		}
		if (!CNDprep(4)) break p000e0043;
 		ACCsynonym(12,255);
		{}

	}

	// ANDA _
	p000e0044:
	{
 		if (skipdoall('p000e0044')) break p000e0044;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0044;
 		}
		if (!CNDlt(34,14)) break p000e0044;
 		ACCcopyff(34,33);
		{}

	}

	// _ _
	p000e0045:
	{
 		if (skipdoall('p000e0045')) break p000e0045;
		if (!CNDprep(4)) break p000e0045;
		if (!CNDeq(33,255)) break p000e0045;
		if (!CNDeq(34,255)) break p000e0045;
 		ACCsynonym(12,255);
 		ACClet(43,255);
		{}

	}

	// _ _
	p000e0046:
	{
 		if (skipdoall('p000e0046')) break p000e0046;
		if (!CNDprep(13)) break p000e0046;
		if (!CNDeq(33,255)) break p000e0046;
		if (!CNDeq(34,255)) break p000e0046;
 		ACCsynonym(13,255);
 		ACClet(43,255);
		{}

	}

	// L _
	p000e0047:
	{
 		if (skipdoall('p000e0047')) break p000e0047;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0047;
 		}
		if (!CNDnoteq(34,255)) break p000e0047;
		if (!CNDprep(4)) break p000e0047;
 		ACCsynonym(30,255);
		{}

	}

	// L _
	p000e0048:
	{
 		if (skipdoall('p000e0048')) break p000e0048;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0048;
 		}
		if (!CNDnoteq(34,255)) break p000e0048;
 		ACCsynonym(30,255);
		{}

	}

	// L _
	p000e0049:
	{
 		if (skipdoall('p000e0049')) break p000e0049;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0049;
 		}
		if (!CNDbnotzero(12,1)) break p000e0049;
 		ACCsynonym(30,255);
		{}

	}

	// REBUSCA _
	p000e0050:
	{
 		if (skipdoall('p000e0050')) break p000e0050;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0050;
 		}
 		ACCsynonym(30,255);
 		ACClet(43,4);
		{}

	}

	// ARRANCA _
	p000e0051:
	{
 		if (skipdoall('p000e0051')) break p000e0051;
 		if (in_response)
		{
			if (!CNDverb(79)) break p000e0051;
 		}
 		ACCsynonym(32,255);
 		ACClet(43,3);
		{}

	}

	// EXITS _
	p000e0052:
	{
 		if (skipdoall('p000e0052')) break p000e0052;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0052;
 		}
		if (!CNDeq(34,255)) break p000e0052;
		if (!CNDbnotzero(12,1)) break p000e0052;
 		ACCsynonym(30,255);
		{}

	}

	// EXITS _
	p000e0053:
	{
 		if (skipdoall('p000e0053')) break p000e0053;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0053;
 		}
		if (!CNDnoteq(34,255)) break p000e0053;
 		ACCsynonym(30,255);
		{}

	}

	// CONSULTA _
	p000e0054:
	{
 		if (skipdoall('p000e0054')) break p000e0054;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0054;
 		}
		if (!CNDnoteq(34,255)) break p000e0054;
		if (!CNDnoteq(51,255)) break p000e0054;
		if (!CNDonotzero(getFlag(51),3)) break p000e0054;
 		ACCsynonym(108,255);
		{}

	}

	// _ _
	p000e0055:
	{
 		if (skipdoall('p000e0055')) break p000e0055;
		if (!CNDprep(9)) break p000e0055;
		if (!CNDbzero(12,2)) break p000e0055;
		if (!CNDnoteq(34,255)) break p000e0055;
 		ACCwhatox(16);
		if (!CNDnoteq(16,255)) break p000e0055;
		if (!CNDnoteq(44,255)) break p000e0055;
 		ACCwhatox2(15);
		if (!CNDnoteq(15,255)) break p000e0055;
 		ACCcopyff(34,14);
 		ACCcopyff(44,34);
 		ACCcopyff(14,44);
 		ACCcopyff(35,14);
 		ACCcopyff(45,35);
 		ACCcopyff(14,45);
 		ACCwhato();
		{}

	}

	// _ _
	p000e0056:
	{
 		if (skipdoall('p000e0056')) break p000e0056;
		if (!CNDprep(2)) break p000e0056;
		if (!CNDbzero(12,2)) break p000e0056;
		if (!CNDnoteq(34,255)) break p000e0056;
 		ACCwhatox(16);
		if (!CNDnoteq(16,255)) break p000e0056;
		if (!CNDnoteq(44,255)) break p000e0056;
 		ACCwhatox2(15);
		if (!CNDnoteq(15,255)) break p000e0056;
 		ACCcopyff(34,14);
 		ACCcopyff(44,34);
 		ACCcopyff(14,44);
 		ACCcopyff(35,14);
 		ACCcopyff(45,35);
 		ACCcopyff(14,45);
 		ACCwhato();
		{}

	}

	// _ _
	p000e0057:
	{
 		if (skipdoall('p000e0057')) break p000e0057;
		if (!CNDprep(4)) break p000e0057;
		if (!CNDbzero(12,2)) break p000e0057;
		if (!CNDnoteq(51,255)) break p000e0057;
		if (!CNDpresent(getFlag(51))) break p000e0057;
		if (!CNDonotzero(getFlag(51),2)) break p000e0057;
		if (!CNDnoteq(44,255)) break p000e0057;
 		ACCwhatox2(15);
		if (!CNDnoteq(15,255)) break p000e0057;
 		ACCcopyff(34,14);
 		ACCcopyff(44,34);
 		ACCcopyff(14,44);
 		ACCcopyff(35,14);
 		ACCcopyff(45,35);
 		ACCcopyff(14,45);
 		ACCwhato();
		{}

	}

	// _ _
	p000e0058:
	{
 		if (skipdoall('p000e0058')) break p000e0058;
		if (!CNDprep(3)) break p000e0058;
		if (!CNDbzero(12,2)) break p000e0058;
		if (!CNDnoteq(51,255)) break p000e0058;
		if (!CNDpresent(getFlag(51))) break p000e0058;
		if (!CNDonotzero(getFlag(51),2)) break p000e0058;
		if (!CNDnoteq(44,255)) break p000e0058;
 		ACCwhatox2(15);
		if (!CNDnoteq(15,255)) break p000e0058;
 		ACCcopyff(34,14);
 		ACCcopyff(44,34);
 		ACCcopyff(14,44);
 		ACCcopyff(35,14);
 		ACCcopyff(45,35);
 		ACCcopyff(14,45);
 		ACCwhato();
		{}

	}

	// COGE _
	p000e0059:
	{
 		if (skipdoall('p000e0059')) break p000e0059;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0059;
 		}
		if (!CNDprep(3)) break p000e0059;
 		ACCsynonym(75,255);
		{}

	}

	// _ _
	p000e0060:
	{
 		if (skipdoall('p000e0060')) break p000e0060;
 		ACChook(20);
		if (done_flag) break pro000_restart;
		{}

	}

	// EX OBJETO
	p000e0061:
	{
 		if (skipdoall('p000e0061')) break p000e0061;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0061;
			if (!CNDnoun1(50)) break p000e0061;
 		}
		if (!CNDpresent(0)) break p000e0061;
 		ACCwriteln(21);
 		ACCextern(22);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME OBJETO
	p000e0062:
	{
 		if (skipdoall('p000e0062')) break p000e0062;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0062;
			if (!CNDnoun1(50)) break p000e0062;
 		}
		if (!CNDpresent(0)) break p000e0062;
 		ACClet(201,40);
 		ACCwriteln(23);
 		ACCwrite(24);
 		ACCask(25,26,200);
 		function anykey00000() 
		{
 		ACCnewline();
		if (CNDeq(200,0))
  		{
 		ACCrandom(202);
 		ACCwriteln(27);
		if (CNDlt(202,getFlag(201)))
  		ACCwriteln(28);
		if (CNDge(202,getFlag(201)))
  		ACCwriteln(29);
 		}
		if (CNDeq(200,1))
  		ACCwriteln(30);
 		ACClet(200,255);
 		ACCdone();
		return;
		}
 		waitKey(anykey00000);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// _ _
	p000e0063:
	{
 		if (skipdoall('p000e0063')) break p000e0063;
 		ACChook(31);
		if (done_flag) break pro000_restart;
		{}

	}

	//  _
	p000e0064:
	{
 		if (skipdoall('p000e0064')) break p000e0064;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0064;
 		}
 		ACCinven();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0065:
	{
 		if (skipdoall('p000e0065')) break p000e0065;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0065;
 		}
		if (!CNDnoteq(51,255)) break p000e0065;
		if (!CNDpresent(getFlag(51))) break p000e0065;
		if (!CNDonotzero(getFlag(51),7)) break p000e0065;
 		ACCgoto(getFlag(51));
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SENTAR _
	p000e0066:
	{
 		if (skipdoall('p000e0066')) break p000e0066;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0066;
 		}
 		ACCwriteln(32);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTARSE _
	p000e0067:
	{
 		if (skipdoall('p000e0067')) break p000e0067;
 		if (in_response)
		{
			if (!CNDverb(53)) break p000e0067;
 		}
 		ACCwriteln(33);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAMINARSE _
	p000e0068:
	{
 		if (skipdoall('p000e0068')) break p000e0068;
 		if (in_response)
		{
			if (!CNDverb(85)) break p000e0068;
 		}
 		ACCwriteln(34);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REGISTRARS _
	p000e0069:
	{
 		if (skipdoall('p000e0069')) break p000e0069;
 		if (in_response)
		{
			if (!CNDverb(84)) break p000e0069;
 		}
 		ACCwriteln(35);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MATARSE _
	p000e0070:
	{
 		if (skipdoall('p000e0070')) break p000e0070;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0070;
 		}
 		ACCwriteln(36);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LAVARSE _
	p000e0071:
	{
 		if (skipdoall('p000e0071')) break p000e0071;
 		if (in_response)
		{
			if (!CNDverb(88)) break p000e0071;
 		}
 		ACCwriteln(37);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// XYZZY _
	p000e0072:
	{
 		if (skipdoall('p000e0072')) break p000e0072;
 		if (in_response)
		{
			if (!CNDverb(82)) break p000e0072;
 		}
 		ACCwriteln(38);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIN _
	p000e0073:
	{
 		if (skipdoall('p000e0073')) break p000e0073;
 		if (in_response)
		{
			if (!CNDverb(25)) break p000e0073;
 		}
 		ACCquit();
 		function anykey00001() 
		{
 		ACCturns();
 		ACCend();
		return;
		}
 		waitKey(anykey00001);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// GRABA _
	p000e0074:
	{
 		if (skipdoall('p000e0074')) break p000e0074;
 		if (in_response)
		{
			if (!CNDverb(26)) break p000e0074;
 		}
 		ACCgoto(getFlag(62));
 		ACCsynonym(28,255);
		{}

	}

	// CARGA _
	p000e0075:
	{
 		if (skipdoall('p000e0075')) break p000e0075;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0075;
 		}
 		ACCsynonym(29,255);
		{}

	}

	// GRABARAM _
	p000e0076:
	{
 		if (skipdoall('p000e0076')) break p000e0076;
 		if (in_response)
		{
			if (!CNDverb(28)) break p000e0076;
 		}
 		ACCgoto(getFlag(62));
 		ACCminus(31,1);
 		ACCramsave();
 		ACCwriteln(39);
 		ACCpause(1);
 		function anykey00002() 
		{
 		ACCdone();
		return;
		}
 		waitKey(anykey00002);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CARGARAM _
	p000e0077:
	{
 		if (skipdoall('p000e0077')) break p000e0077;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0077;
 		}
 		ACCminus(31,1);
 		ACCwriteln(40);
 		ACCpause(1);
 		function anykey00004() 
		{
 		ACCramload(255);
 		ACCdesc();
		return;
		}
 		function anykey00003() 
		{
 		ACCanykey();
 		waitKey(anykey00004);
		}
 		waitKey(anykey00003);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TRANSCRIP _
	p000e0078:
	{
 		if (skipdoall('p000e0078')) break p000e0078;
 		if (in_response)
		{
			if (!CNDverb(68)) break p000e0078;
 		}
 		ACCsilence(1);
 		ACCtranscript(1);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COGE _
	p000e0079:
	{
 		if (skipdoall('p000e0079')) break p000e0079;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0079;
 		}
		if (!CNDprep(3)) break p000e0079;
 		ACCwhatox2(14);
		if (!CNDnoteq(14,255)) break p000e0079;
		if (!CNDonotzero(getFlag(14),2)) break p000e0079;
 		ACCsynonym(75,255);
		{}

	}

	// COGE _
	p000e0080:
	{
 		if (skipdoall('p000e0080')) break p000e0080;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0080;
 		}
		if (!CNDeq(34,255)) break p000e0080;
		if (!CNDbnotzero(12,1)) break p000e0080;
 		ACCwriteln(41);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COGE _
	p000e0081:
	{
 		if (skipdoall('p000e0081')) break p000e0081;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0081;
 		}
		if (!CNDeq(34,255)) break p000e0081;
		if (!CNDbzero(12,1)) break p000e0081;
 		ACCwriteln(42);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COGE _
	p000e0082:
	{
 		if (skipdoall('p000e0082')) break p000e0082;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0082;
 		}
		if (!CNDnoteq(51,255)) break p000e0082;
		if (!CNDonotzero(getFlag(51),21)) break p000e0082;
 		ACCwriteln(43);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COGE _
	p000e0083:
	{
 		if (skipdoall('p000e0083')) break p000e0083;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0083;
 		}
 		ACCautog();
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0084:
	{
 		if (skipdoall('p000e0084')) break p000e0084;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0084;
 		}
		if (!CNDprep(4)) break p000e0084;
 		ACCwhatox2(14);
		if (!CNDnoteq(14,255)) break p000e0084;
		if (!CNDonotzero(getFlag(14),2)) break p000e0084;
 		ACCsynonym(74,255);
		{}

	}

	// DEJA _
	p000e0085:
	{
 		if (skipdoall('p000e0085')) break p000e0085;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0085;
 		}
		if (!CNDprep(17)) break p000e0085;
 		ACCwhatox2(14);
		if (!CNDnoteq(14,255)) break p000e0085;
		if (!CNDonotzero(getFlag(14),18)) break p000e0085;
 		ACCplace(getFlag(51),getFlag(14));
 		ACCwriteln(44);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0086:
	{
 		if (skipdoall('p000e0086')) break p000e0086;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0086;
 		}
		if (!CNDprep(4)) break p000e0086;
 		ACCwhatox2(14);
		if (!CNDnoteq(14,255)) break p000e0086;
		if (!CNDonotzero(getFlag(14),18)) break p000e0086;
 		ACCplace(getFlag(51),getFlag(14));
 		ACCwriteln(45);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0087:
	{
 		if (skipdoall('p000e0087')) break p000e0087;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0087;
 		}
		if (!CNDprep(17)) break p000e0087;
 		ACCwhatox2(14);
		if (!CNDnoteq(14,255)) break p000e0087;
		if (!CNDozero(getFlag(14),18)) break p000e0087;
 		ACCwriteln(46);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0088:
	{
 		if (skipdoall('p000e0088')) break p000e0088;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0088;
 		}
		if (!CNDprep(4)) break p000e0088;
 		ACCwhatox2(14);
		if (!CNDnoteq(14,255)) break p000e0088;
		if (!CNDozero(getFlag(14),18)) break p000e0088;
 		ACCwriteln(47);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0089:
	{
 		if (skipdoall('p000e0089')) break p000e0089;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0089;
 		}
		if (!CNDeq(34,255)) break p000e0089;
		if (!CNDbnotzero(12,1)) break p000e0089;
 		ACCwriteln(48);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0090:
	{
 		if (skipdoall('p000e0090')) break p000e0090;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0090;
 		}
		if (!CNDeq(34,255)) break p000e0090;
		if (!CNDbzero(12,1)) break p000e0090;
 		ACCwriteln(49);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0091:
	{
 		if (skipdoall('p000e0091')) break p000e0091;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0091;
 		}
 		ACCautod();
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0092:
	{
 		if (skipdoall('p000e0092')) break p000e0092;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0092;
 		}
 		ACCwhatox2(15);
		{}

	}

	// EXTRAE _
	p000e0093:
	{
 		if (skipdoall('p000e0093')) break p000e0093;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0093;
 		}
		if (!CNDeq(34,255)) break p000e0093;
		if (!CNDbzero(12,1)) break p000e0093;
 		ACCwriteln(50);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0094:
	{
 		if (skipdoall('p000e0094')) break p000e0094;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0094;
 		}
		if (!CNDeq(34,255)) break p000e0094;
		if (!CNDbnotzero(12,1)) break p000e0094;
 		ACCwriteln(51);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0095:
	{
 		if (skipdoall('p000e0095')) break p000e0095;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0095;
 		}
		if (!CNDnoteq(34,255)) break p000e0095;
		if (!CNDeq(51,255)) break p000e0095;
 		ACCwriteln(52);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0096:
	{
 		if (skipdoall('p000e0096')) break p000e0096;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0096;
 		}
		if (!CNDbzero(12,2)) break p000e0096;
		if (!CNDeq(44,255)) break p000e0096;
		if (!CNDbnotzero(12,1)) break p000e0096;
 		ACCwriteln(53);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0097:
	{
 		if (skipdoall('p000e0097')) break p000e0097;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0097;
 		}
		if (!CNDnoteq(51,255)) break p000e0097;
		if (!CNDworn(getFlag(51))) break p000e0097;
		if (!CNDonotzero(getFlag(51),1)) break p000e0097;
 		ACCwriteln(54);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0098:
	{
 		if (skipdoall('p000e0098')) break p000e0098;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0098;
 		}
		if (!CNDnoteq(51,255)) break p000e0098;
		if (!CNDeq(44,255)) break p000e0098;
		if (!CNDbzero(12,1)) break p000e0098;
 		ACCwriteln(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0099:
	{
 		if (skipdoall('p000e0099')) break p000e0099;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0099;
 		}
		if (!CNDnoteq(51,255)) break p000e0099;
		if (!CNDcarried(getFlag(51))) break p000e0099;
		if (!CNDeq(44,255)) break p000e0099;
		if (!CNDbnotzero(12,1)) break p000e0099;
 		ACCwriteln(56);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0100:
	{
 		if (skipdoall('p000e0100')) break p000e0100;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0100;
 		}
		if (!CNDeq(15,255)) break p000e0100;
 		ACCwriteln(57);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0101:
	{
 		if (skipdoall('p000e0101')) break p000e0101;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0101;
 		}
		if (!CNDnoteq(15,255)) break p000e0101;
		if (!CNDonotzero(getFlag(15),2)) break p000e0101;
		if (!CNDsame(51,15)) break p000e0101;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0102:
	{
 		if (skipdoall('p000e0102')) break p000e0102;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0102;
 		}
		if (!CNDnoteq(15,255)) break p000e0102;
		if (!CNDonotzero(getFlag(15),2)) break p000e0102;
		if (!CNDonotzero(getFlag(15),13)) break p000e0102;
		if (!CNDozero(getFlag(15),14)) break p000e0102;
		if (!CNDpresent(getFlag(15))) break p000e0102;
 		ACCwriteln(58);
 		ACCbreak();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0103:
	{
 		if (skipdoall('p000e0103')) break p000e0103;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0103;
 		}
		if (!CNDnoteq(15,255)) break p000e0103;
		if (!CNDonotzero(getFlag(15),2)) break p000e0103;
		if (!CNDonotzero(getFlag(15),9)) break p000e0103;
		if (!CNDozero(getFlag(15),10)) break p000e0103;
		if (!CNDpresent(getFlag(15))) break p000e0103;
 		ACCwriteln(59);
 		ACCbreak();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0104:
	{
 		if (skipdoall('p000e0104')) break p000e0104;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0104;
 		}
		if (!CNDnoteq(15,255)) break p000e0104;
		if (!CNDonotzero(getFlag(15),2)) break p000e0104;
		if (!CNDabsent(getFlag(15))) break p000e0104;
 		ACCwriteln(60);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0105:
	{
 		if (skipdoall('p000e0105')) break p000e0105;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0105;
 		}
		if (!CNDnoteq(15,255)) break p000e0105;
		if (!CNDonotzero(getFlag(15),2)) break p000e0105;
		if (!CNDabsent(getFlag(15))) break p000e0105;
 		ACCwriteln(61);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0106:
	{
 		if (skipdoall('p000e0106')) break p000e0106;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0106;
 		}
		if (!CNDnoteq(15,255)) break p000e0106;
		if (!CNDonotzero(getFlag(15),2)) break p000e0106;
		if (!CNDpresent(getFlag(15))) break p000e0106;
 		ACCautot(getFlag(15));
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0107:
	{
 		if (skipdoall('p000e0107')) break p000e0107;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0107;
 		}
		if (!CNDnoteq(15,255)) break p000e0107;
		if (!CNDonotzero(getFlag(15),18)) break p000e0107;
		if (!CNDpresent(getFlag(15))) break p000e0107;
 		ACCautot(getFlag(15));
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTRAE _
	p000e0108:
	{
 		if (skipdoall('p000e0108')) break p000e0108;
 		if (in_response)
		{
			if (!CNDverb(75)) break p000e0108;
 		}
		if (!CNDnoteq(15,255)) break p000e0108;
 		ACCwriteln(62);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0109:
	{
 		if (skipdoall('p000e0109')) break p000e0109;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0109;
 		}
 		ACCwhatox2(15);
		{}

	}

	// ECHA _
	p000e0110:
	{
 		if (skipdoall('p000e0110')) break p000e0110;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0110;
 		}
		if (!CNDeq(34,255)) break p000e0110;
		if (!CNDbzero(12,1)) break p000e0110;
 		ACCwriteln(63);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0111:
	{
 		if (skipdoall('p000e0111')) break p000e0111;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0111;
 		}
		if (!CNDeq(34,255)) break p000e0111;
		if (!CNDbnotzero(12,1)) break p000e0111;
 		ACCwriteln(64);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0112:
	{
 		if (skipdoall('p000e0112')) break p000e0112;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0112;
 		}
		if (!CNDnoteq(34,255)) break p000e0112;
		if (!CNDeq(51,255)) break p000e0112;
 		ACCwriteln(65);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0113:
	{
 		if (skipdoall('p000e0113')) break p000e0113;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0113;
 		}
		if (!CNDbzero(12,2)) break p000e0113;
		if (!CNDeq(44,255)) break p000e0113;
		if (!CNDbnotzero(12,1)) break p000e0113;
 		ACCwriteln(66);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0114:
	{
 		if (skipdoall('p000e0114')) break p000e0114;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0114;
 		}
		if (!CNDnoteq(51,255)) break p000e0114;
		if (!CNDworn(getFlag(51))) break p000e0114;
		if (!CNDonotzero(getFlag(51),1)) break p000e0114;
 		ACCwriteln(67);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0115:
	{
 		if (skipdoall('p000e0115')) break p000e0115;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0115;
 		}
		if (!CNDnoteq(51,255)) break p000e0115;
		if (!CNDnotcarr(getFlag(51))) break p000e0115;
 		ACCwriteln(68);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0116:
	{
 		if (skipdoall('p000e0116')) break p000e0116;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0116;
 		}
		if (!CNDnoteq(51,255)) break p000e0116;
		if (!CNDcarried(getFlag(51))) break p000e0116;
		if (!CNDeq(44,255)) break p000e0116;
		if (!CNDbzero(12,1)) break p000e0116;
 		ACCwriteln(69);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0117:
	{
 		if (skipdoall('p000e0117')) break p000e0117;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0117;
 		}
		if (!CNDnoteq(51,255)) break p000e0117;
		if (!CNDcarried(getFlag(51))) break p000e0117;
		if (!CNDeq(44,255)) break p000e0117;
		if (!CNDbnotzero(12,1)) break p000e0117;
 		ACCwriteln(70);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0118:
	{
 		if (skipdoall('p000e0118')) break p000e0118;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0118;
 		}
		if (!CNDnoteq(51,255)) break p000e0118;
		if (!CNDcarried(getFlag(51))) break p000e0118;
		if (!CNDnoteq(44,255)) break p000e0118;
		if (!CNDeq(15,255)) break p000e0118;
 		ACCwriteln(71);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0119:
	{
 		if (skipdoall('p000e0119')) break p000e0119;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0119;
 		}
		if (!CNDnoteq(15,255)) break p000e0119;
		if (!CNDonotzero(getFlag(15),2)) break p000e0119;
		if (!CNDsame(51,15)) break p000e0119;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0120:
	{
 		if (skipdoall('p000e0120')) break p000e0120;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0120;
 		}
		if (!CNDnoteq(15,255)) break p000e0120;
		if (!CNDonotzero(getFlag(15),18)) break p000e0120;
		if (!CNDsame(51,15)) break p000e0120;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0121:
	{
 		if (skipdoall('p000e0121')) break p000e0121;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0121;
 		}
		if (!CNDnoteq(51,255)) break p000e0121;
		if (!CNDcarried(getFlag(51))) break p000e0121;
		if (!CNDnoteq(44,255)) break p000e0121;
		if (!CNDnoteq(15,255)) break p000e0121;
		if (!CNDozero(getFlag(15),2)) break p000e0121;
		if (!CNDozero(getFlag(15),18)) break p000e0121;
		if (!CNDpresent(getFlag(15))) break p000e0121;
 		ACCwriteln(72);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0122:
	{
 		if (skipdoall('p000e0122')) break p000e0122;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0122;
 		}
		if (!CNDnoteq(51,255)) break p000e0122;
		if (!CNDcarried(getFlag(51))) break p000e0122;
		if (!CNDnoteq(44,255)) break p000e0122;
		if (!CNDnoteq(15,255)) break p000e0122;
		if (!CNDonotzero(getFlag(15),2)) break p000e0122;
		if (!CNDabsent(getFlag(15))) break p000e0122;
 		ACCwriteln(73);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0123:
	{
 		if (skipdoall('p000e0123')) break p000e0123;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0123;
 		}
		if (!CNDnoteq(51,255)) break p000e0123;
		if (!CNDcarried(getFlag(51))) break p000e0123;
		if (!CNDnoteq(44,255)) break p000e0123;
		if (!CNDnoteq(15,255)) break p000e0123;
		if (!CNDonotzero(getFlag(15),18)) break p000e0123;
		if (!CNDabsent(getFlag(15))) break p000e0123;
 		ACCwriteln(74);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0124:
	{
 		if (skipdoall('p000e0124')) break p000e0124;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0124;
 		}
		if (!CNDnoteq(51,255)) break p000e0124;
		if (!CNDcarried(getFlag(51))) break p000e0124;
		if (!CNDnoteq(44,255)) break p000e0124;
		if (!CNDnoteq(15,255)) break p000e0124;
		if (!CNDonotzero(getFlag(15),2)) break p000e0124;
		if (!CNDpresent(getFlag(15))) break p000e0124;
		if (!CNDonotzero(getFlag(15),13)) break p000e0124;
		if (!CNDozero(getFlag(15),14)) break p000e0124;
 		ACCwriteln(75);
 		ACCbreak();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0125:
	{
 		if (skipdoall('p000e0125')) break p000e0125;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0125;
 		}
		if (!CNDnoteq(51,255)) break p000e0125;
		if (!CNDcarried(getFlag(51))) break p000e0125;
		if (!CNDnoteq(44,255)) break p000e0125;
		if (!CNDnoteq(15,255)) break p000e0125;
		if (!CNDonotzero(getFlag(15),2)) break p000e0125;
		if (!CNDpresent(getFlag(15))) break p000e0125;
		if (!CNDonotzero(getFlag(15),9)) break p000e0125;
		if (!CNDozero(getFlag(15),10)) break p000e0125;
 		ACCwriteln(76);
 		ACCbreak();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0126:
	{
 		if (skipdoall('p000e0126')) break p000e0126;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0126;
 		}
		if (!CNDnoteq(51,255)) break p000e0126;
		if (!CNDcarried(getFlag(51))) break p000e0126;
		if (!CNDnoteq(44,255)) break p000e0126;
		if (!CNDnoteq(15,255)) break p000e0126;
		if (!CNDonotzero(getFlag(15),2)) break p000e0126;
		if (!CNDpresent(getFlag(15))) break p000e0126;
 		ACCautop(getFlag(15));
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ECHA _
	p000e0127:
	{
 		if (skipdoall('p000e0127')) break p000e0127;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0127;
 		}
		if (!CNDnoteq(51,255)) break p000e0127;
		if (!CNDcarried(getFlag(51))) break p000e0127;
		if (!CNDnoteq(44,255)) break p000e0127;
		if (!CNDnoteq(15,255)) break p000e0127;
		if (!CNDonotzero(getFlag(15),18)) break p000e0127;
		if (!CNDpresent(getFlag(15))) break p000e0127;
 		ACCautop(getFlag(15));
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESVESTIR _
	p000e0128:
	{
 		if (skipdoall('p000e0128')) break p000e0128;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0128;
 		}
		if (!CNDeq(34,255)) break p000e0128;
		if (!CNDbnotzero(12,1)) break p000e0128;
 		ACCwriteln(77);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESVESTIR _
	p000e0129:
	{
 		if (skipdoall('p000e0129')) break p000e0129;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0129;
 		}
		if (!CNDeq(34,255)) break p000e0129;
		if (!CNDbzero(12,1)) break p000e0129;
 		ACCwriteln(78);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESVESTIR _
	p000e0130:
	{
 		if (skipdoall('p000e0130')) break p000e0130;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0130;
 		}
 		ACCautor();
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PON _
	p000e0131:
	{
 		if (skipdoall('p000e0131')) break p000e0131;
 		if (in_response)
		{
			if (!CNDverb(71)) break p000e0131;
 		}
		if (!CNDeq(34,255)) break p000e0131;
		if (!CNDbnotzero(12,1)) break p000e0131;
 		ACCwriteln(79);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PON _
	p000e0132:
	{
 		if (skipdoall('p000e0132')) break p000e0132;
 		if (in_response)
		{
			if (!CNDverb(71)) break p000e0132;
 		}
		if (!CNDeq(34,255)) break p000e0132;
		if (!CNDbzero(12,1)) break p000e0132;
 		ACCwriteln(80);
		{}

	}

	// PON _
	p000e0133:
	{
 		if (skipdoall('p000e0133')) break p000e0133;
 		if (in_response)
		{
			if (!CNDverb(71)) break p000e0133;
 		}
 		ACCautow();
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0134:
	{
 		if (skipdoall('p000e0134')) break p000e0134;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0134;
 		}
		if (!CNDprep(3)) break p000e0134;
		if (!CNDeq(34,255)) break p000e0134;
		if (!CNDbnotzero(12,1)) break p000e0134;
 		ACCwriteln(81);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0135:
	{
 		if (skipdoall('p000e0135')) break p000e0135;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0135;
 		}
		if (!CNDprep(3)) break p000e0135;
		if (!CNDeq(34,255)) break p000e0135;
		if (!CNDbzero(12,1)) break p000e0135;
 		ACCwriteln(82);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0136:
	{
 		if (skipdoall('p000e0136')) break p000e0136;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0136;
 		}
		if (!CNDprep(3)) break p000e0136;
		if (!CNDnoteq(34,255)) break p000e0136;
		if (!CNDnoteq(51,255)) break p000e0136;
		if (!CNDonotzero(getFlag(51),3)) break p000e0136;
		if (!CNDpresent(getFlag(51))) break p000e0136;
 		ACCwriteln(83);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0137:
	{
 		if (skipdoall('p000e0137')) break p000e0137;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0137;
 		}
		if (!CNDprep(3)) break p000e0137;
		if (!CNDnoteq(34,255)) break p000e0137;
		if (!CNDnoteq(51,255)) break p000e0137;
		if (!CNDonotzero(getFlag(51),3)) break p000e0137;
		if (!CNDabsent(getFlag(51))) break p000e0137;
 		ACCwriteln(84);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0138:
	{
 		if (skipdoall('p000e0138')) break p000e0138;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0138;
 		}
		if (!CNDprep(3)) break p000e0138;
		if (!CNDnoteq(34,255)) break p000e0138;
		if (!CNDeq(51,255)) break p000e0138;
 		ACCwriteln(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0139:
	{
 		if (skipdoall('p000e0139')) break p000e0139;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0139;
 		}
		if (!CNDprep(3)) break p000e0139;
		if (!CNDnoteq(51,255)) break p000e0139;
		if (!CNDpresent(getFlag(51))) break p000e0139;
 		ACCwriteln(86);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0140:
	{
 		if (skipdoall('p000e0140')) break p000e0140;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0140;
 		}
		if (!CNDprep(3)) break p000e0140;
		if (!CNDnoteq(51,255)) break p000e0140;
		if (!CNDabsent(getFlag(51))) break p000e0140;
 		ACCwriteln(87);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L _
	p000e0141:
	{
 		if (skipdoall('p000e0141')) break p000e0141;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0141;
 		}
		if (!CNDprep(4)) break p000e0141;
		if (!CNDeq(34,255)) break p000e0141;
		if (!CNDbnotzero(12,1)) break p000e0141;
 		ACCwriteln(88);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L _
	p000e0142:
	{
 		if (skipdoall('p000e0142')) break p000e0142;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0142;
 		}
		if (!CNDprep(4)) break p000e0142;
		if (!CNDeq(34,255)) break p000e0142;
		if (!CNDbzero(12,1)) break p000e0142;
 		ACCwriteln(89);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L _
	p000e0143:
	{
 		if (skipdoall('p000e0143')) break p000e0143;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0143;
 		}
		if (!CNDlt(34,12)) break p000e0143;
 		ACCwriteln(90);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L _
	p000e0144:
	{
 		if (skipdoall('p000e0144')) break p000e0144;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0144;
 		}
		if (!CNDeq(34,255)) break p000e0144;
		if (!CNDbnotzero(12,1)) break p000e0144;
 		ACCwriteln(91);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L _
	p000e0145:
	{
 		if (skipdoall('p000e0145')) break p000e0145;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0145;
 		}
		if (!CNDeq(34,255)) break p000e0145;
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0146:
	{
 		if (skipdoall('p000e0146')) break p000e0146;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0146;
 		}
		if (!CNDeq(34,255)) break p000e0146;
		if (!CNDbnotzero(12,1)) break p000e0146;
 		ACCwriteln(92);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0147:
	{
 		if (skipdoall('p000e0147')) break p000e0147;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0147;
 		}
		if (!CNDeq(34,255)) break p000e0147;
		if (!CNDbzero(12,1)) break p000e0147;
 		ACCwriteln(93);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0148:
	{
 		if (skipdoall('p000e0148')) break p000e0148;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0148;
 		}
		if (!CNDnoteq(34,255)) break p000e0148;
		if (!CNDnoteq(51,255)) break p000e0148;
		if (!CNDonotzero(getFlag(51),3)) break p000e0148;
		if (!CNDpresent(getFlag(51))) break p000e0148;
 		ACCwriteln(94);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0149:
	{
 		if (skipdoall('p000e0149')) break p000e0149;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0149;
 		}
		if (!CNDnoteq(34,255)) break p000e0149;
		if (!CNDnoteq(51,255)) break p000e0149;
		if (!CNDonotzero(getFlag(51),3)) break p000e0149;
		if (!CNDabsent(getFlag(51))) break p000e0149;
 		ACCwriteln(95);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0150:
	{
 		if (skipdoall('p000e0150')) break p000e0150;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0150;
 		}
		if (!CNDnoteq(34,255)) break p000e0150;
		if (!CNDeq(51,255)) break p000e0150;
 		ACCwriteln(96);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0151:
	{
 		if (skipdoall('p000e0151')) break p000e0151;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0151;
 		}
		if (!CNDnoteq(51,255)) break p000e0151;
		if (!CNDpresent(getFlag(51))) break p000e0151;
 		ACCwriteln(97);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCONDE _
	p000e0152:
	{
 		if (skipdoall('p000e0152')) break p000e0152;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0152;
 		}
		if (!CNDnoteq(51,255)) break p000e0152;
		if (!CNDabsent(getFlag(51))) break p000e0152;
 		ACCwriteln(98);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLORA _
	p000e0153:
	{
 		if (skipdoall('p000e0153')) break p000e0153;
 		if (in_response)
		{
			if (!CNDverb(93)) break p000e0153;
 		}
 		ACCwriteln(99);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACORDARSE _
	p000e0154:
	{
 		if (skipdoall('p000e0154')) break p000e0154;
 		if (in_response)
		{
			if (!CNDverb(94)) break p000e0154;
 		}
 		ACCwriteln(100);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SALUDA _
	p000e0155:
	{
 		if (skipdoall('p000e0155')) break p000e0155;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0155;
 		}
 		ACCwriteln(101);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCONDERSE _
	p000e0156:
	{
 		if (skipdoall('p000e0156')) break p000e0156;
 		if (in_response)
		{
			if (!CNDverb(92)) break p000e0156;
 		}
 		ACCwriteln(102);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0157:
	{
 		if (skipdoall('p000e0157')) break p000e0157;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0157;
 		}
		if (!CNDeq(34,255)) break p000e0157;
		if (!CNDbnotzero(12,1)) break p000e0157;
 		ACCwriteln(103);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0158:
	{
 		if (skipdoall('p000e0158')) break p000e0158;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0158;
 		}
		if (!CNDeq(34,255)) break p000e0158;
		if (!CNDbzero(12,1)) break p000e0158;
 		ACCwriteln(104);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0159:
	{
 		if (skipdoall('p000e0159')) break p000e0159;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0159;
 		}
		if (!CNDnoteq(34,255)) break p000e0159;
		if (!CNDnoteq(51,255)) break p000e0159;
		if (!CNDonotzero(getFlag(51),3)) break p000e0159;
		if (!CNDpresent(getFlag(51))) break p000e0159;
 		ACCwriteln(105);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0160:
	{
 		if (skipdoall('p000e0160')) break p000e0160;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0160;
 		}
		if (!CNDnoteq(34,255)) break p000e0160;
		if (!CNDnoteq(51,255)) break p000e0160;
		if (!CNDonotzero(getFlag(51),21)) break p000e0160;
		if (!CNDpresent(getFlag(51))) break p000e0160;
 		ACCwriteln(106);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0161:
	{
 		if (skipdoall('p000e0161')) break p000e0161;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0161;
 		}
		if (!CNDnoteq(34,255)) break p000e0161;
		if (!CNDnoteq(51,255)) break p000e0161;
		if (!CNDonotzero(getFlag(51),3)) break p000e0161;
		if (!CNDabsent(getFlag(51))) break p000e0161;
 		ACCwriteln(107);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0162:
	{
 		if (skipdoall('p000e0162')) break p000e0162;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0162;
 		}
		if (!CNDnoteq(34,255)) break p000e0162;
		if (!CNDnoteq(51,255)) break p000e0162;
		if (!CNDonotzero(getFlag(51),21)) break p000e0162;
		if (!CNDabsent(getFlag(51))) break p000e0162;
 		ACCwriteln(108);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0163:
	{
 		if (skipdoall('p000e0163')) break p000e0163;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0163;
 		}
		if (!CNDnoteq(34,255)) break p000e0163;
		if (!CNDeq(51,255)) break p000e0163;
 		ACCwriteln(109);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0164:
	{
 		if (skipdoall('p000e0164')) break p000e0164;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0164;
 		}
		if (!CNDnoteq(51,255)) break p000e0164;
		if (!CNDpresent(getFlag(51))) break p000e0164;
 		ACCwriteln(110);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0165:
	{
 		if (skipdoall('p000e0165')) break p000e0165;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0165;
 		}
		if (!CNDnoteq(51,255)) break p000e0165;
		if (!CNDabsent(getFlag(51))) break p000e0165;
 		ACCwriteln(111);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPLAZA _
	p000e0166:
	{
 		if (skipdoall('p000e0166')) break p000e0166;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0166;
 		}
		if (!CNDeq(34,255)) break p000e0166;
		if (!CNDbnotzero(12,1)) break p000e0166;
 		ACCwriteln(112);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPLAZA _
	p000e0167:
	{
 		if (skipdoall('p000e0167')) break p000e0167;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0167;
 		}
		if (!CNDeq(34,255)) break p000e0167;
		if (!CNDbzero(12,1)) break p000e0167;
 		ACCwriteln(113);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPLAZA _
	p000e0168:
	{
 		if (skipdoall('p000e0168')) break p000e0168;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0168;
 		}
		if (!CNDnoteq(34,255)) break p000e0168;
		if (!CNDnoteq(51,255)) break p000e0168;
		if (!CNDonotzero(getFlag(51),3)) break p000e0168;
		if (!CNDpresent(getFlag(51))) break p000e0168;
 		ACCwriteln(114);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPLAZA _
	p000e0169:
	{
 		if (skipdoall('p000e0169')) break p000e0169;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0169;
 		}
		if (!CNDnoteq(34,255)) break p000e0169;
		if (!CNDnoteq(51,255)) break p000e0169;
		if (!CNDonotzero(getFlag(51),3)) break p000e0169;
		if (!CNDabsent(getFlag(51))) break p000e0169;
 		ACCwriteln(115);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPLAZA _
	p000e0170:
	{
 		if (skipdoall('p000e0170')) break p000e0170;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0170;
 		}
		if (!CNDnoteq(34,255)) break p000e0170;
		if (!CNDeq(51,255)) break p000e0170;
 		ACCwriteln(116);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPLAZA _
	p000e0171:
	{
 		if (skipdoall('p000e0171')) break p000e0171;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0171;
 		}
		if (!CNDnoteq(51,255)) break p000e0171;
		if (!CNDpresent(getFlag(51))) break p000e0171;
 		ACCwriteln(117);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPLAZA _
	p000e0172:
	{
 		if (skipdoall('p000e0172')) break p000e0172;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0172;
 		}
		if (!CNDnoteq(51,255)) break p000e0172;
		if (!CNDabsent(getFlag(51))) break p000e0172;
 		ACCwriteln(118);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0173:
	{
 		if (skipdoall('p000e0173')) break p000e0173;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0173;
 		}
		if (!CNDeq(34,255)) break p000e0173;
		if (!CNDbnotzero(12,1)) break p000e0173;
 		ACCwriteln(119);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0174:
	{
 		if (skipdoall('p000e0174')) break p000e0174;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0174;
 		}
		if (!CNDeq(34,255)) break p000e0174;
		if (!CNDbzero(12,1)) break p000e0174;
 		ACCwriteln(120);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0175:
	{
 		if (skipdoall('p000e0175')) break p000e0175;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0175;
 		}
		if (!CNDnoteq(51,255)) break p000e0175;
		if (!CNDonotzero(getFlag(51),3)) break p000e0175;
		if (!CNDpresent(getFlag(51))) break p000e0175;
 		ACCwriteln(121);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0176:
	{
 		if (skipdoall('p000e0176')) break p000e0176;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0176;
 		}
		if (!CNDnoteq(51,255)) break p000e0176;
		if (!CNDonotzero(getFlag(51),21)) break p000e0176;
		if (!CNDpresent(getFlag(51))) break p000e0176;
 		ACCwriteln(122);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0177:
	{
 		if (skipdoall('p000e0177')) break p000e0177;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0177;
 		}
		if (!CNDnoteq(51,255)) break p000e0177;
		if (!CNDonotzero(getFlag(51),3)) break p000e0177;
		if (!CNDabsent(getFlag(51))) break p000e0177;
 		ACCwriteln(123);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0178:
	{
 		if (skipdoall('p000e0178')) break p000e0178;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0178;
 		}
		if (!CNDnoteq(51,255)) break p000e0178;
		if (!CNDonotzero(getFlag(51),21)) break p000e0178;
		if (!CNDabsent(getFlag(51))) break p000e0178;
 		ACCwriteln(124);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0179:
	{
 		if (skipdoall('p000e0179')) break p000e0179;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0179;
 		}
		if (!CNDnoteq(34,255)) break p000e0179;
		if (!CNDeq(51,255)) break p000e0179;
 		ACCwriteln(125);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0180:
	{
 		if (skipdoall('p000e0180')) break p000e0180;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0180;
 		}
		if (!CNDnoteq(51,255)) break p000e0180;
		if (!CNDpresent(getFlag(51))) break p000e0180;
 		ACCwriteln(126);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIRA _
	p000e0181:
	{
 		if (skipdoall('p000e0181')) break p000e0181;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0181;
 		}
		if (!CNDnoteq(51,255)) break p000e0181;
		if (!CNDabsent(getFlag(51))) break p000e0181;
 		ACCwriteln(127);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0182:
	{
 		if (skipdoall('p000e0182')) break p000e0182;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0182;
 		}
		if (!CNDeq(34,255)) break p000e0182;
		if (!CNDbnotzero(12,1)) break p000e0182;
 		ACCwriteln(128);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0183:
	{
 		if (skipdoall('p000e0183')) break p000e0183;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0183;
 		}
		if (!CNDeq(34,255)) break p000e0183;
		if (!CNDbzero(12,1)) break p000e0183;
 		ACCwriteln(129);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0184:
	{
 		if (skipdoall('p000e0184')) break p000e0184;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0184;
 		}
		if (!CNDnoteq(51,255)) break p000e0184;
		if (!CNDonotzero(getFlag(51),3)) break p000e0184;
		if (!CNDpresent(getFlag(51))) break p000e0184;
 		ACCwriteln(130);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0185:
	{
 		if (skipdoall('p000e0185')) break p000e0185;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0185;
 		}
		if (!CNDnoteq(51,255)) break p000e0185;
		if (!CNDonotzero(getFlag(51),21)) break p000e0185;
		if (!CNDpresent(getFlag(51))) break p000e0185;
 		ACCwriteln(131);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0186:
	{
 		if (skipdoall('p000e0186')) break p000e0186;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0186;
 		}
		if (!CNDnoteq(51,255)) break p000e0186;
		if (!CNDonotzero(getFlag(51),3)) break p000e0186;
		if (!CNDabsent(getFlag(51))) break p000e0186;
 		ACCwriteln(132);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0187:
	{
 		if (skipdoall('p000e0187')) break p000e0187;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0187;
 		}
		if (!CNDnoteq(51,255)) break p000e0187;
		if (!CNDonotzero(getFlag(51),21)) break p000e0187;
		if (!CNDabsent(getFlag(51))) break p000e0187;
 		ACCwriteln(133);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0188:
	{
 		if (skipdoall('p000e0188')) break p000e0188;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0188;
 		}
		if (!CNDnoteq(34,255)) break p000e0188;
		if (!CNDeq(51,255)) break p000e0188;
 		ACCwriteln(134);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0189:
	{
 		if (skipdoall('p000e0189')) break p000e0189;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0189;
 		}
		if (!CNDnoteq(51,255)) break p000e0189;
		if (!CNDcarried(getFlag(51))) break p000e0189;
 		ACCwriteln(135);
 		ACCplace(getFlag(51),getFlag(38));
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0190:
	{
 		if (skipdoall('p000e0190')) break p000e0190;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0190;
 		}
		if (!CNDnoteq(51,255)) break p000e0190;
		if (!CNDnotcarr(getFlag(51))) break p000e0190;
 		ACCwriteln(136);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUCHA _
	p000e0191:
	{
 		if (skipdoall('p000e0191')) break p000e0191;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0191;
 		}
		if (!CNDnoteq(51,255)) break p000e0191;
		if (!CNDonotzero(getFlag(51),3)) break p000e0191;
		if (!CNDpresent(getFlag(51))) break p000e0191;
 		ACCwriteln(137);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUCHA _
	p000e0192:
	{
 		if (skipdoall('p000e0192')) break p000e0192;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0192;
 		}
		if (!CNDnoteq(51,255)) break p000e0192;
		if (!CNDonotzero(getFlag(51),3)) break p000e0192;
		if (!CNDabsent(getFlag(51))) break p000e0192;
 		ACCwriteln(138);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUCHA _
	p000e0193:
	{
 		if (skipdoall('p000e0193')) break p000e0193;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0193;
 		}
		if (!CNDeq(34,255)) break p000e0193;
 		ACCwriteln(139);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0194:
	{
 		if (skipdoall('p000e0194')) break p000e0194;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0194;
 		}
		if (!CNDeq(34,255)) break p000e0194;
		if (!CNDbnotzero(12,1)) break p000e0194;
 		ACCwriteln(140);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0195:
	{
 		if (skipdoall('p000e0195')) break p000e0195;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0195;
 		}
		if (!CNDeq(34,255)) break p000e0195;
		if (!CNDbzero(12,1)) break p000e0195;
 		ACCwriteln(141);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0196:
	{
 		if (skipdoall('p000e0196')) break p000e0196;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0196;
 		}
		if (!CNDnoteq(34,255)) break p000e0196;
		if (!CNDnoteq(51,255)) break p000e0196;
		if (!CNDonotzero(getFlag(51),3)) break p000e0196;
		if (!CNDpresent(getFlag(51))) break p000e0196;
 		ACCwriteln(142);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0197:
	{
 		if (skipdoall('p000e0197')) break p000e0197;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0197;
 		}
		if (!CNDnoteq(34,255)) break p000e0197;
		if (!CNDnoteq(51,255)) break p000e0197;
		if (!CNDonotzero(getFlag(51),21)) break p000e0197;
		if (!CNDpresent(getFlag(51))) break p000e0197;
 		ACCwriteln(143);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0198:
	{
 		if (skipdoall('p000e0198')) break p000e0198;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0198;
 		}
		if (!CNDnoteq(34,255)) break p000e0198;
		if (!CNDnoteq(51,255)) break p000e0198;
		if (!CNDonotzero(getFlag(51),3)) break p000e0198;
		if (!CNDabsent(getFlag(51))) break p000e0198;
 		ACCwriteln(144);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0199:
	{
 		if (skipdoall('p000e0199')) break p000e0199;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0199;
 		}
		if (!CNDnoteq(34,255)) break p000e0199;
		if (!CNDeq(51,255)) break p000e0199;
 		ACCwriteln(145);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0200:
	{
 		if (skipdoall('p000e0200')) break p000e0200;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0200;
 		}
		if (!CNDnoteq(51,255)) break p000e0200;
		if (!CNDpresent(getFlag(51))) break p000e0200;
		if (!CNDozero(getFlag(51),5)) break p000e0200;
 		ACCwriteln(146);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0201:
	{
 		if (skipdoall('p000e0201')) break p000e0201;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0201;
 		}
		if (!CNDnoteq(51,255)) break p000e0201;
		if (!CNDpresent(getFlag(51))) break p000e0201;
		if (!CNDonotzero(getFlag(51),5)) break p000e0201;
 		ACCwriteln(147);
 		ACCdestroy(getFlag(51));
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COME _
	p000e0202:
	{
 		if (skipdoall('p000e0202')) break p000e0202;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0202;
 		}
		if (!CNDnoteq(51,255)) break p000e0202;
		if (!CNDabsent(getFlag(51))) break p000e0202;
 		ACCwriteln(148);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0203:
	{
 		if (skipdoall('p000e0203')) break p000e0203;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0203;
 		}
		if (!CNDeq(34,255)) break p000e0203;
		if (!CNDbnotzero(12,1)) break p000e0203;
 		ACCwriteln(149);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0204:
	{
 		if (skipdoall('p000e0204')) break p000e0204;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0204;
 		}
		if (!CNDeq(34,255)) break p000e0204;
		if (!CNDbzero(12,1)) break p000e0204;
 		ACCwriteln(150);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0205:
	{
 		if (skipdoall('p000e0205')) break p000e0205;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0205;
 		}
		if (!CNDnoteq(34,255)) break p000e0205;
		if (!CNDnoteq(51,255)) break p000e0205;
		if (!CNDonotzero(getFlag(51),3)) break p000e0205;
		if (!CNDpresent(getFlag(51))) break p000e0205;
 		ACCwriteln(151);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0206:
	{
 		if (skipdoall('p000e0206')) break p000e0206;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0206;
 		}
		if (!CNDnoteq(34,255)) break p000e0206;
		if (!CNDnoteq(51,255)) break p000e0206;
		if (!CNDonotzero(getFlag(51),3)) break p000e0206;
		if (!CNDabsent(getFlag(51))) break p000e0206;
 		ACCwriteln(152);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0207:
	{
 		if (skipdoall('p000e0207')) break p000e0207;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0207;
 		}
		if (!CNDnoteq(34,255)) break p000e0207;
		if (!CNDeq(51,255)) break p000e0207;
 		ACCwriteln(153);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0208:
	{
 		if (skipdoall('p000e0208')) break p000e0208;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0208;
 		}
		if (!CNDnoteq(51,255)) break p000e0208;
		if (!CNDpresent(getFlag(51))) break p000e0208;
		if (!CNDozero(getFlag(51),6)) break p000e0208;
 		ACCwriteln(154);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0209:
	{
 		if (skipdoall('p000e0209')) break p000e0209;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0209;
 		}
		if (!CNDnoteq(51,255)) break p000e0209;
		if (!CNDpresent(getFlag(51))) break p000e0209;
		if (!CNDonotzero(getFlag(51),6)) break p000e0209;
 		ACCwriteln(155);
 		ACCdestroy(getFlag(51));
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BEBE _
	p000e0210:
	{
 		if (skipdoall('p000e0210')) break p000e0210;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0210;
 		}
		if (!CNDnoteq(51,255)) break p000e0210;
		if (!CNDabsent(getFlag(51))) break p000e0210;
 		ACCwriteln(156);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CAGA _
	p000e0211:
	{
 		if (skipdoall('p000e0211')) break p000e0211;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0211;
 		}
 		ACCwriteln(157);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUPE _
	p000e0212:
	{
 		if (skipdoall('p000e0212')) break p000e0212;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0212;
 		}
		if (!CNDeq(34,255)) break p000e0212;
		if (!CNDbnotzero(12,1)) break p000e0212;
 		ACCwriteln(158);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUPE _
	p000e0213:
	{
 		if (skipdoall('p000e0213')) break p000e0213;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0213;
 		}
		if (!CNDeq(34,255)) break p000e0213;
		if (!CNDbzero(12,1)) break p000e0213;
 		ACCwriteln(159);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUPE _
	p000e0214:
	{
 		if (skipdoall('p000e0214')) break p000e0214;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0214;
 		}
		if (!CNDnoteq(34,255)) break p000e0214;
		if (!CNDnoteq(51,255)) break p000e0214;
		if (!CNDonotzero(getFlag(51),3)) break p000e0214;
		if (!CNDpresent(getFlag(51))) break p000e0214;
 		ACCwriteln(160);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUPE _
	p000e0215:
	{
 		if (skipdoall('p000e0215')) break p000e0215;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0215;
 		}
		if (!CNDnoteq(34,255)) break p000e0215;
		if (!CNDnoteq(51,255)) break p000e0215;
		if (!CNDonotzero(getFlag(51),3)) break p000e0215;
		if (!CNDabsent(getFlag(51))) break p000e0215;
 		ACCwriteln(161);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUPE _
	p000e0216:
	{
 		if (skipdoall('p000e0216')) break p000e0216;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0216;
 		}
		if (!CNDnoteq(34,255)) break p000e0216;
		if (!CNDeq(51,255)) break p000e0216;
 		ACCwriteln(162);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUPE _
	p000e0217:
	{
 		if (skipdoall('p000e0217')) break p000e0217;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0217;
 		}
		if (!CNDnoteq(51,255)) break p000e0217;
		if (!CNDpresent(getFlag(51))) break p000e0217;
 		ACCwriteln(163);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCUPE _
	p000e0218:
	{
 		if (skipdoall('p000e0218')) break p000e0218;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0218;
 		}
		if (!CNDnoteq(51,255)) break p000e0218;
		if (!CNDabsent(getFlag(51))) break p000e0218;
 		ACCwriteln(164);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACARICIA _
	p000e0219:
	{
 		if (skipdoall('p000e0219')) break p000e0219;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0219;
 		}
		if (!CNDeq(34,255)) break p000e0219;
		if (!CNDbnotzero(12,1)) break p000e0219;
 		ACCwriteln(165);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACARICIA _
	p000e0220:
	{
 		if (skipdoall('p000e0220')) break p000e0220;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0220;
 		}
		if (!CNDeq(34,255)) break p000e0220;
		if (!CNDbzero(12,1)) break p000e0220;
 		ACCwriteln(166);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACARICIA _
	p000e0221:
	{
 		if (skipdoall('p000e0221')) break p000e0221;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0221;
 		}
		if (!CNDnoteq(34,255)) break p000e0221;
		if (!CNDnoteq(51,255)) break p000e0221;
		if (!CNDonotzero(getFlag(51),3)) break p000e0221;
		if (!CNDpresent(getFlag(51))) break p000e0221;
 		ACCwriteln(167);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACARICIA _
	p000e0222:
	{
 		if (skipdoall('p000e0222')) break p000e0222;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0222;
 		}
		if (!CNDnoteq(34,255)) break p000e0222;
		if (!CNDnoteq(51,255)) break p000e0222;
		if (!CNDonotzero(getFlag(51),3)) break p000e0222;
		if (!CNDabsent(getFlag(51))) break p000e0222;
 		ACCwriteln(168);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACARICIA _
	p000e0223:
	{
 		if (skipdoall('p000e0223')) break p000e0223;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0223;
 		}
		if (!CNDnoteq(34,255)) break p000e0223;
		if (!CNDeq(51,255)) break p000e0223;
 		ACCwriteln(169);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACARICIA _
	p000e0224:
	{
 		if (skipdoall('p000e0224')) break p000e0224;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0224;
 		}
		if (!CNDnoteq(51,255)) break p000e0224;
		if (!CNDpresent(getFlag(51))) break p000e0224;
 		ACCwriteln(170);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACARICIA _
	p000e0225:
	{
 		if (skipdoall('p000e0225')) break p000e0225;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0225;
 		}
		if (!CNDnoteq(51,255)) break p000e0225;
		if (!CNDabsent(getFlag(51))) break p000e0225;
 		ACCwriteln(171);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0226:
	{
 		if (skipdoall('p000e0226')) break p000e0226;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0226;
 		}
		if (!CNDeq(34,255)) break p000e0226;
		if (!CNDbnotzero(12,1)) break p000e0226;
 		ACCwriteln(172);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0227:
	{
 		if (skipdoall('p000e0227')) break p000e0227;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0227;
 		}
		if (!CNDeq(34,255)) break p000e0227;
		if (!CNDbzero(12,1)) break p000e0227;
 		ACCwriteln(173);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0228:
	{
 		if (skipdoall('p000e0228')) break p000e0228;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0228;
 		}
		if (!CNDnoteq(34,255)) break p000e0228;
		if (!CNDnoteq(51,255)) break p000e0228;
		if (!CNDonotzero(getFlag(51),3)) break p000e0228;
		if (!CNDpresent(getFlag(51))) break p000e0228;
 		ACCwriteln(174);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0229:
	{
 		if (skipdoall('p000e0229')) break p000e0229;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0229;
 		}
		if (!CNDnoteq(34,255)) break p000e0229;
		if (!CNDnoteq(51,255)) break p000e0229;
		if (!CNDonotzero(getFlag(51),3)) break p000e0229;
		if (!CNDabsent(getFlag(51))) break p000e0229;
 		ACCwriteln(175);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0230:
	{
 		if (skipdoall('p000e0230')) break p000e0230;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0230;
 		}
		if (!CNDnoteq(34,255)) break p000e0230;
		if (!CNDeq(51,255)) break p000e0230;
 		ACCwriteln(176);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0231:
	{
 		if (skipdoall('p000e0231')) break p000e0231;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0231;
 		}
		if (!CNDnoteq(51,255)) break p000e0231;
		if (!CNDpresent(getFlag(51))) break p000e0231;
		if (!CNDonotzero(getFlag(51),5)) break p000e0231;
 		ACCwriteln(177);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0232:
	{
 		if (skipdoall('p000e0232')) break p000e0232;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0232;
 		}
		if (!CNDnoteq(51,255)) break p000e0232;
		if (!CNDpresent(getFlag(51))) break p000e0232;
		if (!CNDonotzero(getFlag(51),6)) break p000e0232;
 		ACCwriteln(178);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0233:
	{
 		if (skipdoall('p000e0233')) break p000e0233;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0233;
 		}
		if (!CNDnoteq(51,255)) break p000e0233;
		if (!CNDpresent(getFlag(51))) break p000e0233;
 		ACCwriteln(179);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0234:
	{
 		if (skipdoall('p000e0234')) break p000e0234;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0234;
 		}
		if (!CNDnoteq(51,255)) break p000e0234;
		if (!CNDpresent(getFlag(51))) break p000e0234;
		if (!CNDonotzero(getFlag(51),5)) break p000e0234;
 		ACCwriteln(180);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HUELE _
	p000e0235:
	{
 		if (skipdoall('p000e0235')) break p000e0235;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0235;
 		}
		if (!CNDnoteq(51,255)) break p000e0235;
		if (!CNDabsent(getFlag(51))) break p000e0235;
 		ACCwriteln(181);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGITA _
	p000e0236:
	{
 		if (skipdoall('p000e0236')) break p000e0236;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0236;
 		}
		if (!CNDeq(34,255)) break p000e0236;
		if (!CNDbnotzero(12,1)) break p000e0236;
 		ACCwriteln(182);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGITA _
	p000e0237:
	{
 		if (skipdoall('p000e0237')) break p000e0237;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0237;
 		}
		if (!CNDeq(34,255)) break p000e0237;
		if (!CNDbzero(12,1)) break p000e0237;
 		ACCwriteln(183);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGITA _
	p000e0238:
	{
 		if (skipdoall('p000e0238')) break p000e0238;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0238;
 		}
		if (!CNDnoteq(34,255)) break p000e0238;
		if (!CNDnoteq(51,255)) break p000e0238;
		if (!CNDonotzero(getFlag(51),3)) break p000e0238;
		if (!CNDpresent(getFlag(51))) break p000e0238;
 		ACCwriteln(184);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGITA _
	p000e0239:
	{
 		if (skipdoall('p000e0239')) break p000e0239;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0239;
 		}
		if (!CNDnoteq(34,255)) break p000e0239;
		if (!CNDnoteq(51,255)) break p000e0239;
		if (!CNDonotzero(getFlag(51),3)) break p000e0239;
		if (!CNDabsent(getFlag(51))) break p000e0239;
 		ACCwriteln(185);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGITA _
	p000e0240:
	{
 		if (skipdoall('p000e0240')) break p000e0240;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0240;
 		}
		if (!CNDnoteq(34,255)) break p000e0240;
		if (!CNDeq(51,255)) break p000e0240;
 		ACCwriteln(186);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGITA _
	p000e0241:
	{
 		if (skipdoall('p000e0241')) break p000e0241;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0241;
 		}
		if (!CNDnoteq(51,255)) break p000e0241;
		if (!CNDpresent(getFlag(51))) break p000e0241;
 		ACCwriteln(187);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGITA _
	p000e0242:
	{
 		if (skipdoall('p000e0242')) break p000e0242;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0242;
 		}
		if (!CNDnoteq(51,255)) break p000e0242;
		if (!CNDabsent(getFlag(51))) break p000e0242;
 		ACCwriteln(188);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0243:
	{
 		if (skipdoall('p000e0243')) break p000e0243;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0243;
 		}
		if (!CNDeq(34,255)) break p000e0243;
		if (!CNDbnotzero(12,1)) break p000e0243;
 		ACCwriteln(189);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0244:
	{
 		if (skipdoall('p000e0244')) break p000e0244;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0244;
 		}
		if (!CNDeq(34,255)) break p000e0244;
		if (!CNDbzero(12,1)) break p000e0244;
 		ACCwriteln(190);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0245:
	{
 		if (skipdoall('p000e0245')) break p000e0245;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0245;
 		}
		if (!CNDnoteq(34,255)) break p000e0245;
		if (!CNDnoteq(51,255)) break p000e0245;
		if (!CNDonotzero(getFlag(51),3)) break p000e0245;
		if (!CNDpresent(getFlag(51))) break p000e0245;
 		ACCwriteln(191);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0246:
	{
 		if (skipdoall('p000e0246')) break p000e0246;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0246;
 		}
		if (!CNDnoteq(34,255)) break p000e0246;
		if (!CNDnoteq(51,255)) break p000e0246;
		if (!CNDonotzero(getFlag(51),3)) break p000e0246;
		if (!CNDabsent(getFlag(51))) break p000e0246;
 		ACCwriteln(192);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0247:
	{
 		if (skipdoall('p000e0247')) break p000e0247;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0247;
 		}
		if (!CNDnoteq(34,255)) break p000e0247;
		if (!CNDeq(51,255)) break p000e0247;
 		ACCwriteln(193);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0248:
	{
 		if (skipdoall('p000e0248')) break p000e0248;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0248;
 		}
		if (!CNDnoteq(51,255)) break p000e0248;
		if (!CNDpresent(getFlag(51))) break p000e0248;
 		ACCwriteln(194);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0249:
	{
 		if (skipdoall('p000e0249')) break p000e0249;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0249;
 		}
		if (!CNDnoteq(51,255)) break p000e0249;
		if (!CNDabsent(getFlag(51))) break p000e0249;
 		ACCwriteln(195);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESPERA _
	p000e0250:
	{
 		if (skipdoall('p000e0250')) break p000e0250;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0250;
 		}
 		ACCsysmess(35);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESCANSA _
	p000e0251:
	{
 		if (skipdoall('p000e0251')) break p000e0251;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0251;
 		}
 		ACCwriteln(196);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BRINCA _
	p000e0252:
	{
 		if (skipdoall('p000e0252')) break p000e0252;
 		if (in_response)
		{
			if (!CNDverb(45)) break p000e0252;
 		}
 		ACCwriteln(197);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ORA _
	p000e0253:
	{
 		if (skipdoall('p000e0253')) break p000e0253;
 		if (in_response)
		{
			if (!CNDverb(59)) break p000e0253;
 		}
 		ACCwriteln(198);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CAVA _
	p000e0254:
	{
 		if (skipdoall('p000e0254')) break p000e0254;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0254;
 		}
 		ACCwriteln(199);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PENSAR _
	p000e0255:
	{
 		if (skipdoall('p000e0255')) break p000e0255;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0255;
 		}
 		ACCwriteln(200);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESCANSA _
	p000e0256:
	{
 		if (skipdoall('p000e0256')) break p000e0256;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0256;
 		}
 		ACCwriteln(201);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CANTA _
	p000e0257:
	{
 		if (skipdoall('p000e0257')) break p000e0257;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0257;
 		}
 		ACCwriteln(202);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BAILA _
	p000e0258:
	{
 		if (skipdoall('p000e0258')) break p000e0258;
 		if (in_response)
		{
			if (!CNDverb(112)) break p000e0258;
 		}
 		ACCwriteln(203);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGREDE _
	p000e0259:
	{
 		if (skipdoall('p000e0259')) break p000e0259;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0259;
 		}
		if (!CNDeq(34,255)) break p000e0259;
		if (!CNDbnotzero(12,1)) break p000e0259;
 		ACCwriteln(204);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGREDE _
	p000e0260:
	{
 		if (skipdoall('p000e0260')) break p000e0260;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0260;
 		}
		if (!CNDeq(34,255)) break p000e0260;
		if (!CNDbzero(12,1)) break p000e0260;
 		ACCwriteln(205);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGREDE _
	p000e0261:
	{
 		if (skipdoall('p000e0261')) break p000e0261;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0261;
 		}
		if (!CNDnoteq(34,255)) break p000e0261;
		if (!CNDnoteq(51,255)) break p000e0261;
		if (!CNDonotzero(getFlag(51),3)) break p000e0261;
		if (!CNDpresent(getFlag(51))) break p000e0261;
 		ACCwriteln(206);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGREDE _
	p000e0262:
	{
 		if (skipdoall('p000e0262')) break p000e0262;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0262;
 		}
		if (!CNDnoteq(34,255)) break p000e0262;
		if (!CNDnoteq(51,255)) break p000e0262;
		if (!CNDonotzero(getFlag(51),3)) break p000e0262;
		if (!CNDabsent(getFlag(51))) break p000e0262;
 		ACCwriteln(207);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGREDE _
	p000e0263:
	{
 		if (skipdoall('p000e0263')) break p000e0263;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0263;
 		}
		if (!CNDnoteq(34,255)) break p000e0263;
		if (!CNDeq(51,255)) break p000e0263;
 		ACCwriteln(208);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGREDE _
	p000e0264:
	{
 		if (skipdoall('p000e0264')) break p000e0264;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0264;
 		}
		if (!CNDnoteq(51,255)) break p000e0264;
		if (!CNDpresent(getFlag(51))) break p000e0264;
 		ACCwriteln(209);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGREDE _
	p000e0265:
	{
 		if (skipdoall('p000e0265')) break p000e0265;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0265;
 		}
		if (!CNDnoteq(51,255)) break p000e0265;
		if (!CNDabsent(getFlag(51))) break p000e0265;
 		ACCwriteln(210);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0266:
	{
 		if (skipdoall('p000e0266')) break p000e0266;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0266;
 		}
		if (!CNDeq(34,255)) break p000e0266;
		if (!CNDbnotzero(12,1)) break p000e0266;
 		ACCwriteln(211);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0267:
	{
 		if (skipdoall('p000e0267')) break p000e0267;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0267;
 		}
		if (!CNDeq(34,255)) break p000e0267;
		if (!CNDbzero(12,1)) break p000e0267;
 		ACCwriteln(212);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0268:
	{
 		if (skipdoall('p000e0268')) break p000e0268;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0268;
 		}
		if (!CNDnoteq(34,255)) break p000e0268;
		if (!CNDnoteq(51,255)) break p000e0268;
		if (!CNDonotzero(getFlag(51),3)) break p000e0268;
		if (!CNDpresent(getFlag(51))) break p000e0268;
 		ACCwriteln(213);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0269:
	{
 		if (skipdoall('p000e0269')) break p000e0269;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0269;
 		}
		if (!CNDnoteq(34,255)) break p000e0269;
		if (!CNDnoteq(51,255)) break p000e0269;
		if (!CNDonotzero(getFlag(51),3)) break p000e0269;
		if (!CNDabsent(getFlag(51))) break p000e0269;
 		ACCwriteln(214);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0270:
	{
 		if (skipdoall('p000e0270')) break p000e0270;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0270;
 		}
		if (!CNDnoteq(34,255)) break p000e0270;
		if (!CNDeq(51,255)) break p000e0270;
 		ACCwriteln(215);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0271:
	{
 		if (skipdoall('p000e0271')) break p000e0271;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0271;
 		}
		if (!CNDnoteq(51,255)) break p000e0271;
		if (!CNDpresent(getFlag(51))) break p000e0271;
 		ACCwriteln(216);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0272:
	{
 		if (skipdoall('p000e0272')) break p000e0272;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0272;
 		}
		if (!CNDnoteq(51,255)) break p000e0272;
		if (!CNDabsent(getFlag(51))) break p000e0272;
 		ACCwriteln(217);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// USA _
	p000e0273:
	{
 		if (skipdoall('p000e0273')) break p000e0273;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0273;
 		}
 		ACCwriteln(218);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0274:
	{
 		if (skipdoall('p000e0274')) break p000e0274;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0274;
 		}
		if (!CNDprep(4)) break p000e0274;
		if (!CNDeq(34,255)) break p000e0274;
		if (!CNDbnotzero(12,1)) break p000e0274;
 		ACCwriteln(219);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0275:
	{
 		if (skipdoall('p000e0275')) break p000e0275;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0275;
 		}
		if (!CNDprep(4)) break p000e0275;
		if (!CNDeq(34,255)) break p000e0275;
		if (!CNDbzero(12,1)) break p000e0275;
 		ACCwriteln(220);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0276:
	{
 		if (skipdoall('p000e0276')) break p000e0276;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0276;
 		}
		if (!CNDeq(34,255)) break p000e0276;
		if (!CNDbnotzero(12,1)) break p000e0276;
 		ACCwriteln(221);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0277:
	{
 		if (skipdoall('p000e0277')) break p000e0277;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0277;
 		}
		if (!CNDeq(34,255)) break p000e0277;
		if (!CNDbzero(12,1)) break p000e0277;
 		ACCwriteln(222);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0278:
	{
 		if (skipdoall('p000e0278')) break p000e0278;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0278;
 		}
		if (!CNDprep(4)) break p000e0278;
		if (!CNDnoteq(51,255)) break p000e0278;
		if (!CNDonotzero(getFlag(51),3)) break p000e0278;
		if (!CNDpresent(getFlag(51))) break p000e0278;
 		ACCwriteln(223);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0279:
	{
 		if (skipdoall('p000e0279')) break p000e0279;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0279;
 		}
		if (!CNDnoteq(51,255)) break p000e0279;
		if (!CNDonotzero(getFlag(51),3)) break p000e0279;
		if (!CNDabsent(getFlag(51))) break p000e0279;
 		ACCwriteln(224);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0280:
	{
 		if (skipdoall('p000e0280')) break p000e0280;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0280;
 		}
		if (!CNDnoteq(34,255)) break p000e0280;
		if (!CNDnoteq(51,255)) break p000e0280;
		if (!CNDonotzero(getFlag(51),3)) break p000e0280;
		if (!CNDpresent(getFlag(51))) break p000e0280;
 		ACCwriteln(225);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0281:
	{
 		if (skipdoall('p000e0281')) break p000e0281;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0281;
 		}
		if (!CNDnoteq(34,255)) break p000e0281;
		if (!CNDnoteq(51,255)) break p000e0281;
		if (!CNDonotzero(getFlag(51),3)) break p000e0281;
		if (!CNDabsent(getFlag(51))) break p000e0281;
 		ACCwriteln(226);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0282:
	{
 		if (skipdoall('p000e0282')) break p000e0282;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0282;
 		}
		if (!CNDprep(4)) break p000e0282;
		if (!CNDnoteq(34,255)) break p000e0282;
		if (!CNDeq(51,255)) break p000e0282;
 		ACCwriteln(227);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0283:
	{
 		if (skipdoall('p000e0283')) break p000e0283;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0283;
 		}
		if (!CNDnoteq(34,255)) break p000e0283;
		if (!CNDeq(51,255)) break p000e0283;
 		ACCwriteln(228);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0284:
	{
 		if (skipdoall('p000e0284')) break p000e0284;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0284;
 		}
		if (!CNDnoteq(51,255)) break p000e0284;
		if (!CNDpresent(getFlag(51))) break p000e0284;
		if (!CNDonotzero(getFlag(51),2)) break p000e0284;
		if (!CNDonotzero(getFlag(51),16)) break p000e0284;
 		ACCobjat(getFlag(51),13);
		if (!CNDnotzero(13)) break p000e0284;
 		ACCwrite(229);
 		ACClistcontents(getFlag(51));
 		ACCwriteln(230);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0285:
	{
 		if (skipdoall('p000e0285')) break p000e0285;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0285;
 		}
		if (!CNDnoteq(51,255)) break p000e0285;
		if (!CNDpresent(getFlag(51))) break p000e0285;
		if (!CNDonotzero(getFlag(51),2)) break p000e0285;
		if (!CNDozero(getFlag(51),13)) break p000e0285;
 		ACCobjat(getFlag(51),13);
		if (!CNDnotzero(13)) break p000e0285;
 		ACCwrite(231);
 		ACClistcontents(getFlag(51));
 		ACCwriteln(232);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0286:
	{
 		if (skipdoall('p000e0286')) break p000e0286;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0286;
 		}
		if (!CNDnoteq(51,255)) break p000e0286;
		if (!CNDpresent(getFlag(51))) break p000e0286;
		if (!CNDonotzero(getFlag(51),2)) break p000e0286;
		if (!CNDonotzero(getFlag(51),13)) break p000e0286;
		if (!CNDonotzero(getFlag(51),14)) break p000e0286;
 		ACCobjat(getFlag(51),13);
		if (!CNDnotzero(13)) break p000e0286;
 		ACCwrite(233);
 		ACClistcontents(getFlag(51));
 		ACCwriteln(234);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0287:
	{
 		if (skipdoall('p000e0287')) break p000e0287;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0287;
 		}
		if (!CNDnoteq(51,255)) break p000e0287;
		if (!CNDpresent(getFlag(51))) break p000e0287;
		if (!CNDonotzero(getFlag(51),2)) break p000e0287;
		if (!CNDonotzero(getFlag(51),13)) break p000e0287;
		if (!CNDozero(getFlag(51),14)) break p000e0287;
 		ACCwriteln(235);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0288:
	{
 		if (skipdoall('p000e0288')) break p000e0288;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0288;
 		}
		if (!CNDnoteq(51,255)) break p000e0288;
		if (!CNDpresent(getFlag(51))) break p000e0288;
		if (!CNDonotzero(getFlag(51),2)) break p000e0288;
		if (!CNDonotzero(getFlag(51),9)) break p000e0288;
		if (!CNDozero(getFlag(51),10)) break p000e0288;
 		ACCwriteln(236);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0289:
	{
 		if (skipdoall('p000e0289')) break p000e0289;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0289;
 		}
		if (!CNDnoteq(51,255)) break p000e0289;
		if (!CNDpresent(getFlag(51))) break p000e0289;
		if (!CNDonotzero(getFlag(51),18)) break p000e0289;
 		ACCobjat(getFlag(51),13);
		if (!CNDnotzero(13)) break p000e0289;
 		ACCwrite(237);
 		ACClistcontents(getFlag(51));
 		ACCwriteln(238);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0290:
	{
 		if (skipdoall('p000e0290')) break p000e0290;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0290;
 		}
		if (!CNDnoteq(51,255)) break p000e0290;
		if (!CNDpresent(getFlag(51))) break p000e0290;
 		ACCwriteln(239);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0291:
	{
 		if (skipdoall('p000e0291')) break p000e0291;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0291;
 		}
		if (!CNDnoteq(51,255)) break p000e0291;
		if (!CNDabsent(getFlag(51))) break p000e0291;
 		ACCwriteln(240);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FREGAR _
	p000e0292:
	{
 		if (skipdoall('p000e0292')) break p000e0292;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0292;
 		}
		if (!CNDeq(34,255)) break p000e0292;
		if (!CNDbnotzero(12,1)) break p000e0292;
 		ACCwriteln(241);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FREGAR _
	p000e0293:
	{
 		if (skipdoall('p000e0293')) break p000e0293;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0293;
 		}
		if (!CNDeq(34,255)) break p000e0293;
		if (!CNDbzero(12,1)) break p000e0293;
 		ACCwriteln(242);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FREGAR _
	p000e0294:
	{
 		if (skipdoall('p000e0294')) break p000e0294;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0294;
 		}
		if (!CNDnoteq(34,255)) break p000e0294;
		if (!CNDnoteq(51,255)) break p000e0294;
		if (!CNDonotzero(getFlag(51),3)) break p000e0294;
		if (!CNDpresent(getFlag(51))) break p000e0294;
 		ACCwriteln(243);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FREGAR _
	p000e0295:
	{
 		if (skipdoall('p000e0295')) break p000e0295;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0295;
 		}
		if (!CNDnoteq(34,255)) break p000e0295;
		if (!CNDnoteq(51,255)) break p000e0295;
		if (!CNDonotzero(getFlag(51),3)) break p000e0295;
		if (!CNDabsent(getFlag(51))) break p000e0295;
 		ACCwriteln(244);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FREGAR _
	p000e0296:
	{
 		if (skipdoall('p000e0296')) break p000e0296;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0296;
 		}
		if (!CNDnoteq(34,255)) break p000e0296;
		if (!CNDeq(51,255)) break p000e0296;
 		ACCwriteln(245);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FREGAR _
	p000e0297:
	{
 		if (skipdoall('p000e0297')) break p000e0297;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0297;
 		}
		if (!CNDnoteq(51,255)) break p000e0297;
		if (!CNDpresent(getFlag(51))) break p000e0297;
 		ACCwriteln(246);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FREGAR _
	p000e0298:
	{
 		if (skipdoall('p000e0298')) break p000e0298;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0298;
 		}
		if (!CNDnoteq(51,255)) break p000e0298;
		if (!CNDabsent(getFlag(51))) break p000e0298;
 		ACCwriteln(247);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RASCA _
	p000e0299:
	{
 		if (skipdoall('p000e0299')) break p000e0299;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0299;
 		}
		if (!CNDeq(34,255)) break p000e0299;
		if (!CNDbnotzero(12,1)) break p000e0299;
 		ACCwriteln(248);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RASCA _
	p000e0300:
	{
 		if (skipdoall('p000e0300')) break p000e0300;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0300;
 		}
		if (!CNDeq(34,255)) break p000e0300;
		if (!CNDbzero(12,1)) break p000e0300;
 		ACCwriteln(249);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RASCA _
	p000e0301:
	{
 		if (skipdoall('p000e0301')) break p000e0301;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0301;
 		}
		if (!CNDnoteq(34,255)) break p000e0301;
		if (!CNDnoteq(51,255)) break p000e0301;
		if (!CNDonotzero(getFlag(51),3)) break p000e0301;
		if (!CNDpresent(getFlag(51))) break p000e0301;
 		ACCwriteln(250);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RASCA _
	p000e0302:
	{
 		if (skipdoall('p000e0302')) break p000e0302;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0302;
 		}
		if (!CNDnoteq(34,255)) break p000e0302;
		if (!CNDnoteq(51,255)) break p000e0302;
		if (!CNDonotzero(getFlag(51),3)) break p000e0302;
		if (!CNDabsent(getFlag(51))) break p000e0302;
 		ACCwriteln(251);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RASCA _
	p000e0303:
	{
 		if (skipdoall('p000e0303')) break p000e0303;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0303;
 		}
		if (!CNDnoteq(34,255)) break p000e0303;
		if (!CNDeq(51,255)) break p000e0303;
 		ACCwriteln(252);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RASCA _
	p000e0304:
	{
 		if (skipdoall('p000e0304')) break p000e0304;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0304;
 		}
		if (!CNDnoteq(51,255)) break p000e0304;
		if (!CNDpresent(getFlag(51))) break p000e0304;
 		ACCwriteln(253);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RASCA _
	p000e0305:
	{
 		if (skipdoall('p000e0305')) break p000e0305;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0305;
 		}
		if (!CNDnoteq(51,255)) break p000e0305;
		if (!CNDabsent(getFlag(51))) break p000e0305;
 		ACCwriteln(254);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENCENDER _
	p000e0306:
	{
 		if (skipdoall('p000e0306')) break p000e0306;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0306;
 		}
		if (!CNDeq(34,255)) break p000e0306;
		if (!CNDbnotzero(12,1)) break p000e0306;
 		ACCwriteln(255);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENCENDER _
	p000e0307:
	{
 		if (skipdoall('p000e0307')) break p000e0307;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0307;
 		}
		if (!CNDeq(34,255)) break p000e0307;
		if (!CNDbzero(12,1)) break p000e0307;
 		ACCwriteln(256);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENCENDER _
	p000e0308:
	{
 		if (skipdoall('p000e0308')) break p000e0308;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0308;
 		}
		if (!CNDnoteq(51,255)) break p000e0308;
		if (!CNDonotzero(getFlag(51),3)) break p000e0308;
		if (!CNDpresent(getFlag(51))) break p000e0308;
 		ACCwriteln(257);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENCENDER _
	p000e0309:
	{
 		if (skipdoall('p000e0309')) break p000e0309;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0309;
 		}
		if (!CNDnoteq(51,255)) break p000e0309;
		if (!CNDonotzero(getFlag(51),3)) break p000e0309;
		if (!CNDabsent(getFlag(51))) break p000e0309;
 		ACCwriteln(258);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENCENDER _
	p000e0310:
	{
 		if (skipdoall('p000e0310')) break p000e0310;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0310;
 		}
		if (!CNDnoteq(34,255)) break p000e0310;
		if (!CNDeq(51,255)) break p000e0310;
 		ACCwriteln(259);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENCENDER _
	p000e0311:
	{
 		if (skipdoall('p000e0311')) break p000e0311;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0311;
 		}
		if (!CNDnoteq(51,255)) break p000e0311;
		if (!CNDpresent(getFlag(51))) break p000e0311;
 		ACCwriteln(260);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENCENDER _
	p000e0312:
	{
 		if (skipdoall('p000e0312')) break p000e0312;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0312;
 		}
		if (!CNDnoteq(51,255)) break p000e0312;
		if (!CNDabsent(getFlag(51))) break p000e0312;
 		ACCwriteln(261);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APAGA _
	p000e0313:
	{
 		if (skipdoall('p000e0313')) break p000e0313;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0313;
 		}
		if (!CNDeq(34,255)) break p000e0313;
		if (!CNDbnotzero(12,1)) break p000e0313;
 		ACCwriteln(262);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APAGA _
	p000e0314:
	{
 		if (skipdoall('p000e0314')) break p000e0314;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0314;
 		}
		if (!CNDeq(34,255)) break p000e0314;
		if (!CNDbzero(12,1)) break p000e0314;
 		ACCwriteln(263);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APAGA _
	p000e0315:
	{
 		if (skipdoall('p000e0315')) break p000e0315;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0315;
 		}
		if (!CNDnoteq(34,255)) break p000e0315;
		if (!CNDeq(51,255)) break p000e0315;
 		ACCwriteln(264);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APAGA _
	p000e0316:
	{
 		if (skipdoall('p000e0316')) break p000e0316;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0316;
 		}
		if (!CNDnoteq(51,255)) break p000e0316;
		if (!CNDpresent(getFlag(51))) break p000e0316;
 		ACCwriteln(265);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APAGA _
	p000e0317:
	{
 		if (skipdoall('p000e0317')) break p000e0317;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0317;
 		}
		if (!CNDnoteq(51,255)) break p000e0317;
		if (!CNDabsent(getFlag(51))) break p000e0317;
 		ACCwriteln(266);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0318:
	{
 		if (skipdoall('p000e0318')) break p000e0318;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0318;
 		}
		if (!CNDeq(34,255)) break p000e0318;
		if (!CNDbnotzero(12,1)) break p000e0318;
 		ACCwriteln(267);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0319:
	{
 		if (skipdoall('p000e0319')) break p000e0319;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0319;
 		}
		if (!CNDeq(34,255)) break p000e0319;
		if (!CNDbzero(12,1)) break p000e0319;
 		ACCwriteln(268);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0320:
	{
 		if (skipdoall('p000e0320')) break p000e0320;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0320;
 		}
		if (!CNDnoteq(51,255)) break p000e0320;
		if (!CNDonotzero(getFlag(51),3)) break p000e0320;
		if (!CNDpresent(getFlag(51))) break p000e0320;
 		ACCwriteln(269);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0321:
	{
 		if (skipdoall('p000e0321')) break p000e0321;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0321;
 		}
		if (!CNDnoteq(51,255)) break p000e0321;
		if (!CNDonotzero(getFlag(51),21)) break p000e0321;
		if (!CNDpresent(getFlag(51))) break p000e0321;
 		ACCwriteln(270);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0322:
	{
 		if (skipdoall('p000e0322')) break p000e0322;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0322;
 		}
		if (!CNDnoteq(51,255)) break p000e0322;
		if (!CNDonotzero(getFlag(51),3)) break p000e0322;
		if (!CNDabsent(getFlag(51))) break p000e0322;
 		ACCwriteln(271);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0323:
	{
 		if (skipdoall('p000e0323')) break p000e0323;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0323;
 		}
		if (!CNDnoteq(34,255)) break p000e0323;
		if (!CNDeq(51,255)) break p000e0323;
 		ACCwriteln(272);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0324:
	{
 		if (skipdoall('p000e0324')) break p000e0324;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0324;
 		}
		if (!CNDnoteq(51,255)) break p000e0324;
		if (!CNDpresent(getFlag(51))) break p000e0324;
 		ACCwriteln(273);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0325:
	{
 		if (skipdoall('p000e0325')) break p000e0325;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0325;
 		}
		if (!CNDnoteq(51,255)) break p000e0325;
		if (!CNDabsent(getFlag(51))) break p000e0325;
 		ACCwriteln(274);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0326:
	{
 		if (skipdoall('p000e0326')) break p000e0326;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0326;
 		}
		if (!CNDeq(34,255)) break p000e0326;
		if (!CNDbnotzero(12,1)) break p000e0326;
 		ACCwriteln(275);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0327:
	{
 		if (skipdoall('p000e0327')) break p000e0327;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0327;
 		}
		if (!CNDeq(34,255)) break p000e0327;
		if (!CNDbzero(12,1)) break p000e0327;
 		ACCwriteln(276);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0328:
	{
 		if (skipdoall('p000e0328')) break p000e0328;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0328;
 		}
		if (!CNDnoteq(51,255)) break p000e0328;
		if (!CNDonotzero(getFlag(51),3)) break p000e0328;
		if (!CNDpresent(getFlag(51))) break p000e0328;
 		ACCwriteln(277);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0329:
	{
 		if (skipdoall('p000e0329')) break p000e0329;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0329;
 		}
		if (!CNDnoteq(51,255)) break p000e0329;
		if (!CNDonotzero(getFlag(51),3)) break p000e0329;
		if (!CNDabsent(getFlag(51))) break p000e0329;
 		ACCwriteln(278);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0330:
	{
 		if (skipdoall('p000e0330')) break p000e0330;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0330;
 		}
		if (!CNDnoteq(34,255)) break p000e0330;
		if (!CNDeq(51,255)) break p000e0330;
 		ACCwriteln(279);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0331:
	{
 		if (skipdoall('p000e0331')) break p000e0331;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0331;
 		}
		if (!CNDnoteq(51,255)) break p000e0331;
		if (!CNDpresent(getFlag(51))) break p000e0331;
 		ACCwriteln(280);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0332:
	{
 		if (skipdoall('p000e0332')) break p000e0332;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0332;
 		}
		if (!CNDnoteq(51,255)) break p000e0332;
		if (!CNDabsent(getFlag(51))) break p000e0332;
 		ACCwriteln(281);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESATA _
	p000e0333:
	{
 		if (skipdoall('p000e0333')) break p000e0333;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0333;
 		}
		if (!CNDeq(34,255)) break p000e0333;
		if (!CNDbnotzero(12,1)) break p000e0333;
 		ACCwriteln(282);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESATA _
	p000e0334:
	{
 		if (skipdoall('p000e0334')) break p000e0334;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0334;
 		}
		if (!CNDeq(34,255)) break p000e0334;
		if (!CNDbzero(12,1)) break p000e0334;
 		ACCwriteln(283);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESATA _
	p000e0335:
	{
 		if (skipdoall('p000e0335')) break p000e0335;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0335;
 		}
		if (!CNDnoteq(51,255)) break p000e0335;
		if (!CNDonotzero(getFlag(51),3)) break p000e0335;
		if (!CNDpresent(getFlag(51))) break p000e0335;
 		ACCwriteln(284);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESATA _
	p000e0336:
	{
 		if (skipdoall('p000e0336')) break p000e0336;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0336;
 		}
		if (!CNDnoteq(51,255)) break p000e0336;
		if (!CNDonotzero(getFlag(51),3)) break p000e0336;
		if (!CNDabsent(getFlag(51))) break p000e0336;
 		ACCwriteln(285);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESATA _
	p000e0337:
	{
 		if (skipdoall('p000e0337')) break p000e0337;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0337;
 		}
		if (!CNDnoteq(34,255)) break p000e0337;
		if (!CNDeq(51,255)) break p000e0337;
 		ACCwriteln(286);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESATA _
	p000e0338:
	{
 		if (skipdoall('p000e0338')) break p000e0338;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0338;
 		}
		if (!CNDnoteq(51,255)) break p000e0338;
		if (!CNDpresent(getFlag(51))) break p000e0338;
 		ACCwriteln(287);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESATA _
	p000e0339:
	{
 		if (skipdoall('p000e0339')) break p000e0339;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0339;
 		}
		if (!CNDnoteq(51,255)) break p000e0339;
		if (!CNDabsent(getFlag(51))) break p000e0339;
 		ACCwriteln(288);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLENA _
	p000e0340:
	{
 		if (skipdoall('p000e0340')) break p000e0340;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0340;
 		}
		if (!CNDeq(34,255)) break p000e0340;
		if (!CNDbnotzero(12,1)) break p000e0340;
 		ACCwriteln(289);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLENA _
	p000e0341:
	{
 		if (skipdoall('p000e0341')) break p000e0341;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0341;
 		}
		if (!CNDeq(34,255)) break p000e0341;
		if (!CNDbzero(12,1)) break p000e0341;
 		ACCwriteln(290);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLENA _
	p000e0342:
	{
 		if (skipdoall('p000e0342')) break p000e0342;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0342;
 		}
		if (!CNDnoteq(34,255)) break p000e0342;
		if (!CNDeq(51,255)) break p000e0342;
 		ACCwriteln(291);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLENA _
	p000e0343:
	{
 		if (skipdoall('p000e0343')) break p000e0343;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0343;
 		}
		if (!CNDnoteq(51,255)) break p000e0343;
		if (!CNDpresent(getFlag(51))) break p000e0343;
		if (!CNDonotzero(getFlag(51),2)) break p000e0343;
 		ACCwriteln(292);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLENA _
	p000e0344:
	{
 		if (skipdoall('p000e0344')) break p000e0344;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0344;
 		}
		if (!CNDnoteq(51,255)) break p000e0344;
		if (!CNDpresent(getFlag(51))) break p000e0344;
		if (!CNDozero(getFlag(51),2)) break p000e0344;
 		ACCwriteln(293);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLENA _
	p000e0345:
	{
 		if (skipdoall('p000e0345')) break p000e0345;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0345;
 		}
		if (!CNDnoteq(51,255)) break p000e0345;
		if (!CNDabsent(getFlag(51))) break p000e0345;
 		ACCwriteln(294);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// NADA _
	p000e0346:
	{
 		if (skipdoall('p000e0346')) break p000e0346;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0346;
 		}
 		ACCwriteln(295);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA _
	p000e0347:
	{
 		if (skipdoall('p000e0347')) break p000e0347;
 		if (in_response)
		{
			if (!CNDverb(57)) break p000e0347;
 		}
 		ACCwriteln(296);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RETORCER _
	p000e0348:
	{
 		if (skipdoall('p000e0348')) break p000e0348;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0348;
 		}
		if (!CNDeq(34,255)) break p000e0348;
		if (!CNDbnotzero(12,1)) break p000e0348;
 		ACCwriteln(297);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RETORCER _
	p000e0349:
	{
 		if (skipdoall('p000e0349')) break p000e0349;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0349;
 		}
		if (!CNDeq(34,255)) break p000e0349;
		if (!CNDbzero(12,1)) break p000e0349;
 		ACCwriteln(298);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RETORCER _
	p000e0350:
	{
 		if (skipdoall('p000e0350')) break p000e0350;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0350;
 		}
		if (!CNDnoteq(51,255)) break p000e0350;
		if (!CNDonotzero(getFlag(51),3)) break p000e0350;
		if (!CNDpresent(getFlag(51))) break p000e0350;
 		ACCwriteln(299);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RETORCER _
	p000e0351:
	{
 		if (skipdoall('p000e0351')) break p000e0351;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0351;
 		}
		if (!CNDnoteq(51,255)) break p000e0351;
		if (!CNDonotzero(getFlag(51),3)) break p000e0351;
		if (!CNDabsent(getFlag(51))) break p000e0351;
 		ACCwriteln(300);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RETORCER _
	p000e0352:
	{
 		if (skipdoall('p000e0352')) break p000e0352;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0352;
 		}
		if (!CNDnoteq(34,255)) break p000e0352;
		if (!CNDeq(51,255)) break p000e0352;
 		ACCwriteln(301);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RETORCER _
	p000e0353:
	{
 		if (skipdoall('p000e0353')) break p000e0353;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0353;
 		}
		if (!CNDnoteq(51,255)) break p000e0353;
		if (!CNDpresent(getFlag(51))) break p000e0353;
 		ACCwriteln(302);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RETORCER _
	p000e0354:
	{
 		if (skipdoall('p000e0354')) break p000e0354;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0354;
 		}
		if (!CNDnoteq(51,255)) break p000e0354;
		if (!CNDabsent(getFlag(51))) break p000e0354;
 		ACCwriteln(303);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0355:
	{
 		if (skipdoall('p000e0355')) break p000e0355;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0355;
 		}
		if (!CNDeq(34,255)) break p000e0355;
		if (!CNDbnotzero(12,1)) break p000e0355;
 		ACCwriteln(304);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0356:
	{
 		if (skipdoall('p000e0356')) break p000e0356;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0356;
 		}
		if (!CNDeq(34,255)) break p000e0356;
		if (!CNDbzero(12,1)) break p000e0356;
 		ACCwriteln(305);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0357:
	{
 		if (skipdoall('p000e0357')) break p000e0357;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0357;
 		}
		if (!CNDnoteq(51,255)) break p000e0357;
		if (!CNDonotzero(getFlag(51),3)) break p000e0357;
		if (!CNDpresent(getFlag(51))) break p000e0357;
 		ACCwriteln(306);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0358:
	{
 		if (skipdoall('p000e0358')) break p000e0358;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0358;
 		}
		if (!CNDnoteq(51,255)) break p000e0358;
		if (!CNDonotzero(getFlag(51),3)) break p000e0358;
		if (!CNDabsent(getFlag(51))) break p000e0358;
 		ACCwriteln(307);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0359:
	{
 		if (skipdoall('p000e0359')) break p000e0359;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0359;
 		}
		if (!CNDnoteq(34,255)) break p000e0359;
		if (!CNDeq(51,255)) break p000e0359;
 		ACCwriteln(308);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0360:
	{
 		if (skipdoall('p000e0360')) break p000e0360;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0360;
 		}
		if (!CNDnoteq(51,255)) break p000e0360;
		if (!CNDpresent(getFlag(51))) break p000e0360;
 		ACCwriteln(309);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0361:
	{
 		if (skipdoall('p000e0361')) break p000e0361;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0361;
 		}
		if (!CNDnoteq(51,255)) break p000e0361;
		if (!CNDabsent(getFlag(51))) break p000e0361;
 		ACCwriteln(310);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BESA _
	p000e0362:
	{
 		if (skipdoall('p000e0362')) break p000e0362;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0362;
 		}
		if (!CNDeq(34,255)) break p000e0362;
		if (!CNDbnotzero(12,1)) break p000e0362;
 		ACCwriteln(311);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BESA _
	p000e0363:
	{
 		if (skipdoall('p000e0363')) break p000e0363;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0363;
 		}
		if (!CNDeq(34,255)) break p000e0363;
		if (!CNDbzero(12,1)) break p000e0363;
 		ACCwriteln(312);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BESA _
	p000e0364:
	{
 		if (skipdoall('p000e0364')) break p000e0364;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0364;
 		}
		if (!CNDnoteq(34,255)) break p000e0364;
		if (!CNDnoteq(51,255)) break p000e0364;
		if (!CNDonotzero(getFlag(51),3)) break p000e0364;
		if (!CNDpresent(getFlag(51))) break p000e0364;
 		ACCwriteln(313);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BESA _
	p000e0365:
	{
 		if (skipdoall('p000e0365')) break p000e0365;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0365;
 		}
		if (!CNDnoteq(34,255)) break p000e0365;
		if (!CNDnoteq(51,255)) break p000e0365;
		if (!CNDonotzero(getFlag(51),3)) break p000e0365;
		if (!CNDabsent(getFlag(51))) break p000e0365;
 		ACCwriteln(314);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BESA _
	p000e0366:
	{
 		if (skipdoall('p000e0366')) break p000e0366;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0366;
 		}
		if (!CNDnoteq(34,255)) break p000e0366;
		if (!CNDeq(51,255)) break p000e0366;
 		ACCwriteln(315);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BESA _
	p000e0367:
	{
 		if (skipdoall('p000e0367')) break p000e0367;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0367;
 		}
		if (!CNDnoteq(51,255)) break p000e0367;
		if (!CNDpresent(getFlag(51))) break p000e0367;
 		ACCwriteln(316);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BESA _
	p000e0368:
	{
 		if (skipdoall('p000e0368')) break p000e0368;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0368;
 		}
		if (!CNDnoteq(51,255)) break p000e0368;
		if (!CNDabsent(getFlag(51))) break p000e0368;
 		ACCwriteln(317);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRAZA _
	p000e0369:
	{
 		if (skipdoall('p000e0369')) break p000e0369;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0369;
 		}
		if (!CNDeq(34,255)) break p000e0369;
		if (!CNDbnotzero(12,1)) break p000e0369;
 		ACCwriteln(318);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRAZA _
	p000e0370:
	{
 		if (skipdoall('p000e0370')) break p000e0370;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0370;
 		}
		if (!CNDeq(34,255)) break p000e0370;
		if (!CNDbzero(12,1)) break p000e0370;
 		ACCwriteln(319);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRAZA _
	p000e0371:
	{
 		if (skipdoall('p000e0371')) break p000e0371;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0371;
 		}
		if (!CNDnoteq(34,255)) break p000e0371;
		if (!CNDnoteq(51,255)) break p000e0371;
		if (!CNDonotzero(getFlag(51),3)) break p000e0371;
		if (!CNDpresent(getFlag(51))) break p000e0371;
 		ACCwriteln(320);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRAZA _
	p000e0372:
	{
 		if (skipdoall('p000e0372')) break p000e0372;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0372;
 		}
		if (!CNDnoteq(34,255)) break p000e0372;
		if (!CNDnoteq(51,255)) break p000e0372;
		if (!CNDonotzero(getFlag(51),3)) break p000e0372;
		if (!CNDabsent(getFlag(51))) break p000e0372;
 		ACCwriteln(321);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRAZA _
	p000e0373:
	{
 		if (skipdoall('p000e0373')) break p000e0373;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0373;
 		}
		if (!CNDnoteq(34,255)) break p000e0373;
		if (!CNDeq(51,255)) break p000e0373;
 		ACCwriteln(322);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRAZA _
	p000e0374:
	{
 		if (skipdoall('p000e0374')) break p000e0374;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0374;
 		}
		if (!CNDnoteq(51,255)) break p000e0374;
		if (!CNDpresent(getFlag(51))) break p000e0374;
 		ACCwriteln(323);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRAZA _
	p000e0375:
	{
 		if (skipdoall('p000e0375')) break p000e0375;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0375;
 		}
		if (!CNDnoteq(51,255)) break p000e0375;
		if (!CNDabsent(getFlag(51))) break p000e0375;
 		ACCwriteln(324);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACTIVA _
	p000e0376:
	{
 		if (skipdoall('p000e0376')) break p000e0376;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0376;
 		}
		if (!CNDeq(34,255)) break p000e0376;
		if (!CNDbnotzero(12,1)) break p000e0376;
 		ACCwriteln(325);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACTIVA _
	p000e0377:
	{
 		if (skipdoall('p000e0377')) break p000e0377;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0377;
 		}
		if (!CNDeq(34,255)) break p000e0377;
		if (!CNDbzero(12,1)) break p000e0377;
 		ACCwriteln(326);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACTIVA _
	p000e0378:
	{
 		if (skipdoall('p000e0378')) break p000e0378;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0378;
 		}
		if (!CNDnoteq(34,255)) break p000e0378;
		if (!CNDeq(51,255)) break p000e0378;
 		ACCwriteln(327);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACTIVA _
	p000e0379:
	{
 		if (skipdoall('p000e0379')) break p000e0379;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0379;
 		}
		if (!CNDnoteq(51,255)) break p000e0379;
		if (!CNDpresent(getFlag(51))) break p000e0379;
		if (!CNDonotzero(getFlag(51),19)) break p000e0379;
		if (!CNDozero(getFlag(51),20)) break p000e0379;
 		ACCoset(getFlag(51),20);
 		ACCwriteln(328);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACTIVA _
	p000e0380:
	{
 		if (skipdoall('p000e0380')) break p000e0380;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0380;
 		}
		if (!CNDnoteq(51,255)) break p000e0380;
		if (!CNDpresent(getFlag(51))) break p000e0380;
		if (!CNDonotzero(getFlag(51),19)) break p000e0380;
		if (!CNDonotzero(getFlag(51),20)) break p000e0380;
 		ACCwriteln(329);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACTIVA _
	p000e0381:
	{
 		if (skipdoall('p000e0381')) break p000e0381;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0381;
 		}
		if (!CNDnoteq(51,255)) break p000e0381;
		if (!CNDpresent(getFlag(51))) break p000e0381;
 		ACCwriteln(330);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACTIVA _
	p000e0382:
	{
 		if (skipdoall('p000e0382')) break p000e0382;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0382;
 		}
		if (!CNDnoteq(51,255)) break p000e0382;
		if (!CNDabsent(getFlag(51))) break p000e0382;
 		ACCwriteln(331);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESACTIVA _
	p000e0383:
	{
 		if (skipdoall('p000e0383')) break p000e0383;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0383;
 		}
		if (!CNDeq(34,255)) break p000e0383;
		if (!CNDbnotzero(12,1)) break p000e0383;
 		ACCwriteln(332);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESACTIVA _
	p000e0384:
	{
 		if (skipdoall('p000e0384')) break p000e0384;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0384;
 		}
		if (!CNDeq(34,255)) break p000e0384;
		if (!CNDbzero(12,1)) break p000e0384;
 		ACCwriteln(333);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESACTIVA _
	p000e0385:
	{
 		if (skipdoall('p000e0385')) break p000e0385;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0385;
 		}
		if (!CNDnoteq(34,255)) break p000e0385;
		if (!CNDeq(51,255)) break p000e0385;
 		ACCwriteln(334);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESACTIVA _
	p000e0386:
	{
 		if (skipdoall('p000e0386')) break p000e0386;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0386;
 		}
		if (!CNDnoteq(51,255)) break p000e0386;
		if (!CNDpresent(getFlag(51))) break p000e0386;
		if (!CNDonotzero(getFlag(51),19)) break p000e0386;
		if (!CNDonotzero(getFlag(51),20)) break p000e0386;
 		ACCoclear(getFlag(51),20);
 		ACCwriteln(335);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESACTIVA _
	p000e0387:
	{
 		if (skipdoall('p000e0387')) break p000e0387;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0387;
 		}
		if (!CNDnoteq(51,255)) break p000e0387;
		if (!CNDpresent(getFlag(51))) break p000e0387;
		if (!CNDonotzero(getFlag(51),19)) break p000e0387;
		if (!CNDozero(getFlag(51),20)) break p000e0387;
 		ACCwriteln(336);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESACTIVA _
	p000e0388:
	{
 		if (skipdoall('p000e0388')) break p000e0388;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0388;
 		}
		if (!CNDnoteq(51,255)) break p000e0388;
		if (!CNDpresent(getFlag(51))) break p000e0388;
 		ACCwriteln(337);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESACTIVA _
	p000e0389:
	{
 		if (skipdoall('p000e0389')) break p000e0389;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0389;
 		}
		if (!CNDnoteq(51,255)) break p000e0389;
		if (!CNDabsent(getFlag(51))) break p000e0389;
 		ACCwriteln(338);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0390:
	{
 		if (skipdoall('p000e0390')) break p000e0390;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0390;
 		}
		if (!CNDeq(34,255)) break p000e0390;
		if (!CNDbnotzero(12,1)) break p000e0390;
 		ACCwriteln(339);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0391:
	{
 		if (skipdoall('p000e0391')) break p000e0391;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0391;
 		}
		if (!CNDeq(34,255)) break p000e0391;
		if (!CNDbzero(12,1)) break p000e0391;
 		ACCwriteln(340);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0392:
	{
 		if (skipdoall('p000e0392')) break p000e0392;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0392;
 		}
		if (!CNDnoteq(34,255)) break p000e0392;
		if (!CNDeq(51,255)) break p000e0392;
 		ACCwriteln(341);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0393:
	{
 		if (skipdoall('p000e0393')) break p000e0393;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0393;
 		}
		if (!CNDnoteq(51,255)) break p000e0393;
		if (!CNDpresent(getFlag(51))) break p000e0393;
		if (!CNDonotzero(getFlag(51),13)) break p000e0393;
		if (!CNDozero(getFlag(51),14)) break p000e0393;
		if (!CNDozero(getFlag(51),10)) break p000e0393;
 		ACCoset(getFlag(51),14);
 		ACCwriteln(342);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0394:
	{
 		if (skipdoall('p000e0394')) break p000e0394;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0394;
 		}
		if (!CNDnoteq(51,255)) break p000e0394;
		if (!CNDpresent(getFlag(51))) break p000e0394;
		if (!CNDonotzero(getFlag(51),13)) break p000e0394;
		if (!CNDozero(getFlag(51),14)) break p000e0394;
		if (!CNDonotzero(getFlag(51),10)) break p000e0394;
 		ACCwriteln(343);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0395:
	{
 		if (skipdoall('p000e0395')) break p000e0395;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0395;
 		}
		if (!CNDnoteq(51,255)) break p000e0395;
		if (!CNDpresent(getFlag(51))) break p000e0395;
		if (!CNDonotzero(getFlag(51),13)) break p000e0395;
		if (!CNDonotzero(getFlag(51),14)) break p000e0395;
 		ACCwriteln(344);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0396:
	{
 		if (skipdoall('p000e0396')) break p000e0396;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0396;
 		}
		if (!CNDnoteq(51,255)) break p000e0396;
		if (!CNDpresent(getFlag(51))) break p000e0396;
		if (!CNDonotzero(getFlag(51),13)) break p000e0396;
		if (!CNDonotzero(getFlag(51),14)) break p000e0396;
 		ACCwriteln(345);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0397:
	{
 		if (skipdoall('p000e0397')) break p000e0397;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0397;
 		}
		if (!CNDnoteq(51,255)) break p000e0397;
		if (!CNDpresent(getFlag(51))) break p000e0397;
 		ACCwriteln(346);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0398:
	{
 		if (skipdoall('p000e0398')) break p000e0398;
 		if (in_response)
		{
			if (!CNDverb(64)) break p000e0398;
 		}
		if (!CNDnoteq(51,255)) break p000e0398;
		if (!CNDabsent(getFlag(51))) break p000e0398;
 		ACCwriteln(347);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0399:
	{
 		if (skipdoall('p000e0399')) break p000e0399;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0399;
 		}
		if (!CNDeq(34,255)) break p000e0399;
 		ACCwriteln(348);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0400:
	{
 		if (skipdoall('p000e0400')) break p000e0400;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0400;
 		}
		if (!CNDeq(34,255)) break p000e0400;
		if (!CNDbzero(12,1)) break p000e0400;
 		ACCwriteln(349);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0401:
	{
 		if (skipdoall('p000e0401')) break p000e0401;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0401;
 		}
		if (!CNDnoteq(34,255)) break p000e0401;
		if (!CNDeq(51,255)) break p000e0401;
 		ACCwriteln(350);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0402:
	{
 		if (skipdoall('p000e0402')) break p000e0402;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0402;
 		}
		if (!CNDnoteq(51,255)) break p000e0402;
		if (!CNDpresent(getFlag(51))) break p000e0402;
		if (!CNDonotzero(getFlag(51),13)) break p000e0402;
		if (!CNDonotzero(getFlag(51),14)) break p000e0402;
 		ACCoclear(getFlag(51),14);
 		ACCwriteln(351);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0403:
	{
 		if (skipdoall('p000e0403')) break p000e0403;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0403;
 		}
		if (!CNDnoteq(51,255)) break p000e0403;
		if (!CNDpresent(getFlag(51))) break p000e0403;
		if (!CNDonotzero(getFlag(51),13)) break p000e0403;
		if (!CNDozero(getFlag(51),14)) break p000e0403;
 		ACCwriteln(352);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0404:
	{
 		if (skipdoall('p000e0404')) break p000e0404;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0404;
 		}
		if (!CNDnoteq(51,255)) break p000e0404;
		if (!CNDpresent(getFlag(51))) break p000e0404;
 		ACCwriteln(353);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0405:
	{
 		if (skipdoall('p000e0405')) break p000e0405;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0405;
 		}
		if (!CNDnoteq(51,255)) break p000e0405;
		if (!CNDabsent(getFlag(51))) break p000e0405;
 		ACCwriteln(354);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEE _
	p000e0406:
	{
 		if (skipdoall('p000e0406')) break p000e0406;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0406;
 		}
		if (!CNDeq(34,255)) break p000e0406;
		if (!CNDbnotzero(12,1)) break p000e0406;
 		ACCwriteln(355);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEE _
	p000e0407:
	{
 		if (skipdoall('p000e0407')) break p000e0407;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0407;
 		}
		if (!CNDeq(34,255)) break p000e0407;
		if (!CNDbzero(12,1)) break p000e0407;
 		ACCwriteln(356);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEE _
	p000e0408:
	{
 		if (skipdoall('p000e0408')) break p000e0408;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0408;
 		}
		if (!CNDnoteq(34,255)) break p000e0408;
		if (!CNDeq(51,255)) break p000e0408;
 		ACCwriteln(357);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEE _
	p000e0409:
	{
 		if (skipdoall('p000e0409')) break p000e0409;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0409;
 		}
		if (!CNDnoteq(51,255)) break p000e0409;
		if (!CNDpresent(getFlag(51))) break p000e0409;
 		ACCwriteln(358);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEE _
	p000e0410:
	{
 		if (skipdoall('p000e0410')) break p000e0410;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0410;
 		}
		if (!CNDnoteq(51,255)) break p000e0410;
		if (!CNDabsent(getFlag(51))) break p000e0410;
 		ACCwriteln(359);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0411:
	{
 		if (skipdoall('p000e0411')) break p000e0411;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0411;
 		}
		if (!CNDeq(34,255)) break p000e0411;
		if (!CNDbnotzero(12,1)) break p000e0411;
 		ACCwriteln(360);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0412:
	{
 		if (skipdoall('p000e0412')) break p000e0412;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0412;
 		}
		if (!CNDeq(34,255)) break p000e0412;
		if (!CNDbzero(12,1)) break p000e0412;
 		ACCwriteln(361);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0413:
	{
 		if (skipdoall('p000e0413')) break p000e0413;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0413;
 		}
		if (!CNDnoteq(51,255)) break p000e0413;
		if (!CNDonotzero(getFlag(51),3)) break p000e0413;
		if (!CNDpresent(getFlag(51))) break p000e0413;
 		ACCwriteln(362);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0414:
	{
 		if (skipdoall('p000e0414')) break p000e0414;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0414;
 		}
		if (!CNDnoteq(51,255)) break p000e0414;
		if (!CNDonotzero(getFlag(51),5)) break p000e0414;
		if (!CNDpresent(getFlag(51))) break p000e0414;
 		ACCwriteln(363);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0415:
	{
 		if (skipdoall('p000e0415')) break p000e0415;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0415;
 		}
		if (!CNDnoteq(51,255)) break p000e0415;
		if (!CNDonotzero(getFlag(51),3)) break p000e0415;
		if (!CNDabsent(getFlag(51))) break p000e0415;
 		ACCwriteln(364);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0416:
	{
 		if (skipdoall('p000e0416')) break p000e0416;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0416;
 		}
		if (!CNDnoteq(34,255)) break p000e0416;
		if (!CNDeq(51,255)) break p000e0416;
 		ACCwriteln(365);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0417:
	{
 		if (skipdoall('p000e0417')) break p000e0417;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0417;
 		}
		if (!CNDnoteq(51,255)) break p000e0417;
		if (!CNDpresent(getFlag(51))) break p000e0417;
 		ACCwriteln(366);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHUPA _
	p000e0418:
	{
 		if (skipdoall('p000e0418')) break p000e0418;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0418;
 		}
		if (!CNDnoteq(51,255)) break p000e0418;
		if (!CNDabsent(getFlag(51))) break p000e0418;
 		ACCwriteln(367);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESTRUIR _
	p000e0419:
	{
 		if (skipdoall('p000e0419')) break p000e0419;
 		if (in_response)
		{
			if (!CNDverb(69)) break p000e0419;
 		}
		if (!CNDeq(34,255)) break p000e0419;
		if (!CNDbnotzero(12,1)) break p000e0419;
 		ACCwriteln(368);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESTRUIR _
	p000e0420:
	{
 		if (skipdoall('p000e0420')) break p000e0420;
 		if (in_response)
		{
			if (!CNDverb(69)) break p000e0420;
 		}
		if (!CNDeq(34,255)) break p000e0420;
		if (!CNDbzero(12,1)) break p000e0420;
 		ACCwriteln(369);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESTRUIR _
	p000e0421:
	{
 		if (skipdoall('p000e0421')) break p000e0421;
 		if (in_response)
		{
			if (!CNDverb(69)) break p000e0421;
 		}
		if (!CNDnoteq(34,255)) break p000e0421;
		if (!CNDnoteq(51,255)) break p000e0421;
		if (!CNDonotzero(getFlag(51),3)) break p000e0421;
		if (!CNDpresent(getFlag(51))) break p000e0421;
 		ACCwriteln(370);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESTRUIR _
	p000e0422:
	{
 		if (skipdoall('p000e0422')) break p000e0422;
 		if (in_response)
		{
			if (!CNDverb(69)) break p000e0422;
 		}
		if (!CNDnoteq(34,255)) break p000e0422;
		if (!CNDnoteq(51,255)) break p000e0422;
		if (!CNDonotzero(getFlag(51),3)) break p000e0422;
		if (!CNDabsent(getFlag(51))) break p000e0422;
 		ACCwriteln(371);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESTRUIR _
	p000e0423:
	{
 		if (skipdoall('p000e0423')) break p000e0423;
 		if (in_response)
		{
			if (!CNDverb(69)) break p000e0423;
 		}
		if (!CNDnoteq(34,255)) break p000e0423;
		if (!CNDeq(51,255)) break p000e0423;
 		ACCwriteln(372);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESTRUIR _
	p000e0424:
	{
 		if (skipdoall('p000e0424')) break p000e0424;
 		if (in_response)
		{
			if (!CNDverb(69)) break p000e0424;
 		}
		if (!CNDnoteq(51,255)) break p000e0424;
		if (!CNDpresent(getFlag(51))) break p000e0424;
 		ACCwriteln(373);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESTRUIR _
	p000e0425:
	{
 		if (skipdoall('p000e0425')) break p000e0425;
 		if (in_response)
		{
			if (!CNDverb(69)) break p000e0425;
 		}
		if (!CNDnoteq(51,255)) break p000e0425;
		if (!CNDabsent(getFlag(51))) break p000e0425;
 		ACCwriteln(374);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ADQUIERE _
	p000e0426:
	{
 		if (skipdoall('p000e0426')) break p000e0426;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0426;
 		}
		if (!CNDeq(34,255)) break p000e0426;
		if (!CNDbnotzero(12,1)) break p000e0426;
 		ACCwriteln(375);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ADQUIERE _
	p000e0427:
	{
 		if (skipdoall('p000e0427')) break p000e0427;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0427;
 		}
		if (!CNDeq(34,255)) break p000e0427;
		if (!CNDbzero(12,1)) break p000e0427;
 		ACCwriteln(376);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ADQUIERE _
	p000e0428:
	{
 		if (skipdoall('p000e0428')) break p000e0428;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0428;
 		}
		if (!CNDnoteq(34,255)) break p000e0428;
		if (!CNDnoteq(51,255)) break p000e0428;
		if (!CNDonotzero(getFlag(51),3)) break p000e0428;
		if (!CNDpresent(getFlag(51))) break p000e0428;
 		ACCwriteln(377);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ADQUIERE _
	p000e0429:
	{
 		if (skipdoall('p000e0429')) break p000e0429;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0429;
 		}
		if (!CNDnoteq(34,255)) break p000e0429;
		if (!CNDnoteq(51,255)) break p000e0429;
		if (!CNDonotzero(getFlag(51),3)) break p000e0429;
		if (!CNDabsent(getFlag(51))) break p000e0429;
 		ACCwriteln(378);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ADQUIERE _
	p000e0430:
	{
 		if (skipdoall('p000e0430')) break p000e0430;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0430;
 		}
		if (!CNDnoteq(34,255)) break p000e0430;
		if (!CNDeq(51,255)) break p000e0430;
 		ACCwriteln(379);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ADQUIERE _
	p000e0431:
	{
 		if (skipdoall('p000e0431')) break p000e0431;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0431;
 		}
		if (!CNDnoteq(51,255)) break p000e0431;
		if (!CNDpresent(getFlag(51))) break p000e0431;
 		ACCwriteln(380);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ADQUIERE _
	p000e0432:
	{
 		if (skipdoall('p000e0432')) break p000e0432;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0432;
 		}
		if (!CNDnoteq(51,255)) break p000e0432;
		if (!CNDabsent(getFlag(51))) break p000e0432;
 		ACCwriteln(381);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DOBLA _
	p000e0433:
	{
 		if (skipdoall('p000e0433')) break p000e0433;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0433;
 		}
		if (!CNDeq(34,255)) break p000e0433;
		if (!CNDbnotzero(12,1)) break p000e0433;
 		ACCwriteln(382);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DOBLA _
	p000e0434:
	{
 		if (skipdoall('p000e0434')) break p000e0434;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0434;
 		}
		if (!CNDeq(34,255)) break p000e0434;
		if (!CNDbzero(12,1)) break p000e0434;
 		ACCwriteln(383);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DOBLA _
	p000e0435:
	{
 		if (skipdoall('p000e0435')) break p000e0435;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0435;
 		}
		if (!CNDnoteq(34,255)) break p000e0435;
		if (!CNDnoteq(51,255)) break p000e0435;
		if (!CNDonotzero(getFlag(51),3)) break p000e0435;
		if (!CNDpresent(getFlag(51))) break p000e0435;
 		ACCwriteln(384);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DOBLA _
	p000e0436:
	{
 		if (skipdoall('p000e0436')) break p000e0436;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0436;
 		}
		if (!CNDnoteq(34,255)) break p000e0436;
		if (!CNDnoteq(51,255)) break p000e0436;
		if (!CNDonotzero(getFlag(51),3)) break p000e0436;
		if (!CNDabsent(getFlag(51))) break p000e0436;
 		ACCwriteln(385);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DOBLA _
	p000e0437:
	{
 		if (skipdoall('p000e0437')) break p000e0437;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0437;
 		}
		if (!CNDnoteq(34,255)) break p000e0437;
		if (!CNDeq(51,255)) break p000e0437;
 		ACCwriteln(386);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DOBLA _
	p000e0438:
	{
 		if (skipdoall('p000e0438')) break p000e0438;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0438;
 		}
		if (!CNDnoteq(51,255)) break p000e0438;
		if (!CNDpresent(getFlag(51))) break p000e0438;
 		ACCwriteln(387);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DOBLA _
	p000e0439:
	{
 		if (skipdoall('p000e0439')) break p000e0439;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0439;
 		}
		if (!CNDnoteq(51,255)) break p000e0439;
		if (!CNDabsent(getFlag(51))) break p000e0439;
 		ACCwriteln(388);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESDOBLA _
	p000e0440:
	{
 		if (skipdoall('p000e0440')) break p000e0440;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0440;
 		}
		if (!CNDeq(34,255)) break p000e0440;
		if (!CNDbnotzero(12,1)) break p000e0440;
 		ACCwriteln(389);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESDOBLA _
	p000e0441:
	{
 		if (skipdoall('p000e0441')) break p000e0441;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0441;
 		}
		if (!CNDeq(34,255)) break p000e0441;
		if (!CNDbzero(12,1)) break p000e0441;
 		ACCwriteln(390);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESDOBLA _
	p000e0442:
	{
 		if (skipdoall('p000e0442')) break p000e0442;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0442;
 		}
		if (!CNDnoteq(34,255)) break p000e0442;
		if (!CNDnoteq(51,255)) break p000e0442;
		if (!CNDonotzero(getFlag(51),3)) break p000e0442;
		if (!CNDpresent(getFlag(51))) break p000e0442;
 		ACCwriteln(391);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESDOBLA _
	p000e0443:
	{
 		if (skipdoall('p000e0443')) break p000e0443;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0443;
 		}
		if (!CNDnoteq(34,255)) break p000e0443;
		if (!CNDnoteq(51,255)) break p000e0443;
		if (!CNDonotzero(getFlag(51),3)) break p000e0443;
		if (!CNDabsent(getFlag(51))) break p000e0443;
 		ACCwriteln(392);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESDOBLA _
	p000e0444:
	{
 		if (skipdoall('p000e0444')) break p000e0444;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0444;
 		}
		if (!CNDnoteq(34,255)) break p000e0444;
		if (!CNDeq(51,255)) break p000e0444;
 		ACCwriteln(393);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESDOBLA _
	p000e0445:
	{
 		if (skipdoall('p000e0445')) break p000e0445;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0445;
 		}
		if (!CNDnoteq(51,255)) break p000e0445;
		if (!CNDpresent(getFlag(51))) break p000e0445;
 		ACCwriteln(394);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESDOBLA _
	p000e0446:
	{
 		if (skipdoall('p000e0446')) break p000e0446;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0446;
 		}
		if (!CNDnoteq(51,255)) break p000e0446;
		if (!CNDabsent(getFlag(51))) break p000e0446;
 		ACCwriteln(395);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXITS _
	p000e0447:
	{
 		if (skipdoall('p000e0447')) break p000e0447;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0447;
 		}
 		ACCexits(getFlag(38),1000);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AYUDA _
	p000e0448:
	{
 		if (skipdoall('p000e0448')) break p000e0448;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0448;
 		}
 		ACChelp();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VERSION _
	p000e0449:
	{
 		if (skipdoall('p000e0449')) break p000e0449;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0449;
 		}
 		ACCversion();
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0450:
	{
 		if (skipdoall('p000e0450')) break p000e0450;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0450;
 		}
		if (!CNDsame(46,34)) break p000e0450;
 		ACCwhatox(13);
		if (!CNDonotzero(getFlag(13),3)) break p000e0450;
		if (!CNDnoteq(44,255)) break p000e0450;
 		ACCcopyff(34,14);
 		ACCcopyff(44,34);
 		ACCcopyff(14,44);
 		ACCcopyff(35,14);
 		ACCcopyff(45,35);
 		ACCcopyff(14,45);
 		ACCwhato();
		{}

	}

	// DA _
	p000e0451:
	{
 		if (skipdoall('p000e0451')) break p000e0451;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0451;
 		}
		if (!CNDprep(2)) break p000e0451;
		if (!CNDbzero(12,2)) break p000e0451;
		if (!CNDeq(44,255)) break p000e0451;
		if (!CNDbnotzero(12,1)) break p000e0451;
 		ACCwriteln(396);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0452:
	{
 		if (skipdoall('p000e0452')) break p000e0452;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0452;
 		}
		if (!CNDeq(34,255)) break p000e0452;
		if (!CNDbzero(12,1)) break p000e0452;
 		ACCwriteln(397);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0453:
	{
 		if (skipdoall('p000e0453')) break p000e0453;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0453;
 		}
		if (!CNDeq(34,255)) break p000e0453;
		if (!CNDbnotzero(12,1)) break p000e0453;
 		ACCwriteln(398);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0454:
	{
 		if (skipdoall('p000e0454')) break p000e0454;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0454;
 		}
		if (!CNDnoteq(34,255)) break p000e0454;
		if (!CNDeq(51,255)) break p000e0454;
 		ACCwriteln(399);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0455:
	{
 		if (skipdoall('p000e0455')) break p000e0455;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0455;
 		}
		if (!CNDnoteq(51,255)) break p000e0455;
		if (!CNDworn(getFlag(51))) break p000e0455;
		if (!CNDonotzero(getFlag(51),1)) break p000e0455;
 		ACCwriteln(400);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0456:
	{
 		if (skipdoall('p000e0456')) break p000e0456;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0456;
 		}
		if (!CNDnoteq(51,255)) break p000e0456;
		if (!CNDnotcarr(getFlag(51))) break p000e0456;
 		ACCwriteln(401);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0457:
	{
 		if (skipdoall('p000e0457')) break p000e0457;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0457;
 		}
		if (!CNDnoteq(51,255)) break p000e0457;
		if (!CNDcarried(getFlag(51))) break p000e0457;
		if (!CNDeq(44,255)) break p000e0457;
		if (!CNDbzero(12,1)) break p000e0457;
 		ACCwriteln(402);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0458:
	{
 		if (skipdoall('p000e0458')) break p000e0458;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0458;
 		}
		if (!CNDnoteq(51,255)) break p000e0458;
		if (!CNDcarried(getFlag(51))) break p000e0458;
		if (!CNDeq(44,255)) break p000e0458;
		if (!CNDbnotzero(12,1)) break p000e0458;
 		ACCwriteln(403);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0459:
	{
 		if (skipdoall('p000e0459')) break p000e0459;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0459;
 		}
		if (!CNDnoteq(51,255)) break p000e0459;
		if (!CNDcarried(getFlag(51))) break p000e0459;
		if (!CNDnoteq(44,255)) break p000e0459;
 		ACCwhatox2(15);
		if (!CNDeq(15,255)) break p000e0459;
 		ACCwriteln(404);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0460:
	{
 		if (skipdoall('p000e0460')) break p000e0460;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0460;
 		}
		if (!CNDnoteq(51,255)) break p000e0460;
		if (!CNDcarried(getFlag(51))) break p000e0460;
		if (!CNDnoteq(44,255)) break p000e0460;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0460;
		if (!CNDozero(getFlag(13),3)) break p000e0460;
 		ACCwriteln(405);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0461:
	{
 		if (skipdoall('p000e0461')) break p000e0461;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0461;
 		}
		if (!CNDnoteq(51,255)) break p000e0461;
		if (!CNDcarried(getFlag(51))) break p000e0461;
		if (!CNDnoteq(44,255)) break p000e0461;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0461;
		if (!CNDonotzero(getFlag(13),3)) break p000e0461;
		if (!CNDabsent(getFlag(13))) break p000e0461;
 		ACCwriteln(406);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0462:
	{
 		if (skipdoall('p000e0462')) break p000e0462;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0462;
 		}
		if (!CNDnoteq(51,255)) break p000e0462;
		if (!CNDcarried(getFlag(51))) break p000e0462;
		if (!CNDnoteq(44,255)) break p000e0462;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0462;
		if (!CNDonotzero(getFlag(13),3)) break p000e0462;
		if (!CNDpresent(getFlag(13))) break p000e0462;
 		ACCwriteln(407);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0463:
	{
 		if (skipdoall('p000e0463')) break p000e0463;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0463;
 		}
		if (!CNDsame(46,34)) break p000e0463;
 		ACCwhatox(13);
		if (!CNDonotzero(getFlag(13),3)) break p000e0463;
		if (!CNDnoteq(44,255)) break p000e0463;
 		ACCcopyff(34,14);
 		ACCcopyff(44,34);
 		ACCcopyff(14,44);
 		ACCcopyff(35,14);
 		ACCcopyff(45,35);
 		ACCcopyff(14,45);
 		ACCwhato();
		{}

	}

	// ENSEÑA _
	p000e0464:
	{
 		if (skipdoall('p000e0464')) break p000e0464;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0464;
 		}
		if (!CNDprep(2)) break p000e0464;
		if (!CNDbzero(12,2)) break p000e0464;
		if (!CNDeq(44,255)) break p000e0464;
		if (!CNDbnotzero(12,1)) break p000e0464;
 		ACCwriteln(408);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0465:
	{
 		if (skipdoall('p000e0465')) break p000e0465;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0465;
 		}
		if (!CNDeq(34,255)) break p000e0465;
		if (!CNDbzero(12,1)) break p000e0465;
 		ACCwriteln(409);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0466:
	{
 		if (skipdoall('p000e0466')) break p000e0466;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0466;
 		}
		if (!CNDeq(34,255)) break p000e0466;
		if (!CNDbnotzero(12,1)) break p000e0466;
 		ACCwriteln(410);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0467:
	{
 		if (skipdoall('p000e0467')) break p000e0467;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0467;
 		}
		if (!CNDnoteq(34,255)) break p000e0467;
		if (!CNDeq(51,255)) break p000e0467;
 		ACCwriteln(411);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0468:
	{
 		if (skipdoall('p000e0468')) break p000e0468;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0468;
 		}
		if (!CNDnoteq(51,255)) break p000e0468;
		if (!CNDworn(getFlag(51))) break p000e0468;
		if (!CNDonotzero(getFlag(51),1)) break p000e0468;
 		ACCwriteln(412);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0469:
	{
 		if (skipdoall('p000e0469')) break p000e0469;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0469;
 		}
		if (!CNDnoteq(51,255)) break p000e0469;
		if (!CNDnotcarr(getFlag(51))) break p000e0469;
 		ACCwriteln(413);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0470:
	{
 		if (skipdoall('p000e0470')) break p000e0470;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0470;
 		}
		if (!CNDnoteq(51,255)) break p000e0470;
		if (!CNDcarried(getFlag(51))) break p000e0470;
		if (!CNDeq(44,255)) break p000e0470;
		if (!CNDbzero(12,1)) break p000e0470;
 		ACCwriteln(414);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0471:
	{
 		if (skipdoall('p000e0471')) break p000e0471;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0471;
 		}
		if (!CNDnoteq(51,255)) break p000e0471;
		if (!CNDcarried(getFlag(51))) break p000e0471;
		if (!CNDeq(44,255)) break p000e0471;
		if (!CNDbnotzero(12,1)) break p000e0471;
 		ACCwriteln(415);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0472:
	{
 		if (skipdoall('p000e0472')) break p000e0472;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0472;
 		}
		if (!CNDnoteq(51,255)) break p000e0472;
		if (!CNDcarried(getFlag(51))) break p000e0472;
		if (!CNDnoteq(44,255)) break p000e0472;
 		ACCwhatox2(13);
		if (!CNDeq(13,255)) break p000e0472;
 		ACCwriteln(416);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0473:
	{
 		if (skipdoall('p000e0473')) break p000e0473;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0473;
 		}
		if (!CNDnoteq(51,255)) break p000e0473;
		if (!CNDcarried(getFlag(51))) break p000e0473;
		if (!CNDnoteq(44,255)) break p000e0473;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0473;
		if (!CNDozero(getFlag(13),3)) break p000e0473;
 		ACCwriteln(417);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0474:
	{
 		if (skipdoall('p000e0474')) break p000e0474;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0474;
 		}
		if (!CNDnoteq(51,255)) break p000e0474;
		if (!CNDcarried(getFlag(51))) break p000e0474;
		if (!CNDnoteq(44,255)) break p000e0474;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0474;
		if (!CNDonotzero(getFlag(13),3)) break p000e0474;
		if (!CNDabsent(getFlag(13))) break p000e0474;
 		ACCwriteln(418);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSEÑA _
	p000e0475:
	{
 		if (skipdoall('p000e0475')) break p000e0475;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0475;
 		}
		if (!CNDnoteq(51,255)) break p000e0475;
		if (!CNDcarried(getFlag(51))) break p000e0475;
		if (!CNDnoteq(44,255)) break p000e0475;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0475;
		if (!CNDonotzero(getFlag(13),3)) break p000e0475;
		if (!CNDpresent(getFlag(13))) break p000e0475;
 		ACCwriteln(419);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SOPLA _
	p000e0476:
	{
 		if (skipdoall('p000e0476')) break p000e0476;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0476;
 		}
		if (!CNDeq(34,255)) break p000e0476;
		if (!CNDbnotzero(12,1)) break p000e0476;
 		ACCwriteln(420);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SOPLA _
	p000e0477:
	{
 		if (skipdoall('p000e0477')) break p000e0477;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0477;
 		}
		if (!CNDeq(34,255)) break p000e0477;
		if (!CNDbzero(12,1)) break p000e0477;
 		ACCwriteln(421);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SOPLA _
	p000e0478:
	{
 		if (skipdoall('p000e0478')) break p000e0478;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0478;
 		}
		if (!CNDnoteq(34,255)) break p000e0478;
		if (!CNDnoteq(51,255)) break p000e0478;
		if (!CNDonotzero(getFlag(51),3)) break p000e0478;
		if (!CNDpresent(getFlag(51))) break p000e0478;
 		ACCwriteln(422);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SOPLA _
	p000e0479:
	{
 		if (skipdoall('p000e0479')) break p000e0479;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0479;
 		}
		if (!CNDnoteq(34,255)) break p000e0479;
		if (!CNDnoteq(51,255)) break p000e0479;
		if (!CNDonotzero(getFlag(51),3)) break p000e0479;
		if (!CNDabsent(getFlag(51))) break p000e0479;
 		ACCwriteln(423);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SOPLA _
	p000e0480:
	{
 		if (skipdoall('p000e0480')) break p000e0480;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0480;
 		}
		if (!CNDnoteq(34,255)) break p000e0480;
		if (!CNDeq(51,255)) break p000e0480;
 		ACCwriteln(424);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SOPLA _
	p000e0481:
	{
 		if (skipdoall('p000e0481')) break p000e0481;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0481;
 		}
		if (!CNDnoteq(51,255)) break p000e0481;
		if (!CNDpresent(getFlag(51))) break p000e0481;
 		ACCwriteln(425);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SOPLA _
	p000e0482:
	{
 		if (skipdoall('p000e0482')) break p000e0482;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0482;
 		}
		if (!CNDnoteq(51,255)) break p000e0482;
		if (!CNDabsent(getFlag(51))) break p000e0482;
 		ACCwriteln(426);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHILLA _
	p000e0483:
	{
 		if (skipdoall('p000e0483')) break p000e0483;
 		if (in_response)
		{
			if (!CNDverb(77)) break p000e0483;
 		}
		if (!CNDnoteq(34,255)) break p000e0483;
		if (!CNDnoteq(51,255)) break p000e0483;
		if (!CNDonotzero(getFlag(51),3)) break p000e0483;
		if (!CNDpresent(getFlag(51))) break p000e0483;
 		ACCwriteln(427);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHILLA _
	p000e0484:
	{
 		if (skipdoall('p000e0484')) break p000e0484;
 		if (in_response)
		{
			if (!CNDverb(77)) break p000e0484;
 		}
		if (!CNDnoteq(34,255)) break p000e0484;
		if (!CNDnoteq(51,255)) break p000e0484;
		if (!CNDonotzero(getFlag(51),3)) break p000e0484;
		if (!CNDabsent(getFlag(51))) break p000e0484;
 		ACCwriteln(428);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHILLA _
	p000e0485:
	{
 		if (skipdoall('p000e0485')) break p000e0485;
 		if (in_response)
		{
			if (!CNDverb(77)) break p000e0485;
 		}
 		ACCwriteln(429);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPERTAR _
	p000e0486:
	{
 		if (skipdoall('p000e0486')) break p000e0486;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0486;
 		}
		if (!CNDnoteq(34,255)) break p000e0486;
		if (!CNDnoteq(51,255)) break p000e0486;
		if (!CNDonotzero(getFlag(51),3)) break p000e0486;
		if (!CNDpresent(getFlag(51))) break p000e0486;
 		ACCwriteln(430);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPERTAR _
	p000e0487:
	{
 		if (skipdoall('p000e0487')) break p000e0487;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0487;
 		}
		if (!CNDnoteq(34,255)) break p000e0487;
		if (!CNDnoteq(51,255)) break p000e0487;
		if (!CNDonotzero(getFlag(51),3)) break p000e0487;
		if (!CNDabsent(getFlag(51))) break p000e0487;
 		ACCwriteln(431);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPERTAR _
	p000e0488:
	{
 		if (skipdoall('p000e0488')) break p000e0488;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0488;
 		}
 		ACCwriteln(432);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0489:
	{
 		if (skipdoall('p000e0489')) break p000e0489;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0489;
 		}
		if (!CNDeq(34,255)) break p000e0489;
		if (!CNDbnotzero(12,1)) break p000e0489;
 		ACCwriteln(433);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0490:
	{
 		if (skipdoall('p000e0490')) break p000e0490;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0490;
 		}
		if (!CNDeq(34,255)) break p000e0490;
		if (!CNDbzero(12,1)) break p000e0490;
 		ACCwriteln(434);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0491:
	{
 		if (skipdoall('p000e0491')) break p000e0491;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0491;
 		}
		if (!CNDnoteq(34,255)) break p000e0491;
		if (!CNDnoteq(51,255)) break p000e0491;
		if (!CNDonotzero(getFlag(51),3)) break p000e0491;
		if (!CNDpresent(getFlag(51))) break p000e0491;
 		ACCwriteln(435);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0492:
	{
 		if (skipdoall('p000e0492')) break p000e0492;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0492;
 		}
		if (!CNDnoteq(34,255)) break p000e0492;
		if (!CNDnoteq(51,255)) break p000e0492;
		if (!CNDonotzero(getFlag(51),3)) break p000e0492;
		if (!CNDabsent(getFlag(51))) break p000e0492;
 		ACCwriteln(436);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0493:
	{
 		if (skipdoall('p000e0493')) break p000e0493;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0493;
 		}
		if (!CNDnoteq(34,255)) break p000e0493;
		if (!CNDeq(51,255)) break p000e0493;
 		ACCwriteln(437);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0494:
	{
 		if (skipdoall('p000e0494')) break p000e0494;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0494;
 		}
		if (!CNDnoteq(51,255)) break p000e0494;
		if (!CNDpresent(getFlag(51))) break p000e0494;
		if (!CNDgt(55,52)) break p000e0494;
 		ACCwriteln(438);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0495:
	{
 		if (skipdoall('p000e0495')) break p000e0495;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0495;
 		}
		if (!CNDnoteq(51,255)) break p000e0495;
		if (!CNDpresent(getFlag(51))) break p000e0495;
 		ACCwriteln(439);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEVANTA _
	p000e0496:
	{
 		if (skipdoall('p000e0496')) break p000e0496;
 		if (in_response)
		{
			if (!CNDverb(114)) break p000e0496;
 		}
		if (!CNDnoteq(51,255)) break p000e0496;
		if (!CNDabsent(getFlag(51))) break p000e0496;
 		ACCwriteln(440);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRASTRA _
	p000e0497:
	{
 		if (skipdoall('p000e0497')) break p000e0497;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0497;
 		}
		if (!CNDeq(34,255)) break p000e0497;
		if (!CNDbnotzero(12,1)) break p000e0497;
 		ACCwriteln(441);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRASTRA _
	p000e0498:
	{
 		if (skipdoall('p000e0498')) break p000e0498;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0498;
 		}
		if (!CNDeq(34,255)) break p000e0498;
		if (!CNDbzero(12,1)) break p000e0498;
 		ACCwriteln(442);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRASTRA _
	p000e0499:
	{
 		if (skipdoall('p000e0499')) break p000e0499;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0499;
 		}
		if (!CNDnoteq(34,255)) break p000e0499;
		if (!CNDnoteq(51,255)) break p000e0499;
		if (!CNDonotzero(getFlag(51),3)) break p000e0499;
		if (!CNDpresent(getFlag(51))) break p000e0499;
 		ACCwriteln(443);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRASTRA _
	p000e0500:
	{
 		if (skipdoall('p000e0500')) break p000e0500;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0500;
 		}
		if (!CNDnoteq(34,255)) break p000e0500;
		if (!CNDnoteq(51,255)) break p000e0500;
		if (!CNDonotzero(getFlag(51),3)) break p000e0500;
		if (!CNDabsent(getFlag(51))) break p000e0500;
 		ACCwriteln(444);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRASTRA _
	p000e0501:
	{
 		if (skipdoall('p000e0501')) break p000e0501;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0501;
 		}
		if (!CNDnoteq(34,255)) break p000e0501;
		if (!CNDeq(51,255)) break p000e0501;
 		ACCwriteln(445);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRASTRA _
	p000e0502:
	{
 		if (skipdoall('p000e0502')) break p000e0502;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0502;
 		}
		if (!CNDnoteq(51,255)) break p000e0502;
		if (!CNDpresent(getFlag(51))) break p000e0502;
 		ACCwriteln(446);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRASTRA _
	p000e0503:
	{
 		if (skipdoall('p000e0503')) break p000e0503;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0503;
 		}
		if (!CNDnoteq(51,255)) break p000e0503;
		if (!CNDabsent(getFlag(51))) break p000e0503;
 		ACCwriteln(447);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0504:
	{
 		if (skipdoall('p000e0504')) break p000e0504;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0504;
 		}
		if (!CNDeq(34,255)) break p000e0504;
		if (!CNDbnotzero(12,1)) break p000e0504;
 		ACCwriteln(448);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0505:
	{
 		if (skipdoall('p000e0505')) break p000e0505;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0505;
 		}
		if (!CNDeq(34,255)) break p000e0505;
		if (!CNDbzero(12,1)) break p000e0505;
 		ACCwriteln(449);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0506:
	{
 		if (skipdoall('p000e0506')) break p000e0506;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0506;
 		}
		if (!CNDnoteq(34,255)) break p000e0506;
		if (!CNDnoteq(51,255)) break p000e0506;
		if (!CNDonotzero(getFlag(51),3)) break p000e0506;
		if (!CNDpresent(getFlag(51))) break p000e0506;
 		ACCwriteln(450);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0507:
	{
 		if (skipdoall('p000e0507')) break p000e0507;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0507;
 		}
		if (!CNDnoteq(34,255)) break p000e0507;
		if (!CNDnoteq(51,255)) break p000e0507;
		if (!CNDonotzero(getFlag(51),3)) break p000e0507;
		if (!CNDabsent(getFlag(51))) break p000e0507;
 		ACCwriteln(451);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0508:
	{
 		if (skipdoall('p000e0508')) break p000e0508;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0508;
 		}
		if (!CNDnoteq(34,255)) break p000e0508;
		if (!CNDeq(51,255)) break p000e0508;
 		ACCwriteln(452);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0509:
	{
 		if (skipdoall('p000e0509')) break p000e0509;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0509;
 		}
		if (!CNDnoteq(51,255)) break p000e0509;
		if (!CNDpresent(getFlag(51))) break p000e0509;
 		ACCwriteln(453);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// VACIA _
	p000e0510:
	{
 		if (skipdoall('p000e0510')) break p000e0510;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0510;
 		}
		if (!CNDnoteq(51,255)) break p000e0510;
		if (!CNDabsent(getFlag(51))) break p000e0510;
 		ACCwriteln(454);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLAMA _
	p000e0511:
	{
 		if (skipdoall('p000e0511')) break p000e0511;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0511;
 		}
		if (!CNDeq(34,255)) break p000e0511;
		if (!CNDbnotzero(12,1)) break p000e0511;
 		ACCwriteln(455);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLAMA _
	p000e0512:
	{
 		if (skipdoall('p000e0512')) break p000e0512;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0512;
 		}
		if (!CNDeq(34,255)) break p000e0512;
		if (!CNDbzero(12,1)) break p000e0512;
 		ACCwriteln(456);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLAMA _
	p000e0513:
	{
 		if (skipdoall('p000e0513')) break p000e0513;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0513;
 		}
		if (!CNDnoteq(34,255)) break p000e0513;
		if (!CNDnoteq(51,255)) break p000e0513;
		if (!CNDonotzero(getFlag(51),3)) break p000e0513;
		if (!CNDpresent(getFlag(51))) break p000e0513;
 		ACCwriteln(457);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLAMA _
	p000e0514:
	{
 		if (skipdoall('p000e0514')) break p000e0514;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0514;
 		}
		if (!CNDnoteq(34,255)) break p000e0514;
		if (!CNDnoteq(51,255)) break p000e0514;
		if (!CNDonotzero(getFlag(51),3)) break p000e0514;
		if (!CNDabsent(getFlag(51))) break p000e0514;
 		ACCwriteln(458);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLAMA _
	p000e0515:
	{
 		if (skipdoall('p000e0515')) break p000e0515;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0515;
 		}
		if (!CNDnoteq(34,255)) break p000e0515;
		if (!CNDeq(51,255)) break p000e0515;
 		ACCwriteln(459);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLAMA _
	p000e0516:
	{
 		if (skipdoall('p000e0516')) break p000e0516;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0516;
 		}
		if (!CNDnoteq(51,255)) break p000e0516;
		if (!CNDpresent(getFlag(51))) break p000e0516;
 		ACCwriteln(460);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LLAMA _
	p000e0517:
	{
 		if (skipdoall('p000e0517')) break p000e0517;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0517;
 		}
		if (!CNDnoteq(51,255)) break p000e0517;
		if (!CNDabsent(getFlag(51))) break p000e0517;
 		ACCwriteln(461);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PREGUNTA _
	p000e0518:
	{
 		if (skipdoall('p000e0518')) break p000e0518;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0518;
 		}
		if (!CNDeq(34,255)) break p000e0518;
		if (!CNDbnotzero(12,1)) break p000e0518;
 		ACCwriteln(462);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PREGUNTA _
	p000e0519:
	{
 		if (skipdoall('p000e0519')) break p000e0519;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0519;
 		}
		if (!CNDeq(34,255)) break p000e0519;
		if (!CNDbzero(12,1)) break p000e0519;
 		ACCwriteln(463);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PREGUNTA _
	p000e0520:
	{
 		if (skipdoall('p000e0520')) break p000e0520;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0520;
 		}
		if (!CNDnoteq(34,255)) break p000e0520;
		if (!CNDnoteq(51,255)) break p000e0520;
		if (!CNDonotzero(getFlag(51),3)) break p000e0520;
		if (!CNDpresent(getFlag(51))) break p000e0520;
 		ACCwriteln(464);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PREGUNTA _
	p000e0521:
	{
 		if (skipdoall('p000e0521')) break p000e0521;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0521;
 		}
		if (!CNDnoteq(34,255)) break p000e0521;
		if (!CNDnoteq(51,255)) break p000e0521;
		if (!CNDonotzero(getFlag(51),3)) break p000e0521;
		if (!CNDabsent(getFlag(51))) break p000e0521;
 		ACCwriteln(465);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PREGUNTA _
	p000e0522:
	{
 		if (skipdoall('p000e0522')) break p000e0522;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0522;
 		}
		if (!CNDnoteq(34,255)) break p000e0522;
		if (!CNDeq(51,255)) break p000e0522;
 		ACCwriteln(466);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PREGUNTA _
	p000e0523:
	{
 		if (skipdoall('p000e0523')) break p000e0523;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0523;
 		}
		if (!CNDnoteq(51,255)) break p000e0523;
		if (!CNDpresent(getFlag(51))) break p000e0523;
 		ACCwriteln(467);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PREGUNTA _
	p000e0524:
	{
 		if (skipdoall('p000e0524')) break p000e0524;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0524;
 		}
		if (!CNDnoteq(51,255)) break p000e0524;
		if (!CNDabsent(getFlag(51))) break p000e0524;
 		ACCwriteln(468);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONSULTA _
	p000e0525:
	{
 		if (skipdoall('p000e0525')) break p000e0525;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0525;
 		}
		if (!CNDeq(34,255)) break p000e0525;
		if (!CNDbnotzero(12,1)) break p000e0525;
 		ACCwriteln(469);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONSULTA _
	p000e0526:
	{
 		if (skipdoall('p000e0526')) break p000e0526;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0526;
 		}
		if (!CNDeq(34,255)) break p000e0526;
		if (!CNDbzero(12,1)) break p000e0526;
 		ACCwriteln(470);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONSULTA _
	p000e0527:
	{
 		if (skipdoall('p000e0527')) break p000e0527;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0527;
 		}
		if (!CNDnoteq(34,255)) break p000e0527;
		if (!CNDeq(51,255)) break p000e0527;
 		ACCwriteln(471);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONSULTA _
	p000e0528:
	{
 		if (skipdoall('p000e0528')) break p000e0528;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0528;
 		}
		if (!CNDnoteq(51,255)) break p000e0528;
		if (!CNDpresent(getFlag(51))) break p000e0528;
 		ACCwriteln(472);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONSULTA _
	p000e0529:
	{
 		if (skipdoall('p000e0529')) break p000e0529;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0529;
 		}
		if (!CNDnoteq(51,255)) break p000e0529;
		if (!CNDabsent(getFlag(51))) break p000e0529;
 		ACCwriteln(473);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INCREPA _
	p000e0530:
	{
 		if (skipdoall('p000e0530')) break p000e0530;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0530;
 		}
		if (!CNDeq(34,255)) break p000e0530;
		if (!CNDbnotzero(12,1)) break p000e0530;
 		ACCwriteln(474);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INCREPA _
	p000e0531:
	{
 		if (skipdoall('p000e0531')) break p000e0531;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0531;
 		}
		if (!CNDeq(34,255)) break p000e0531;
		if (!CNDbzero(12,1)) break p000e0531;
 		ACCwriteln(475);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INCREPA _
	p000e0532:
	{
 		if (skipdoall('p000e0532')) break p000e0532;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0532;
 		}
		if (!CNDnoteq(34,255)) break p000e0532;
		if (!CNDnoteq(51,255)) break p000e0532;
		if (!CNDonotzero(getFlag(51),3)) break p000e0532;
		if (!CNDpresent(getFlag(51))) break p000e0532;
 		ACCwriteln(476);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INCREPA _
	p000e0533:
	{
 		if (skipdoall('p000e0533')) break p000e0533;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0533;
 		}
		if (!CNDnoteq(34,255)) break p000e0533;
		if (!CNDnoteq(51,255)) break p000e0533;
		if (!CNDonotzero(getFlag(51),3)) break p000e0533;
		if (!CNDabsent(getFlag(51))) break p000e0533;
 		ACCwriteln(477);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INCREPA _
	p000e0534:
	{
 		if (skipdoall('p000e0534')) break p000e0534;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0534;
 		}
		if (!CNDnoteq(34,255)) break p000e0534;
		if (!CNDeq(51,255)) break p000e0534;
 		ACCwriteln(478);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INCREPA _
	p000e0535:
	{
 		if (skipdoall('p000e0535')) break p000e0535;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0535;
 		}
		if (!CNDnoteq(51,255)) break p000e0535;
		if (!CNDpresent(getFlag(51))) break p000e0535;
 		ACCwriteln(479);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INCREPA _
	p000e0536:
	{
 		if (skipdoall('p000e0536')) break p000e0536;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0536;
 		}
		if (!CNDnoteq(51,255)) break p000e0536;
		if (!CNDabsent(getFlag(51))) break p000e0536;
 		ACCwriteln(480);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACERCA _
	p000e0537:
	{
 		if (skipdoall('p000e0537')) break p000e0537;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0537;
 		}
		if (!CNDeq(34,255)) break p000e0537;
		if (!CNDbnotzero(12,1)) break p000e0537;
 		ACCwriteln(481);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACERCA _
	p000e0538:
	{
 		if (skipdoall('p000e0538')) break p000e0538;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0538;
 		}
		if (!CNDeq(34,255)) break p000e0538;
		if (!CNDbzero(12,1)) break p000e0538;
 		ACCwriteln(482);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACERCA _
	p000e0539:
	{
 		if (skipdoall('p000e0539')) break p000e0539;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0539;
 		}
		if (!CNDnoteq(34,255)) break p000e0539;
		if (!CNDnoteq(51,255)) break p000e0539;
		if (!CNDonotzero(getFlag(51),3)) break p000e0539;
		if (!CNDpresent(getFlag(51))) break p000e0539;
 		ACCwriteln(483);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACERCA _
	p000e0540:
	{
 		if (skipdoall('p000e0540')) break p000e0540;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0540;
 		}
		if (!CNDnoteq(34,255)) break p000e0540;
		if (!CNDnoteq(51,255)) break p000e0540;
		if (!CNDonotzero(getFlag(51),3)) break p000e0540;
		if (!CNDabsent(getFlag(51))) break p000e0540;
 		ACCwriteln(484);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACERCA _
	p000e0541:
	{
 		if (skipdoall('p000e0541')) break p000e0541;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0541;
 		}
		if (!CNDnoteq(34,255)) break p000e0541;
		if (!CNDeq(51,255)) break p000e0541;
 		ACCwriteln(485);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACERCA _
	p000e0542:
	{
 		if (skipdoall('p000e0542')) break p000e0542;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0542;
 		}
		if (!CNDnoteq(51,255)) break p000e0542;
		if (!CNDpresent(getFlag(51))) break p000e0542;
 		ACCwriteln(486);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACERCA _
	p000e0543:
	{
 		if (skipdoall('p000e0543')) break p000e0543;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0543;
 		}
		if (!CNDnoteq(51,255)) break p000e0543;
		if (!CNDabsent(getFlag(51))) break p000e0543;
 		ACCwriteln(487);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEPARA _
	p000e0544:
	{
 		if (skipdoall('p000e0544')) break p000e0544;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0544;
 		}
		if (!CNDeq(34,255)) break p000e0544;
		if (!CNDbnotzero(12,1)) break p000e0544;
 		ACCwriteln(488);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEPARA _
	p000e0545:
	{
 		if (skipdoall('p000e0545')) break p000e0545;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0545;
 		}
		if (!CNDeq(34,255)) break p000e0545;
		if (!CNDbzero(12,1)) break p000e0545;
 		ACCwriteln(489);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEPARA _
	p000e0546:
	{
 		if (skipdoall('p000e0546')) break p000e0546;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0546;
 		}
		if (!CNDnoteq(34,255)) break p000e0546;
		if (!CNDnoteq(51,255)) break p000e0546;
		if (!CNDonotzero(getFlag(51),3)) break p000e0546;
		if (!CNDpresent(getFlag(51))) break p000e0546;
 		ACCwriteln(490);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEPARA _
	p000e0547:
	{
 		if (skipdoall('p000e0547')) break p000e0547;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0547;
 		}
		if (!CNDnoteq(34,255)) break p000e0547;
		if (!CNDnoteq(51,255)) break p000e0547;
		if (!CNDonotzero(getFlag(51),3)) break p000e0547;
		if (!CNDabsent(getFlag(51))) break p000e0547;
 		ACCwriteln(491);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEPARA _
	p000e0548:
	{
 		if (skipdoall('p000e0548')) break p000e0548;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0548;
 		}
		if (!CNDnoteq(34,255)) break p000e0548;
		if (!CNDeq(51,255)) break p000e0548;
 		ACCwriteln(492);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEPARA _
	p000e0549:
	{
 		if (skipdoall('p000e0549')) break p000e0549;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0549;
 		}
		if (!CNDnoteq(51,255)) break p000e0549;
		if (!CNDpresent(getFlag(51))) break p000e0549;
 		ACCwriteln(493);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEPARA _
	p000e0550:
	{
 		if (skipdoall('p000e0550')) break p000e0550;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0550;
 		}
		if (!CNDnoteq(51,255)) break p000e0550;
		if (!CNDabsent(getFlag(51))) break p000e0550;
 		ACCwriteln(494);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DECIR _
	p000e0551:
	{
 		if (skipdoall('p000e0551')) break p000e0551;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0551;
 		}
		if (!CNDeq(34,255)) break p000e0551;
		if (!CNDbnotzero(12,1)) break p000e0551;
 		ACCwriteln(495);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DECIR _
	p000e0552:
	{
 		if (skipdoall('p000e0552')) break p000e0552;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0552;
 		}
		if (!CNDeq(34,255)) break p000e0552;
		if (!CNDbzero(12,1)) break p000e0552;
 		ACCwriteln(496);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DECIR _
	p000e0553:
	{
 		if (skipdoall('p000e0553')) break p000e0553;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0553;
 		}
		if (!CNDnoteq(34,255)) break p000e0553;
		if (!CNDnoteq(51,255)) break p000e0553;
		if (!CNDonotzero(getFlag(51),3)) break p000e0553;
		if (!CNDpresent(getFlag(51))) break p000e0553;
 		ACCwriteln(497);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DECIR _
	p000e0554:
	{
 		if (skipdoall('p000e0554')) break p000e0554;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0554;
 		}
		if (!CNDnoteq(34,255)) break p000e0554;
		if (!CNDnoteq(51,255)) break p000e0554;
		if (!CNDonotzero(getFlag(51),3)) break p000e0554;
		if (!CNDabsent(getFlag(51))) break p000e0554;
 		ACCwriteln(498);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DECIR _
	p000e0555:
	{
 		if (skipdoall('p000e0555')) break p000e0555;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0555;
 		}
		if (!CNDnoteq(34,255)) break p000e0555;
		if (!CNDeq(51,255)) break p000e0555;
 		ACCwriteln(499);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DECIR _
	p000e0556:
	{
 		if (skipdoall('p000e0556')) break p000e0556;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0556;
 		}
		if (!CNDnoteq(51,255)) break p000e0556;
		if (!CNDpresent(getFlag(51))) break p000e0556;
 		ACCwriteln(500);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DECIR _
	p000e0557:
	{
 		if (skipdoall('p000e0557')) break p000e0557;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0557;
 		}
		if (!CNDnoteq(51,255)) break p000e0557;
		if (!CNDabsent(getFlag(51))) break p000e0557;
 		ACCwriteln(501);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANDA _
	p000e0558:
	{
 		if (skipdoall('p000e0558')) break p000e0558;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0558;
 		}
		if (!CNDeq(34,255)) break p000e0558;
		if (!CNDbnotzero(12,1)) break p000e0558;
 		ACCwriteln(502);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANDA _
	p000e0559:
	{
 		if (skipdoall('p000e0559')) break p000e0559;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0559;
 		}
		if (!CNDeq(34,255)) break p000e0559;
		if (!CNDbzero(12,1)) break p000e0559;
 		ACCwriteln(503);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANDA _
	p000e0560:
	{
 		if (skipdoall('p000e0560')) break p000e0560;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0560;
 		}
		if (!CNDnoteq(34,255)) break p000e0560;
		if (!CNDnoteq(51,255)) break p000e0560;
		if (!CNDonotzero(getFlag(51),3)) break p000e0560;
		if (!CNDpresent(getFlag(51))) break p000e0560;
 		ACCwriteln(504);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANDA _
	p000e0561:
	{
 		if (skipdoall('p000e0561')) break p000e0561;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0561;
 		}
		if (!CNDnoteq(34,255)) break p000e0561;
		if (!CNDnoteq(51,255)) break p000e0561;
		if (!CNDonotzero(getFlag(51),3)) break p000e0561;
		if (!CNDabsent(getFlag(51))) break p000e0561;
 		ACCwriteln(505);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANDA _
	p000e0562:
	{
 		if (skipdoall('p000e0562')) break p000e0562;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0562;
 		}
		if (!CNDnoteq(34,255)) break p000e0562;
		if (!CNDeq(51,255)) break p000e0562;
 		ACCwriteln(506);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANDA _
	p000e0563:
	{
 		if (skipdoall('p000e0563')) break p000e0563;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0563;
 		}
		if (!CNDnoteq(51,255)) break p000e0563;
		if (!CNDpresent(getFlag(51))) break p000e0563;
 		ACCwriteln(507);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANDA _
	p000e0564:
	{
 		if (skipdoall('p000e0564')) break p000e0564;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0564;
 		}
		if (!CNDnoteq(51,255)) break p000e0564;
		if (!CNDabsent(getFlag(51))) break p000e0564;
 		ACCwriteln(508);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0565:
	{
 		if (skipdoall('p000e0565')) break p000e0565;
		if (!CNDlt(33,14)) break p000e0565;
		if (!CNDmove(38)) break p000e0565;
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0566:
	{
 		if (skipdoall('p000e0566')) break p000e0566;
		if (!CNDlt(33,14)) break p000e0566;
 		ACCplus(33,1002);
 		ACCwrite(509);
 		ACCmes(getFlag(33));
 		ACCwrite(510);
 		ACCminus(33,1002);
 		ACCexits(getFlag(38),1000);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0567:
	{
 		if (skipdoall('p000e0567')) break p000e0567;
 		ACChook(511);
		if (done_flag) break pro000_restart;
		{}

	}


}
}

function pro001()
{
process_restart=true;
pro001_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p001e0000:
	{
 		if (skipdoall('p001e0000')) break p001e0000;
 		ACChook(512);
		if (done_flag) break pro001_restart;
		{}

	}

	// _ _
	p001e0001:
	{
 		if (skipdoall('p001e0001')) break p001e0001;
 		ACCgraphic(0);
 		ACCtitle(513);
		{}

	}

	// _ _
	p001e0002:
	{
 		if (skipdoall('p001e0002')) break p001e0002;
		if (!CNDat(0)) break p001e0002;
 		ACClet(59,2);
		{}

	}

	// _ _
	p001e0003:
	{
 		if (skipdoall('p001e0003')) break p001e0003;
		if (!CNDislight()) break p001e0003;
		if (!CNDgt(38,20)) break p001e0003;
 		ACCnewline();
 		ACCexits(getFlag(38),1000);
 		ACClistobj();
 		ACClistnpc(getFlag(38));
 		ACClet(62,getFlag(38));
		if (CNDeq(64,0))
  		ACCvolume(0,0);
		{}

	}

	// _ _
	p001e0004:
	{
 		if (skipdoall('p001e0004')) break p001e0004;
		if (!CNDat(0)) break p001e0004;
 		ACClet(37,12);
 		ACClet(52,100);
 		ACClet(31,0);
 		ACClet(68,21);
 		ACClet(60,0);
 		ACClet(200,255);
 		ACClet(63,1);
 		ACClet(64,1);
 		ACClet(67,0);
 		ACClet(100,1);
 		ACClet(101,1);
 		ACCanykey();
 		function anykey00007() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00006() 
		{
 		ACCnewline();
 		ACCwriteln(517);
 		ACCgoto(getFlag(68));
 		ACCnewline();
 		ACCanykey();
 		waitKey(anykey00007);
		}
 		function anykey00005() 
		{
 		ACCnewline();
 		ACCwriteln(514);
 		ACCnewline();
 		ACCwriteln(515);
 		ACCnewline();
 		ACCwriteln(516);
 		ACCanykey();
 		waitKey(anykey00006);
		}
 		waitKey(anykey00005);
		done_flag=true;
		break pro001_restart;
		{}

	}

	// _ _
	p001e0005:
	{
 		if (skipdoall('p001e0005')) break p001e0005;
		if (!CNDat(21)) break p001e0005;
 		ACCanykey();
 		function anykey00008() 
		{
 		ACCnewline();
 		ACCwriteln(518);
		}
 		waitKey(anykey00008);
		done_flag=true;
		break pro001_restart;
		{}

	}


}
}

function pro002()
{
process_restart=true;
pro002_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p002e0000:
	{
 		if (skipdoall('p002e0000')) break p002e0000;
 		ACChook(519);
		if (done_flag) break pro002_restart;
		{}

	}

	// _ _
	p002e0001:
	{
 		if (skipdoall('p002e0001')) break p002e0001;
 		ACCweight(61);
 		ACClet(60,getFlag(32));
 		ACCmul(60,256);
 		ACCplus(60,getFlag(31));
		{}

	}

	// _ _
	p002e0002:
	{
 		if (skipdoall('p002e0002')) break p002e0002;
 		ACCextern(520);
		{}

	}


}
}

function pro003()
{
process_restart=true;
pro003_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p003e0000:
	{
 		if (skipdoall('p003e0000')) break p003e0000;
 		ACCclear(99);
		{}

	}

	// _ _
	p003e0001:
	{
 		if (skipdoall('p003e0001')) break p003e0001;
		if (CNDeq(98,100))
  		ACCextern(521);
		{}

	}

	// _ _
	p003e0002:
	{
 		if (skipdoall('p003e0002')) break p003e0002;
 		ACCgetkey(99);
 		function anykey00009() 
		{
 		ACCprocess(4);
		}
 		waitKey(anykey00009);
		done_flag=true;
		break pro003_restart;
		{}

	}


}
}

function pro004()
{
process_restart=true;
pro004_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p004e0000:
	{
 		if (skipdoall('p004e0000')) break p004e0000;
		if (!CNDeq(98,100)) break p004e0000;
		if (!CNDeq(99,49)) break p004e0000;
		if (!CNDzero(63)) break p004e0000;
 		ACCvolume(1,65535);
 		ACClet(63,1);
 		ACCwriteln(522);
 		ACCminus(31,1);
 		ACCextern(523);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0001:
	{
 		if (skipdoall('p004e0001')) break p004e0001;
		if (!CNDeq(98,100)) break p004e0001;
		if (!CNDeq(99,49)) break p004e0001;
		if (!CNDnotzero(63)) break p004e0001;
 		ACCvolume(1,0);
 		ACClet(63,0);
 		ACCwriteln(524);
 		ACCminus(31,1);
 		ACCextern(525);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0002:
	{
 		if (skipdoall('p004e0002')) break p004e0002;
		if (!CNDeq(98,100)) break p004e0002;
		if (!CNDeq(99,50)) break p004e0002;
		if (!CNDzero(64)) break p004e0002;
 		ACCvolume(0,65535);
 		ACCvolume(2,65535);
 		ACClet(64,1);
 		ACCwriteln(526);
 		ACCminus(31,1);
 		ACCextern(527);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0003:
	{
 		if (skipdoall('p004e0003')) break p004e0003;
		if (!CNDeq(98,100)) break p004e0003;
		if (!CNDeq(99,50)) break p004e0003;
		if (!CNDnotzero(64)) break p004e0003;
 		ACCvolume(0,0);
 		ACCvolume(2,0);
 		ACClet(64,0);
 		ACCwriteln(528);
 		ACCminus(31,1);
 		ACCextern(529);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0004:
	{
 		if (skipdoall('p004e0004')) break p004e0004;
		if (!CNDeq(98,100)) break p004e0004;
		if (!CNDeq(99,51)) break p004e0004;
		if (!CNDzero(67)) break p004e0004;
 		ACClet(67,1);
 		ACCwriteln(530);
 		ACCminus(31,1);
 		ACCextern(531);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0005:
	{
 		if (skipdoall('p004e0005')) break p004e0005;
		if (!CNDeq(98,100)) break p004e0005;
		if (!CNDeq(99,51)) break p004e0005;
		if (!CNDnotzero(67)) break p004e0005;
 		ACClet(67,0);
 		ACCwriteln(532);
 		ACCminus(31,1);
 		ACCextern(533);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0006:
	{
 		if (skipdoall('p004e0006')) break p004e0006;
		if (!CNDeq(98,100)) break p004e0006;
		if (!CNDeq(99,52)) break p004e0006;
 		ACCminus(31,1);
 		ACCwriteln(534);
 		ACCminus(31,1);
 		ACCextern(535);
 		ACCanykey();
 		function anykey00010() 
		{
 		ACCramload(255);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00010);
		done_flag=true;
		break pro004_restart;
		{}

	}

	// _ _
	p004e0007:
	{
 		if (skipdoall('p004e0007')) break p004e0007;
		if (!CNDeq(98,100)) break p004e0007;
		if (!CNDeq(99,53)) break p004e0007;
 		ACCgoto(getFlag(62));
 		ACCminus(31,1);
 		ACCramsave();
 		ACCwriteln(536);
 		ACCminus(31,1);
 		ACCextern(537);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0008:
	{
 		if (skipdoall('p004e0008')) break p004e0008;
		if (!CNDeq(98,100)) break p004e0008;
		if (!CNDeq(99,27)) break p004e0008;
 		ACCwriteln(538);
 		ACCminus(31,1);
 		ACCextern(539);
 		ACCnotdone();
		break pro004_restart;
		{}

	}

	// _ _
	p004e0009:
	{
 		if (skipdoall('p004e0009')) break p004e0009;
 		ACCprocess(3);
		{}

	}


}
}

last_process = 4;
// This file is (C) Carlos Sanchez 2014, released under the MIT license

// This function is called first by the start() function that runs when the game starts for the first time
var h_init = function()
{
}


// This function is called last by the start() function that runs when the game starts for the first time
var h_post =  function()
{
}

// This function is called when the engine tries to write any text
var h_writeText =  function (text)
{
	return text;
}

//This function is called every time the user types any order
var h_playerOrder = function(player_order)
{
	return player_order;
}

// This function is called every time a location is described, just after the location text is written
var h_description_init =  function ()
{
}

// This function is called every time a location is described, just after the process 1 is executed
var h_description_post = function()
{
}


// this function is called when the savegame object has been created, in order to be able to add more custom properties
var h_saveGame = function(savegame_object)
{
	return savegame_object;
}


// this function is called after the restore game function has restored the standard information in savegame, in order to restore any additional data included in a patched (by h_saveGame) savegame.
var h_restoreGame = function(savegame_object)
{
}

// this funcion is called before writing a message about player order beeing impossible to understand
var h_invalidOrder = function(player_order)
{
}

// this function is called when a sequence tag is found giving a chance for any hook library to provide a response
// tagparams receives the params inside the tag as an array  {XXXX|nn|mm|yy} => ['XXXX', 'nn', 'mm', 'yy']
var h_sequencetag = function (tagparams)
{
	return '';
}

// this function is called from certain points in the response or process tables via the HOOK condact. Depending on the string received it can do something or not.
// it's designed to allow direct javascript code to take control in the start database just installing a plugin library (avoiding the wirter need to enter code to activate the library)
var h_code = function(str)
{
	return false;
}


// this function is called from the keydown evente handler used by block and other functions to emulate a pause or waiting for a keypress. It is designed to allow plugin condacts or
// libraries to attend those key presses and react accordingly. In case a hook function decides that the standard keydown functions should not be processed, the hook function should return false.
// Also, any h_keydown replacement should probably do the same.
var h_keydown = function (event)
{
	return true;
}


// this function is called every time a process is called,  either by the internall loop of by the PROCESS condact, just before running it.
var h_preProcess = function(procno)
{

}

// this function is called every time a process is called just after the process exits (no matter which DONE status it has), either by the internall loop of by the PROCESS condact
var h_postProcess= function (procno)
{

}// This file is (C) Carlos Sanchez 2014, and is released under the MIT license
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ACCdesc()
{
	describe_location_flag = true;
	ACCbreak(); // Cancel doall loop
}


function ACCdone()
{
	done_flag = true;
}

function CNDat(locno)
{
  return (loc_here()==locno);
}

function CNDnotat(locno)
{
	 return (loc_here()!=locno);
}


function CNDatgt(locno)
{
	 return (loc_here()>locno);
}


function CNDatlt(locno)
{
	 return (loc_here()<locno);
}

function CNDpresent(objno)
{
	var loc = getObjectLocation(objno);
	if (loc == loc_here()) return true;
	if (loc == LOCATION_WORN) return true;
	if (loc == LOCATION_CARRIED) return true;
	if ( (!bittest(getFlag(FLAG_PARSER_SETTINGS),7)) && (objectIsContainer(loc) || objectIsSupporter(loc))  &&  (loc<=last_object_number)  && (CNDpresent(loc)) )  // Extended context and object in another object that is present
	{
		if (objectIsSupporter(loc)) return true;  // On supporter
		if ( objectIsContainer(loc) && objectIsAttr(loc, ATTR_OPENABLE) && objectIsAttr(loc, ATTR_OPEN)) return true; // In a openable & open container
		if ( objectIsContainer(loc) && (!objectIsAttr(loc, ATTR_OPENABLE)) ) return true; // In a not openable container
	}
	return false;
}

function CNDabsent(objno)
{
	return !CNDpresent(objno);
}

function CNDworn(objno)
{
	return (getObjectLocation(objno) == LOCATION_WORN);
}

function CNDnotworn(objno)
{
	return !CNDworn(objno);
}

function CNDcarried(objno)
{
	return (getObjectLocation(objno) == LOCATION_CARRIED);	
}

function CNDnotcarr(objno)
{
	return !CNDcarried(objno);
}


function CNDchance(percent)
{
	 var val = Math.floor((Math.random()*101));
	 return (val<=percent);
}

function CNDzero(flagno)
{
	return (getFlag(flagno) == 0);
}

function CNDnotzero(flagno)
{
	 return !CNDzero(flagno)
}


function CNDeq(flagno, value)
{
	return (getFlag(flagno) == value);
}

function CNDnoteq(flagno,value)
{
	return !CNDeq(flagno, value);
}

function CNDgt(flagno, value)
{
	return (getFlag(flagno) > value);
}

function CNDlt(flagno, value)
{
	return (getFlag(flagno) < value);
}


function CNDadject1(wordno)
{
	return (getFlag(FLAG_ADJECT1) == wordno);
}

function CNDadverb(wordno)
{
	return (getFlag(FLAG_ADVERB) == wordno);
}


function CNDtimeout()
{
	 return bittest(getFlag(FLAG_TIMEOUT_SETTINGS),7);
}


function CNDisat(objno, locno)
{
	return (getObjectLocation(objno) == locno);

}


function CNDisnotat(objno, locno)
{
	return !CNDisat(objno, locno);
}



function CNDprep(wordno)
{
	return (getFlag(FLAG_PREP) == wordno);
}




function CNDnoun2(wordno)
{
	return (getFlag(FLAG_NOUN2) == wordno);
}

function CNDadject2(wordno)
{
	return (getFlag(FLAG_ADJECT2) == wordno);
}

function CNDsame(flagno1,flagno2)
{
	return (getFlag(flagno1) == getFlag(flagno2));
}


function CNDnotsame(flagno1,flagno2)
{
	return (getFlag(flagno1) != getFlag(flagno2));
}

function ACCinven()
{
	var count = 0;
	writeSysMessage(SYSMESS_YOUARECARRYING);
	ACCnewline();
	var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS),3);
	var i;
	for (i=0;i<num_objects;i++)
	{
		if ((getObjectLocation(i)) == LOCATION_CARRIED)
		{
			
			if ((listnpcs_with_objects) || (!objectIsNPC(i)))
			{
				writeObject(i);
				if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
				ACCnewline();
				count++;
			}
		}
		if (getObjectLocation(i) == LOCATION_WORN)
		{
			if (listnpcs_with_objects || (!objectIsNPC(i)))
			{
				writeObject(i);
				writeSysMessage(SYSMESS_WORN);
				count++;
				ACCnewline();
			}
		}
	}
	if (!count) 
	{
		 writeSysMessage(SYSMESS_CARRYING_NOTHING);
		 ACCnewline();
	}

	if (!listnpcs_with_objects)
	{
		var numNPC = getNPCCountAt(LOCATION_CARRIED);
		if (numNPC)	ACClistnpc(LOCATION_CARRIED);
	}
	done_flag = true;
}

function desc()
{
	describe_location_flag = true;
}


function ACCquit()
{
	inQUIT = true;
	writeSysMessage(SYSMESS_AREYOUSURE);
}


function ACCend()
{
	$('.input').hide();
	inEND = true;
	writeSysMessage(SYSMESS_PLAYAGAIN);
	done_flag = true;
}


function done()
{
	done_flag = true;
}

function ACCok()
{
	writeSysMessage(SYSMESS_OK);
	done_flag = true;
}



function ACCramsave()
{
	ramsave_value = getSaveGameObject();
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	localStorage.setItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME, savegame);
}

function ACCramload()
{
	if (ramsave_value==null) 
	{
		var json_str = localStorage.getItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME);
		if (json_str)
		{
			savegame_object = JSON.parse(json_str.trim());
			restoreSaveGameObject(savegame_object);
			ACCdesc();
			focusInput();
			return;
		}
		else
		{
			writeText (STR_RAMLOAD_ERROR); 
			done_flag = true; 
			return;
		}
	}
	restoreSaveGameObject(ramsave_value);
	ACCdesc();
}

function ACCsave()
{
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	filename = prompt(getSysMessageText(SYSMESS_SAVEFILE),'');
	if ( filename !== null ) localStorage.setItem('ngpaws_savegame_' + filename.toUpperCase(), savegame);
	ACCok();
}

 
function ACCload() 	
{
	var json_str;
	filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
	if ( filename !== null ) json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
	if (json_str)
	{
		savegame_object = JSON.parse(json_str.trim());
		restoreSaveGameObject(savegame_object);
	}
	else
	{
		writeSysMessage(SYSMESS_FILENOTFOUND);
		done_flag = true; return;
	}
	ACCdesc();
	focusInput();
}



function ACCturns()
{
	var turns = getFlag(FLAG_TURNS_HIGH) * 256 +  getFlag(FLAG_TURNS_LOW);
	writeSysMessage(SYSMESS_TURNS_START);
	writeText(turns + '');
	writeSysMessage(SYSMESS_TURNS_CONTINUE);
	if (turns > 1) writeSysMessage(SYSMESS_TURNS_PLURAL);
	writeSysMessage(SYSMESS_TURNS_END);
}

function ACCscore()
{
	var score = getFlag(FLAG_SCORE);
	writeSysMessage(SYSMESS_SCORE_START);
	writeText(score + '');
	writeSysMessage(SYSMESS_SCORE_END);
}


function ACCcls()
{
	clearScreen();
}

function ACCdropall()
{
	// Done in two different loops cause PAW did it like that, just a question of retro compatibility
	var i;
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_CARRIED)setObjectLocation(i, getFlag(FLAG_LOCATION));
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_WORN)setObjectLocation(i, getFlag(FLAG_LOCATION));
}


function ACCautog()
{
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),7))  // Extended context for objects
	for (var i=0; i<num_objects;i++) // Try to find it in present containers/supporters
	{
		if (CNDpresent(i) && (isAccesibleContainer(i) || objectIsAttr(i, ATTR_SUPPORTER)) )  // If there is another object present that is an accesible container or a supporter
		{
			objno =findMatchingObject(i);
			if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
		}
	}
	success = false;
	writeSysMessage(SYSMESS_CANTSEETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautod()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };  
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautow()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautor()
{
	var objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOURENOTWEARINGTHAT);
	ACCnewtext();
	ACCdone();
}



function ACCpause(value)
{
 if (value == 0) value = 256;
 pauseRemainingTime = Math.floor(value /50 * 1000);	
 inPause = true;
 showAnykeyLayer();
} 

function ACCgoto(locno)
{
 	setFlag(FLAG_LOCATION,locno);
}

function ACCmessage(mesno)
{
	writeMessage(mesno);
	ACCnewline();
}


function ACCremove(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case loc_here():
			writeSysMessage(SYSMESS_YOUARENOTWEARINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case LOCATION_WORN:
			if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
			{
				writeSysMessage(SYSMESS_CANTREMOVE_TOOMANYOBJECTS);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_CARRIED);
			writeSysMessage(SYSMESS_YOUREMOVEOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUARENOTWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}


function trytoGet(objno)  // auxiliaty function for ACCget
{
	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		doall_flag = false;
		return;
	}
	var weight = 0;
	weight += getObjectWeight(objno);
	weight +=  getLocationObjectsWeight(LOCATION_CARRIED);
	weight +=  getLocationObjectsWeight(LOCATION_WORN);
	if (weight > getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}
	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;
}


 function ACCget(objno)
 {
 	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():
			trytoGet(objno);
			break;

		default: 
			if  ((locno<=last_object_number) && (CNDpresent(locno)))    // If it's not here, carried or worn but it present, that means that bit 7 of flag 12 is cleared, thus you can get objects from present containers/supporters
			{
				trytoGet(objno);
			}
			else
			{
				writeSysMessage(SYSMESS_CANTSEETHAT);
				ACCnewtext();
				ACCdone();
				return;
				break;
		    }
	}
 }

function ACCdrop(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			setObjectLocation(objno, loc_here());
			writeSysMessage(SYSMESS_YOUDROPOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}

function ACCwear(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWAERINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			if (!objectIsWearable(objno))
			{
				writeSysMessage(SYSMESS_YOUCANTWEAROBJECT);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_WORN);
			writeSysMessage(SYSMESS_YOUWEAROBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}



function ACCdestroy(objno)
{
	setObjectLocation(objno, LOCATION_NONCREATED);
}


function ACCcreate(objno)
{
	setObjectLocation(objno, loc_here());
}


function ACCswap(objno1,objno2)
{
	var locno1 = getObjectLocation (objno1);
	var locno2 = getObjectLocation (objno2);
	ACCplace (objno1,locno2);
	ACCplace (objno2,locno1);
	setReferredObject(objno2);
}


function ACCplace(objno, locno)
{
	setObjectLocation(objno, locno);
}

function ACCset(flagno)
{
	setFlag(flagno, SET_VALUE);
}

function ACCclear(flagno)
{
	setFlag(flagno,0);
}

function ACCplus(flagno,value)
{
	var newval = getFlag(flagno) + value;
	setFlag(flagno, newval);
}

function ACCminus(flagno,value)
{
    var newval = getFlag(flagno) - value;
    if (newval < 0) newval = 0;
	setFlag(flagno, newval);
}

function ACClet(flagno,value)
{
	setFlag(flagno,value);
}

function ACCnewline()
{
	writeText(STR_NEWLINE);
}

function ACCprint(flagno)
{
	writeText(getFlag(flagno) +'');
}

function ACCsysmess(sysno)
{
	writeSysMessage(sysno);
}

function ACCcopyof(objno,flagno)
{
	setFlag(flagno, getObjectLocation(objno))
}

function ACCcopyoo(objno1, objno2)
{
	setObjectLocation(objno2,getObjectLocation(objno1));
	setReferredObject(objno2);
}

function ACCcopyfo(flagno,objno)
{
	setObjectLocation(objno, getFlag(flagno));
}

function ACCcopyff(flagno1, flagno2)
{
	setFlag(flagno2, getFlag(flagno1));
}

function ACCadd(flagno1, flagno2)
{
	var newval = getFlag(flagno1) + getFlag(flagno2);
	setFlag(flagno2, newval);
}

function ACCsub(flagno1,flagno2)
{
	var newval = getFlag(flagno2) - getFlag(flagno1);
	if (newval < 0) newval = 0;
	setFlag(flagno2, newval);
}


function CNDparse()
{
	return (!getLogicSentence());
}


function ACClistat(locno, container_objno)   // objno is a container/suppoter number, used to list contents of objects
{
  var listingContainer = false;
  if (arguments.length > 1) listingContainer = true;
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);  
  objscount = objscount - concealed_or_scenery_objcount;
  if (!listingContainer) setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
  if (!objscount) return;
  var continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  if (listingContainer) 
  	{
  		writeText(' (');
  		if (objectIsAttr(container_objno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_OVER_YOUCANSEE); else if (objectIsAttr(container_objno, ATTR_CONTAINER)) writeSysMessage(SYSMESS_INSIDE_YOUCANSEE);
  		continouslisting = true;  // listing contents of container always continuous
  	}
  
  if (!listingContainer)
  {
    setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
    if (!continouslisting) ACCnewline();
  }
  var progresscount = 0;
  for (var i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if  ( ((!objectIsNPC(i)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))  && (!objectIsAttr(i,ATTR_CONCEALED)) && (!objectIsAttr(i,ATTR_SCENERY))   ) // if not an NPC or parser setting say NPCs are considered objects, and object is not concealed nor scenery
  		  { 
  		     writeText(getObjectText(i)); 
  		     if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
  		     progresscount++
  		     if (continouslisting)
  		     {
		  			if (progresscount <= objscount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  					if (progresscount == objscount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  					if (!listingContainer) if (progresscount == objscount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
  if (arguments.length > 1) writeText(')');
}


function ACClistnpc(locno)
{
  var npccount =  getNPCCountAt(locno);
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  if (!npccount) return;
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  writeSysMessage(SYSMESS_NPCLISTSTART);
  if (!continouslisting) ACCnewline();
  if (npccount==1)  writeSysMessage(SYSMESS_NPCLISTCONTINUE); else writeSysMessage(SYSMESS_NPCLISTCONTINUE_PLURAL);
  var progresscount = 0;
  var i;
  for (i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if ( (objectIsNPC(i)) && (!objectIsAttr(i,ATTR_CONCEALED)) ) // only NPCs not concealed
  		  { 
  		     writeText(getObjectText(i)); 
  		     progresscount++
  		     if (continouslisting)
  		     {
		  	 	if (progresscount <= npccount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  			 	if (progresscount == npccount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  			 	if (progresscount == npccount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
}


function ACClistobj()
{
  var locno = loc_here();
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);

  objscount = objscount - concealed_or_scenery_objcount;
  if (objscount)
  {
	  writeSysMessage(SYSMESS_YOUCANSEE);
      ACClistat(loc_here());
  }
}

function ACCprocess(procno)
{
	if (procno > last_process) 
	{
		writeText(STR_WRONG_PROCESS);
		ACCnewtext();
		ACCdone();
	}
	callProcess(procno);
    if (describe_location_flag) done_flag = true;
}

function ACCmes(mesno)
{
	writeMessage(mesno);
}

function ACCmode(mode)
{
	setFlag(FLAG_MODE, mode);
}

function ACCtime(length, settings)
{
	setFlag(FLAG_TIMEOUT_LENGTH, length);
	setFlag(FLAG_TIMEOUT_SETTINGS, settings);
}

function ACCdoall(locno)
{
	doall_flag = true;
	if (locno == LOCATION_HERE) locno = loc_here();
	// Each object will be considered for doall loop if is at locno and it's not the object specified by the NOUN2/ADJECT2 pair and it's not a NPC (or setting to consider NPCs as objects is set)
	setFlag(FLAG_DOALL_LOC, locno);
	var doall_obj;
	doall_loop:
	for (doall_obj=0;(doall_obj<num_objects) && (doall_flag);doall_obj++)  
	{
		if (getObjectLocation(doall_obj) == locno)
			if ((!objectIsNPC(doall_obj)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3))) 
 			 if (!objectIsAttr(doall_obj, ATTR_CONCEALED)) 
 			  if (!objectIsAttr(doall_obj, ATTR_SCENERY)) 
				if (!( (objectsNoun[doall_obj]==getFlag(FLAG_NOUN2))  &&    ((objectsAdjective[doall_obj]==getFlag(FLAG_ADJECT2)) || (objectsAdjective[doall_obj]==EMPTY_WORD)) ) ) // implements "TAKE ALL EXCEPT BIG SWORD"
				{
					setFlag(FLAG_NOUN1, objectsNoun[doall_obj]);
					setFlag(FLAG_ADJECT1, objectsAdjective[doall_obj]);
					setReferredObject(doall_obj);
					callProcess(process_in_doall);
					if (describe_location_flag) 
						{
							doall_flag = false;
							entry_for_doall = '';
							break doall_loop;
						}
				}
	}
	doall_flag = false;
	entry_for_doall = '';
	if (describe_location_flag) descriptionLoop();
}

function ACCprompt(value)  // deprecated
{
	setFlag(FLAG_PROMPT, value);
	setInputPlaceHolder();
}


function ACCweigh(objno, flagno)
{
	var weight = getObjectWeight(objno);
	setFlag(flagno, weight);
}

function ACCputin(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if (getObjectLocation(objno) == LOCATION_WORN)
	{
		writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == LOCATION_CARRIED)
	{
		setObjectLocation(objno, locno);
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUPUTOBJECTON); else writeSysMessage(SYSMESS_YOUPUTOBJECTIN);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		success = true;
		return;
	}

	writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
	ACCnewtext();
	ACCdone();
}


function ACCtakeout(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if ((getObjectLocation(objno) == LOCATION_WORN) || (getObjectLocation(objno) == LOCATION_CARRIED))
	{
		writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectWeight(objno) + getLocationObjectsWeight(LOCATION_WORN) + getLocationObjectsWeight(LOCATION_CARRIED) >  getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{		
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		return;
	}

	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;


}
function ACCnewtext()
{
	player_order_buffer = '';
}

function ACCability(maxObjectsCarried, maxWeightCarried)
{
	setFlag(FLAG_MAXOBJECTS_CARRIED, maxObjectsCarried);
	setFlag(FLAG_MAXWEIGHT_CARRIED, maxWeightCarried);
}

function ACCweight(flagno)
{
	var weight_carried = getLocationObjectsWeight(LOCATION_CARRIED);
	var weight_worn = getLocationObjectsWeight(LOCATION_WORN);
	var total_weight = weight_worn + weight_carried;
	setFlag(flagno, total_weight);
}


function ACCrandom(flagno)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*100)));
}

function ACCwhato()
{
	var whatofound = getReferredObject();
	if (whatofound != EMPTY_OBJECT) setReferredObject(whatofound);
}

function ACCputo(locno)
{
	setObjectLocation(getFlag(FLAG_REFERRED_OBJECT), locno);
}

function ACCnotdone()
{
	done_flag = false;
}

function ACCautop(locno)
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautot(locno)
{

	var objno =findMatchingObject(locno);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };

	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
			writeText(getObjectFixArticles(locno));
			writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION)
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
	
}


function CNDmove(flagno)
{
	var locno = getFlag(flagno);
	var dirno = getFlag(FLAG_VERB);
	var destination = getConnection( locno,  dirno);
	if (destination != -1) 
		{
			 setFlag(flagno, destination);
			 return true;
		}
	return false;
}


function ACCextern(writeno)
{
	eval(writemessages[writeno]);
}


function ACCpicture(picno)
{
	drawPicture(picno);
}



function ACCgraphic(option)
{
	graphicsON = (option==1);  
	if (!graphicsON) hideGraphicsWindow();	
}

function ACCbeep(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'play');
}

function ACCsound(value)
{
	soundsON = (value==1);  
	if (!soundsON) sfxstopall();
}

function CNDozero(objno, attrno)
{
	if (attrno > 63) return false;
	return !objectIsAttr(objno, attrno);

}

function CNDonotzero(objno, attrno)
{
	return objectIsAttr(objno, attrno);
}

function ACCoset(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		attrs = getObjectLowAttributes(objno);
		var attrs = bitset(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitset(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}

function ACCoclear(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitclear(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitclear(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}


function CNDislight()
{
	if (!isDarkHere()) return true;
	return lightObjectsPresent();
}



function CNDisnotlight()
{
	return ! CNDislight();
}

function ACCversion()
{
	writeText(filterText(STR_RUNTIME_VERSION));
}


function ACCwrite(writeno)
{
	writeWriteMessage(writeno);
}

function ACCwriteln(writeno)
{
	writeWriteMessage(writeno);
	ACCnewline();
}

function ACCrestart()
{
  process_restart = true;
}


function ACCtranscript()
{
	$('#transcript_area').html(transcript);
	$('.transcript_layer').show();
	inTranscript = true;
}

function ACCanykey()
{
	writeSysMessage(SYSMESS_PRESSANYKEY);
	inAnykey = true;
}

function ACCgetkey(flagno)
{
	getkey_return_flag = flagno;
	inGetkey = true;
}


//////////////////
//   LEGACY     //
//////////////////

// From PAW PC
function ACCbell()
{
 	// Empty, PAW PC legacy, just does nothing 
}


// From PAW Spectrum
function ACCreset()
{
	// Legacy condact, does nothing now
}


function ACCpaper(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCink(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCborder(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCcharset(value)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCline(lineno)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCinput()
{
	// Legacy condact, does nothing now
}

function ACCsaveat()
{
	// Legacy condact, does nothing now
}

function ACCbackat()
{
	// Legacy condact, does nothing now
}

function ACCprintat()
{
	// Legacy condact, does nothing now
}

function ACCprotect()
{
	// Legacy condact, does nothing now
}

// From Superglus


function ACCdebug()
{
	// Legacy condact, does nothing now		
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS FOR COMPILER //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CNDverb(wordno)
{
	return (getFlag(FLAG_VERB) == wordno);
}


function CNDnoun1(wordno)
{
	return (getFlag(FLAG_NOUN1) == wordno);
}

//   PLUGINS    ;

//CND ASK W 14 14 1 0

// Global vars for ASK


var inAsk = false;
var ask_responses = null;
var ask_flagno = null;



function ACCask(writeno, writenoOptions, flagno)
{
	inAsk = true;
	writeWriteMessage(writeno);
	ask_responses = getWriteMessageText(writenoOptions);
	ask_flagno = flagno;
}



// hook replacement
var old_ask_h_keydown  = h_keydown;
h_keydown  = function (event)
{
	if (inAsk)
	{
		var keyCodeAsChar = String.fromCharCode(event.keyCode).toLowerCase();
		if (ask_responses.indexOf(keyCodeAsChar)!= -1)
		{
			setFlag(ask_flagno, ask_responses.indexOf(keyCodeAsChar));
			inAsk = false;
			event.preventDefault();
            $('.input').show();
		    $('.input').focus();
		    hideBlock();
			waitKeyCallback();
		};
		return false; // if we are in ASK condact, no keypress should be considered other than ASK response
	} else return old_ask_h_keydown(event);
}

//CND ATGE C 8 0 0 0

function CNDatge(locno)
{
	return (getFlag(FLAG_LOCATION) >= locno);
}

//CND ATLE C 8 0 0 0

function CNDatle(locno)
{
	return (getFlag(FLAG_LOCATION) <= locno);
}

//CND BCLEAR A 1 2 0 0

function ACCbclear(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitclear(getFlag(flagno), bitno));
}
//CND BLOCK A 14 2 2 0

function ACCblock(writeno, picno, procno)
{
   inBlock = true;
   disableInterrupt();
   $('.block_layer').hide();
   var text = getWriteMessageText(writeno);
   $('.block_text').html(text);
   
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var imgsrc = '<img class="block_picture" src="' + filename + '" />';
		$('.block_graphics').html(imgsrc);
	}
    if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
    $('.block_layer').show();

}
//CND BNEG A 1 2 0 0

function ACCbneg(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitneg(getFlag(flagno),bitno));
}
//CND BNOTZERO C 1 2 0 0

function CNDbnotzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (bittest(getFlag(flagno), bitno));
}

//CND BREAK A 0 0 0 0

function ACCbreak()
{
	doall_flag = false; 
	entry_for_doall = '';
}
//CND BSET A 1 2 0 0

function ACCbset(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitset(getFlag(flagno),bitno));
}
//CND BZERO C 1 2 0 0

function CNDbzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (!bittest(getFlag(flagno), bitno));
}
//CND CLEAREXIT A 2 0 0 0

function ACCclearexit(wordno)
{
	if ((wordno >= NUM_CONNECTION_VERBS) || (wordno< 0 )) return;
	setConnection(loc_here(),wordno, -1);
}
//CND COMMAND A 2 0 0 0

function ACCcommand(value)
{
	if (value) {$('.input').show();$('.input').focus();} else $('.input').hide();
}
//CND DIV A 1 2 0 0

function ACCdiv(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) / valor));
}
//CND EXITS A 8 5 0 0

function ACCexits(locno,mesno)
{
  if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent()))
  {
  		var exitcount = 0;
  		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1) exitcount++;
      if (exitcount)
      {
    		writeMessage(mesno);
    		var exitcountprogress = 0;
    		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1)
    		{ 
    			exitcountprogress++;
    			writeMessage(mesno + 2 + i);
    			if (exitcountprogress == exitcount) writeSysMessage(SYSMESS_LISTEND);
    			if (exitcountprogress == exitcount-1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
    			if (exitcountprogress <= exitcount-2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  		  }
      } else writeMessage(mesno + 1);
  } else writeMessage(mesno + 1);
}

//CND FADEIN A 2 2 2 0

function ACCfadein(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'fadein');
}
//CND FADEOUT A 2 2 0 0

function ACCfadeout(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxfadeout(channelno, value);
}
//CND GE C 1 2 0 0

function CNDge(flagno, valor)
{
	return (getFlag(flagno)>=valor);
}
//CND GETEXIT A 2 2 0 0

function ACCgetexit(value,flagno)
{
	if (value >= NUM_CONNECTION_VERBS) 
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	var locno = getConnection(loc_here(),value);
	if (locno == -1)
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	setFlag(flagno,locno);
}
//CND HELP A 0 0 0 0

function ACChelp()
{
	if (getLang()=='EN') EnglishHelp();
	else if (getLang()=='ES') SpanishHelp();
	else if (getLang()=='CA') CatalanHelp();
	else NoHelp();
}	

function NoHelp()
{
	writeText("Lang Error. Please check flag 59");
}

function EnglishHelp()
{
	writeText('HOW DO I SEND COMMANDS TO THE PC?');
	writeText(STR_NEWLINE);
	writeText('Use simple orders: OPEN DOOR, TAKE KEY, GO UP, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I MOVE IN THE MAP?');
	writeText(STR_NEWLINE);
	writeText('Usually you will have to use compass directions as north (shortcut: "N"), south (S), east (E), west (W) or other directions (up, down, enter, leave, etc.). Some games allow complex order like "go to well". Usually you would be able to know avaliable exits by location description, some games also provide the "EXITS" command.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK MY INVENTORY?');
	writeText(STR_NEWLINE);
	writeText('type INVENTORY (shortcut "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I USE THE OBJECTS?');
	writeText(STR_NEWLINE);
	writeText('Use the proper verb, that is, instead of USE KEY type OPEN.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK SOMETHING CLOSELY?');
	writeText(STR_NEWLINE);
	writeText('Use "examine" verb: EXAMINE DISH. (shortcut: EX)');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SEE AGAIN THE CURRENT LOCATION DSCRIPTION?');
	writeText(STR_NEWLINE);
	writeText('Type LOOK (shortcut "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I TALK TO OTHER CHARACTERS?');
	writeText(STR_NEWLINE);
	writeText('Most common methods are [CHARACTER, SENTENCE] or [SAY CHARACTER "SENTENCE"]. For instance: [JOHN, HELLO] o [SAY JOHN "HELLO"]. Some games also allow just [TALK TO JOHN]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING IN A CONTAINER, HOW CAN I TAKE SOMETHING OUT?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY IN BOX. TAKE KEY OUT OF BOX. INSERT KEY IN BOX. EXTRACT KEY FROM BOX.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING ON SOMETHING ELSE?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY ON TABLE. TAKE KEY FROM TABLE');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

function SpanishHelp()
{
	writeText('¿CÓMO DOY ORDENES AL PERSONAJE?');
	writeText(STR_NEWLINE);
	writeText('Utiliza órdenes en imperativo o infinitivo: ABRE PUERTA, COGER LLAVE, SUBIR, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO ME MUEVO POR EL JUEGO?');
	writeText(STR_NEWLINE);
	writeText('Por regla general, mediante los puntos cardinales como norte (abreviado "N"), sur (S), este (E), oeste (O) o direcciones espaciales (arriba, abajo, bajar, subir, entrar, salir, etc.). Algunas aventuras permiten también cosas como "ir a pozo". Normalmente podrás saber en que dirección puedes ir por la descripción del sitio, aunque algunos juegos facilitan el comando "SALIDAS" que te dirá exactamente cuales hay.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO SABER QUE OBJETOS LLEVO?');
	writeText(STR_NEWLINE);
	writeText('Teclea INVENTARIO (abreviado "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO USO LOS OBJETOS?');
	writeText(STR_NEWLINE);
	writeText('Utiliza el verbo correcto, en lugar de USAR ESCOBA escribe BARRER.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO MIRAR DE CERCA UN OBJETO U OBSERVARLO MÁS DETALLADAMENTE?');
	writeText(STR_NEWLINE);
	writeText('Con el verbo examinar: EXAMINAR PLATO. Generalmente se puede usar la abreviatura "EX" : EX PLATO.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO VER DE NUEVO LA DESCRIPCIÓN DEL SITIO DONDE ESTOY?');
	writeText(STR_NEWLINE);
	writeText('Escribe MIRAR (abreviado "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO HABLO CON LOS PERSONAJES?');
	writeText(STR_NEWLINE);
	writeText('Los modos más comunes son [PERSONAJE, FRASE] o [DECIR A PERSONAJE "FRASE"]. Por ejemplo: [LUIS, HOLA] o [DECIR A LUIS "HOLA"]. En algunas aventuras también se puede utilizar el formato [HABLAR A LUIS]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO METO ALGO EN UN CONTENEDOR? ¿COMO SACO ALGO?');
	writeText(STR_NEWLINE);
	writeText('METER LLAVE  EN CAJA. SACAR LLAVE DE CAJA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PONGO ALGO SOBRE ALGO? ¿COMO LO QUITO?');
	writeText(STR_NEWLINE);
	writeText('PONER LLAVE EN MESA. COGER LLAVE DE MESA');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

function CatalanHelp()
{
	writeText('### TO DO HELP ###');
	writeText(STR_NEWLINE);
	writeText('- Utilitza ordres en imperatiu o infinitiu: OBRE PORTA, AGAFAR CLAU, PUJAR, etc.');
	writeText(STR_NEWLINE);
	writeText('- Mou-te mitjançant els punts cardinals com NORD (N), SUD (S), EST (E), OEST (O) o direccions espacials (PUJAR, BAIXAR, ENTRAR, SORTIR, etc.). Algunes aventures permeten coses com "ANAR AL POU". Generalment podràs saber on pots anar per la descripció del lloc, però alguns jocs faciliten la instrucció "SORTIDES (X)" per saber exactament on pots anar.');
	writeText(STR_NEWLINE);
	writeText('- Utilitza el verb correcte, en lloc d\'escriure UTILITZAR PISTOLA, escriu DISPARA.');
	writeText(STR_NEWLINE);
	writeText('- INVENTARI (IN). Consulta el objectes que portes. Algunes aventures poden incloure l\'inventari sempre visible a la pantalla.');
	writeText(STR_NEWLINE);
	writeText('- EXAMINAR (EX) - Mira objectes i entorn amb més detall o de més a prop: EXAMINAR PLAT. Aquesta és la instrucció bàsica per obtenir informació.');
	writeText(STR_NEWLINE);
	writeText('- MIRAR (M) - Torna a llegir la descripció del lloc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('Com parlo amb personatges?');
	writeText(STR_NEWLINE);
	writeText('- PARLAR ## - Parla amb personatges: PARLAR AMB MANEL Hi ha d\'altres maneres com [PERSONATGE, FRASE] o [DIR A PERSONATGE "FRASE"]. Per exemple: [MANEL, HOLA] o [DIR A MANEL "HOLA"].');
	writeText(STR_NEWLINE);
	writeText('- DESAR / CARREGAR - Desa o carrega la partida a les cookies del navegador. Posa-hi un nom per desar o escriu el nom per carregar');
	writeText(STR_NEWLINE + STR_NEWLINE);

}
//CND HOOK A 14 0 0 5

function ACChook(writeno)
{
	h_code(writemessages[writeno]);
}
//CND ISDOALL C 0 0 0 0

function CNDisdoall()
{
	return doall_flag;	
}

//CND ISDONE C 0 0 0 0

function CNDisdone()
{
	return done_flag;	
}

//CND ISMOV C 0 0 0 0

function CNDismov()
{
	if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)==EMPTY_WORD)) return true;

	if ((getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_VERB)==EMPTY_WORD)) return true;

    if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS)) return true;
    
    return false;
}

//CND ISMUSIC C 0 0 0 0

function CNDismusic()
{
	return (CNDissound(0));	
}

//CND ISNOTDOALL C 0 0 0 0

function CNDisnotdoall()
{
	return !CNDisdoall();
}

//CND ISNOTDONE C 0 0 0 0

function CNDisnotdone()
{
	return !CNDisdone();
}

//CND ISNOTMOV C 0 0 0 0

function CNDisnotmov()
{
	return !CNDismov();	
}

//CND ISNOTMUSIC C 0 0 0 0

function CNDisnotmusic()
{
  return !CNDismusic();
}

//CND ISNOTRESP C 0 0 0 0

function CNDisnotresp()
{
	return !in_response;	
}

//CND ISNOTSOUND C 1 0 0 0

function CNDisnotsound(channelno)
{
  if ((channelno <1) || (channelno >MAX_CHANNELS)) return false;
  return !(CNDissound(channelno));
}
//CND ISRESP C 0 0 0 0

function CNDisresp()
{
	return in_response;	
}

//CND ISSOUND C 1 0 0 0

function CNDissound(channelno)
{
	if ((channelno <1 ) || (channelno > MAX_CHANNELS)) return false;
    return channelActive(channelno);
}
//CND ISVIDEO C 0 0 0 0

function CNDisvideo()
{
	if (typeof videoElement == 'undefined') return false;
	if (!videoLoopCount) return false;
	if (videoElement.paused) return false;
	return true;
}

//CND LE C 1 2 0 0

function CNDle(flagno, valor)
{
	return (getFlag(flagno) <= valor);
}
//CND LISTCONTENTS A 9 0 0 0

function ACClistcontents(locno)
{
   ACClistat(locno, locno)
}
//CND LOG A 14 0 0 0

function ACClog(writeno)
{
  console_log(writemessages[writeno]);
}
//CND MOD A 1 2 0 0

function ACCmod(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) % valor));
}
//CND MUL A 1 2 0 0

function ACCmul(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) * valor));
}
//CND NORESP A 0 0 0 0

function ACCnoresp()
{
	in_response = false;
}	

//CND NPCAT A 9 1 0 0

function ACCnpcat(locno, flagno)
{
	setFlag(flagno,getNPCCountAt(locno));
}

//CND OBJAT A 9 1 0 0

function ACCobjat(locno, flagno)
{
	setFlag(flagno, getObjectCountAt(locno));
}
//CND OBJFOUND C 2 9 0 0

function CNDobjfound(attrno, locno)
{

	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return true; }
	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return false;
}

//CND OBJNOTFOUND C 2 9 0 0

function CNDobjnotfound(attrno, locno)
{
	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return false; }

	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return true;
}
//CND ONEG A 4 2 0 0

function ACConeg(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitneg(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitneg(attrs, attrno);
	setObjectHighAttributes(objno, attrs);
}

//CND PAUSEVIDEO A 0 0 0 0


function ACCpausevideo()
{
	if (typeof videoElement != 'undefined') 
		if (!videoElement.ended) 
		if (!videoElement.paused)
		   videoElement.pause();
}

//CND PICTUREAT A 2 2 2 0

/*
In order to determine the actual size of both background image and pictureat image they should be loaded, thus two chained "onload" are needed. That is, 
background image is loaded to determine its size, then pictureat image is loaded to determine its size. Size of currently displayed background image cannot
be used as it may have been already stretched.
*/

function ACCpictureat(x,y,picno)
{
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (!filename) return;

	// Check location has a picture, otherwise exit
	var currentBackgroundScreenImage = $('.location_picture');
	if (!currentBackgroundScreenImage) return;

	// Create a new image with the contents of current background image, to be able to calculate original height of image
	var virtualBackgroundImage = new Image();
	// Pass required data as image properties in order to be avaliable at "onload" event
	virtualBackgroundImage.bg_data=[];
	virtualBackgroundImage.bg_data.filename = filename; 
	virtualBackgroundImage.bg_data.x = x;
	virtualBackgroundImage.bg_data.y = y;
	virtualBackgroundImage.bg_data.picno = picno;
	virtualBackgroundImage.bg_data.currentBackgroundScreenImage = currentBackgroundScreenImage;


	// Event triggered when virtual background image is loaded
	virtualBackgroundImage.onload = function()
		{
			var originalBackgroundImageHeight = this.height;
			var scale = this.bg_data.currentBackgroundScreenImage.height() / originalBackgroundImageHeight;

			// Create a new image with the contents of picture to show with PICTUREAT, to be able to calculate height of image
			var virtualPictureAtImage = new Image();
			// Also pass data from background image as property so they are avaliable in the onload event
			virtualPictureAtImage.pa_data = [];
			virtualPictureAtImage.pa_data.x = this.bg_data.x;
			virtualPictureAtImage.pa_data.y = this.bg_data.y;
			virtualPictureAtImage.pa_data.picno = this.bg_data.picno;
			virtualPictureAtImage.pa_data.filename = this.bg_data.filename;
			virtualPictureAtImage.pa_data.scale = scale;
			virtualPictureAtImage.pa_data.currentBackgroundImageWidth = this.bg_data.currentBackgroundScreenImage.width();
			
			// Event triggered when virtual PCITUREAT image is loaded
			virtualPictureAtImage.onload = function ()
			{
		    		var imageHeight = this.height; 
					var x = Math.floor(this.pa_data.x * this.pa_data.scale);
					var y = Math.floor(this.pa_data.y * this.pa_data.scale);
					var newimageHeight = Math.floor(imageHeight * this.pa_data.scale);
					var actualBackgroundImageX = Math.floor((parseInt($('.graphics').width()) - this.pa_data.currentBackgroundImageWidth)/2);;
					var id = 'pictureat_' + this.pa_data.picno;

					// Add new image, notice we are not using the virtual image, but creating a new one
					$('.graphics').append('<img  alt="" id="'+id+'" style="display:none" />');				
					$('#' + id).css('position','absolute');
					$('#' + id).css('left', actualBackgroundImageX + x  + 'px');
					$('#' + id).css('top',y + 'px');
					$('#' + id).css('z-index','100');
					$('#' + id).attr('src', this.pa_data.filename);
					$('#' + id).css('height',newimageHeight + 'px');
					$('#' + id).show();
			}

			// Assign the virtual pictureat image the destinationsrc to trigger the "onload" event
			virtualPictureAtImage.src = this.bg_data.filename;
			};

	// Assign the virtual background image same src as current background to trigger the "onload" event
	virtualBackgroundImage.src = currentBackgroundScreenImage.attr("src");

}

//CND PLAYVIDEO A 14 2 2 0

var videoLoopCount;
var videoEscapable;
var videoElement;

function ACCplayvideo(strno, loopCount, settings)
{
	videoEscapable = settings & 1; // if bit 0 of settings is 1, video can be interrupted with ESC key
	if (loopCount == 0) loopCount = -1;
	videoLoopCount = loopCount;

	str = '<video id="videoframe" height="100%">';
	str = str + '<source src="dat/' + writemessages[strno] + '.mp4" type="video/mp4" codecs="avc1.4D401E, mp4a.40.2">';
	str = str + '<source src="dat/' + writemessages[strno] + '.webm" type="video/webm" codecs="vp8.0, vorbis">';
	str = str + '<source src="dat/' + writemessages[strno] + '.ogg" type="video/ogg" codecs="theora, vorbis">';
	str = str + '</video>';
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#videoframe').css('height','100%');
	$('#videoframe').css('display','block');
	$('#videoframe').css('margin-left','auto');
	$('#videoframe').css('margin-right','auto');
	$('#graphics').show();
	videoElement = document.getElementById('videoframe');
	videoElement.onended = function() 
	{
    	if (videoLoopCount == -1) videoElement.play();
    	else
    	{
    		videoLoopCount--;
    		if (videoLoopCount) videoElement.play();
    	}
	};
	videoElement.play();

}

// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_video_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#videoframe").length > 0) $("#videoframe").remove();	
	old_video_h_description_init();
}

// Hook into keypress to cancel video playing if ESC is pressed and video is skippable

var old_video_h_keydown =  h_keydown;
h_keydown = function (event)
{
 	if ((event.keyCode == 27) && (typeof videoElement != 'undefined') && (!videoElement.ended) && (videoEscapable)) 
 	{
 		videoElement.pause(); 
 		return false;  // we've finished attending ESC press
 	}
 	else return old_video_h_keydown(event);
}




//CND RANDOMX A 1 2 0 0

function ACCrandomx(flagno, value)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*value)));
}
//CND RESP A 0 0 0 0

function ACCresp()
{
	in_response = true;
}	

//CND RESUMEVIDEO A 0 0 0 0


function ACCresumevideo()
{
	if (typeof videoElement != 'undefined') 
		if (videoElement.paused)
		  videoElement.play();
}

//CND RNDWRITE A 14 14 14 0

function ACCrndwrite(writeno1,writeno2,writeno3)
{
	var val = Math.floor((Math.random()*3));
	switch (val)
	{
		case 0 : writeWriteMessage(writeno1);break;
		case 1 : writeWriteMessage(writeno2);break;
		case 2 : writeWriteMessage(writeno3);break;
	}
}
//CND RNDWRITELN A 14 14 14 0

function ACCrndwriteln(writeno1,writeno2,writeno3)
{
	ACCrndwrite(writeno1,writeno2,writeno3);
	ACCnewline();
}
//CND SETEXIT A 2 2 0 0

function ACCsetexit(value, locno)
{
	if (value < NUM_CONNECTION_VERBS) setConnection(loc_here(), value, locno);
}
//CND SETWEIGHT A 4 2 0 0

function ACCsetweight(objno, value)
{
   objectsWeight[objno] = value;
}

//CND SILENCE A 2 0 0 0

function ACCsilence(channelno)
{
	if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
	sfxstop(channelno);
}
//CND SOFTBLOCK A 2 0 0 0

function ACCsoftblock(procno)
{
   inBlock = true;
   disableInterrupt();

   $('.block_layer').css('display','none');
   $('.block_text').html('');
   $('.block_graphics').html('');
   $('.block_layer').css('background','transparent');
   if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
   $('.block_layer').css('display','block');
}
//CND SPACE A 0 0 0 0

function ACCspace()
{
	writeText(' ');
}
//CND SYNONYM A 15 13 0 0

function ACCsynonym(wordno1, wordno2)
{
   if (wordno1!=EMPTY_WORD) setFlag(FLAG_VERB, wordno1);
   if (wordno2!=EMPTY_WORD)	setFlag(FLAG_NOUN1, wordno2);
}
//CND TEXTPIC A 2 2 0 0

function ACCtextpic(picno, align)
{
	var style = '';
	var post = '';
	var pre = '';
	switch(align)
	{
		case 0: post='<br style="clear:left">';break;
		case 1: style = 'float:left'; break;
		case 2: style = 'float:right'; break;
		case 3: pre='<center>';post='</center><br style="clear:left">';break;
	}
	filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var texto = pre + "<img alt='' class='textpic' style='"+style+"' src='"+filename+"' />" + post;
		writeText(texto);
		$(".text").scrollTop($(".text")[0].scrollHeight);
	}
}
//CND TITLE A 14 0 0 0

function ACCtitle(writeno)
{
	document.title = writemessages[writeno];
}
//CND VOLUME A 2 2 0 0

function ACCvolume(channelno, value)
{
	if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
	sfxvolume(channelno, value);
}

//CND VOLUMEVIDEO A 2 0 0 0


function ACCvolumevideo(value)
{
	if (typeof videoElement != 'undefined') 
		videoElement.volume = value  / 65535;
}

//CND WARNINGS A 2 0 0 0

function ACCwarnings(value)
{
	if (value) showWarnings = true; else showWarnings = false;
}
//CND WHATOX A 1 0 0 0

function ACCwhatox(flagno)
{
	var whatoxfound = getReferredObject();
	setFlag(flagno,whatoxfound);
}

//CND WHATOX2 A 1 0 0 0

function ACCwhatox2(flagno)
{	
	var auxNoun = getFlag(FLAG_NOUN1);
	var auxAdj = getFlag(FLAG_ADJECT1);
	setFlag(FLAG_NOUN1, getFlag(FLAG_NOUN2));
	setFlag(FLAG_ADJECT1, getFlag(FLAG_ADJECT2));
	var whatox2found = getReferredObject();
	setFlag(flagno,whatox2found);
	setFlag(FLAG_NOUN1, auxNoun);
	setFlag(FLAG_ADJECT1, auxAdj);
}
//CND YOUTUBE A 14 0 0 0

function ACCyoutube(strno)
{

	var str = '<iframe id="youtube" width="560" height="315" src="http://www.youtube.com/embed/' + writemessages[strno] + '?autoplay=1&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>'
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#youtube').css('height','100%');
	$('#youtube').css('display','block');
	$('#youtube').css('margin-left','auto');
	$('#youtube').css('margin-right','auto');
	$('.graphics').show();
}


// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_youtube_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#youtube").length > 0) $("#youtube").remove();	
	old_youtube_h_description_init();
}
//CND ZONE C 8 8 0 0

function CNDzone(locno1, locno2)
{

	if (loc_here()<locno1) return false;
	if (loc_here()>locno2) return false;
	return true;
}
// This file is (C) Carlos Sanchez 2014, released under the MIT license


// IMPORTANT: Please notice this file must be encoded with the same encoding the index.html file is, so the "normalize" function works properly.
//            As currently the ngpwas compiler generates utf-8, and the index.html is using utf-8 also, this file must be using that encoding.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                         Auxiliary functions                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General functions
String.prototype.rights = function (n) {
	if (n <= 0)
		return "";
	else if (n > String(this).length)
		return this;
	else {
		var iLen = String(this).length;
		return String(this).substring(iLen, iLen - n);
	}
}


String.prototype.firstToLower = function () {
	return this.charAt(0).toLowerCase() + this.slice(1);
}


// Returns true if using Internet Explorer 9 or below, where some features are not supported
function isBadIE() {
	var myNav = navigator.userAgent.toLowerCase();
	if (myNav.indexOf('msie') == -1) return false;
	ieversion = parseInt(myNav.split('msie')[1]);
	return (ieversion < 10);
}


function runningLocal() {
	return (window.location.protocol == 'file:');
}


// Levenshtein function

function getLevenshteinDistance(a, b) {
	if (a.length == 0) return b.length;
	if (b.length == 0) return a.length;

	var matrix = [];

	// increment along the first column of each row
	var i;
	for (i = 0; i <= b.length; i++) {
		matrix[i] = [i];
	}

	// increment each column in the first row
	var j;
	for (j = 0; j <= a.length; j++) {
		matrix[0][j] = j;
	}

	// Fill in the rest of the matrix
	for (i = 1; i <= b.length; i++) {
		for (j = 1; j <= a.length; j++) {
			if (b.charAt(i - 1) == a.charAt(j - 1)) {
				matrix[i][j] = matrix[i - 1][j - 1];
			} else {
				matrix[i][j] = Math.min(matrix[i - 1][j - 1] + 1, // substitution
					Math.min(matrix[i][j - 1] + 1, // insertion
						matrix[i - 1][j] + 1)); // deletion
			}
		}
	}

	return matrix[b.length][a.length];
};

// waitKey helper for all key-wait condacts

function waitKey(callbackFunction) {
	waitkey_callback_function.push(callbackFunction);
	showAnykeyLayer();
}

function waitKeyCallback() {
	var callback = waitkey_callback_function.pop();
	if (callback) callback();
	if (describe_location_flag) descriptionLoop();
}


// Check DOALL entry

function skipdoall(entry) {
	return ((doall_flag == true) && (entry_for_doall != '') && (current_process == process_in_doall) && (entry_for_doall > entry));
}

// Dynamic attribute use functions
function getNextFreeAttribute() {
	var value = nextFreeAttr;
	nextFreeAttr++;
	return value;
}


// Gender functions

function getSimpleGender(objno)  // Simple, for english
{
	isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
	if (isPlural) return "P";
	isFemale = objectIsAttr(objno, ATTR_FEMALE);
	if (isFemale) return "F";
	isMale = objectIsAttr(objno, ATTR_MALE);
	if (isMale) return "M";
	return "N"; // Neuter
}

function getAdvancedGender(objno)  // Complex, for spanish
{
	var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
	var isFemale = objectIsAttr(objno, ATTR_FEMALE);
	var isMale = objectIsAttr(objno, ATTR_MALE);

	if (!isPlural) {
		if (isFemale) return "F";
		if (isMale) return "M";
		return "N"; // Neuter
	}
	else {
		if (isFemale) return "PF";
		if (isMale) return "PM";
		return "PN"; // Neuter plural
	}

}

function getAdvancedGenderCat(objno)  // Complex, for catalan
{
	var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
	var isFemale = objectIsAttr(objno, ATTR_FEMALE);
	var isMale = objectIsAttr(objno, ATTR_MALE);
	var isVocal = objectIsAttr(objno, ATTR_VOCAL);

	if (isVocal) {
		return "V";
	}

	if (!isPlural) {
		if (isFemale) return "F";
		if (isMale) return "M";
		return "N"; // Neuter
	}
	else {
		if (isFemale) return "PF";
		if (isMale) return "PM";
		return "PN"; // Neuter plural
	}

}

function getLang() {
	//var value = bittest(getFlag(FLAG_PARSER_SETTINGS),5);
	//if (value) return "ES"; else return "EN";

	switch (getFlag(59)) {
		case 0:
		default:
			return 'EN';
			break;
		case 1:
			return 'ES';
			break;
		case 2:
			return 'CA';
			break;
	}

}

function getObjectFixArticles(objno) {
	var object_text = getObjectText(objno);
	var object_words = object_text.split(' ');
	if (object_words.length == 1) return object_text;
	var candidate = object_words[0];
	object_words.splice(0, 1);
	if (getLang() == 'EN') {
		if ((candidate != 'an') && (candidate != 'a') && (candidate != 'some')) return object_text;
		return 'the ' + object_words.join(' ');
	}
	else if (getLang() == 'ES') {
		if ((candidate != 'un') && (candidate != 'una') && (candidate != 'unos') && (candidate != 'unas') && (candidate != 'alguna') && (candidate != 'algunos') && (candidate != 'algunas') && (candidate != 'algun')) return object_text;
		var gender = getAdvancedGender(objno);
		if (gender == 'F') return 'la ' + object_words.join(' ');
		if (gender == 'M') return 'el ' + object_words.join(' ');
		if (gender == 'N') return 'el ' + object_words.join(' ');
		if (gender == 'PF') return 'las ' + object_words.join(' ');
		if (gender == 'PM') return 'los ' + object_words.join(' ');
		if (gender == 'PN') return 'los ' + object_words.join(' ');
	}
	else if (getLang() == 'CA') {
		if ((candidate != 'un') && (candidate != 'una') && (candidate != 'uns') && (candidate != 'unes') && (candidate != 'alguna') && (candidate != 'alguns') && (candidate != 'algunes') && (candidate != 'algún')) return object_text;
		var gender = getAdvancedGenderCat(objno);
		if (gender == 'V') return 'l\'' + object_words.join(' ');
		if (gender == 'F') return 'la ' + object_words.join(' ');
		if (gender == 'M') return 'el ' + object_words.join(' ');
		if (gender == 'N') return 'el ' + object_words.join(' ');
		if (gender == 'PF') return 'les ' + object_words.join(' ');
		if (gender == 'PM') return 'els ' + object_words.join(' ');
		if (gender == 'PN') return 'els ' + object_words.join(' ');
	}
	else {
		return ''; //Error del flag 59
	}


}



// JS level log functions
function console_log(string) {
	if (typeof console != "undefined") console.log(string);
}


// Resources functions
function getResourceById(resource_type, id) {
	for (var i = 0; i < resources.length; i++)
		if ((resources[i][0] == resource_type) && (resources[i][1] == id)) return resources[i][2];
	return false;
}

// Flag read/write functions
function getFlag(flagno) {
	return flags[flagno];
}

function setFlag(flagno, value) {
	flags[flagno] = value;
}

// Locations functions
function loc_here()  // Returns current location, avoid direct use of flags
{
	return getFlag(FLAG_LOCATION);
}


// Connections functions

function setConnection(locno1, dirno, locno2) {
	connections[locno1][dirno] = locno2;
}

function getConnection(locno, dirno) {
	return connections[locno][dirno];
}

// Objects text functions

function getObjectText(objno) {
	return filterText(objects[objno]);
}


// Message text functions
function getMessageText(mesno) {
	return filterText(messages[mesno]);
}

function getSysMessageText(sysno) {
	return filterText(sysmessages[sysno]);
}

function getWriteMessageText(writeno) {
	return filterText(writemessages[writeno]);
}

// Location text functions
function getLocationText(locno) {
	return filterText(locations[locno]);
}



// Output processing functions
function implementTag(tag) {
	tagparams = tag.split('|');
	for (var tagindex = 0; tagindex < tagparams.length - 1; tagindex++) tagparams[tagindex] = tagparams[tagindex].trim();
	if (tagparams.length == 0) { writeWarning(STR_INVALID_TAG_SEQUENCE_EMPTY); return '' }

	var resolved_hook_value = h_sequencetag(tagparams);
	if (resolved_hook_value != '') return resolved_hook_value;

	switch (tagparams[0].toUpperCase()) {
		case 'URL': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<a target="newWindow" href="' + tagparams[1] + '">' + tagparams[2] + '</a>'; // Note: _blank would get the underscore character replaced by current selected object so I prefer to use a different target name as most browsers will open a new window
			break;
		case 'CLASS': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<span class="' + tagparams[1] + '">' + tagparams[2] + '</span>';
			break;
		case 'STYLE': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<span style="' + tagparams[1] + '">' + tagparams[2] + '</span>';
			break;
		case 'INK': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<span style="color:' + tagparams[1] + '">' + tagparams[2] + '</span>';
			break;
		case 'PAPER': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<span style="background-color:' + tagparams[1] + '">' + tagparams[2] + '</span>';
			break;
		case 'OBJECT': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			if (objects[getFlag(tagparams[1])]) return getObjectFixArticles(getFlag(tagparams[1])); else return '';
			break;
		case 'WEIGHT': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			if (objectsWeight[getFlag(tagparams[1])]) return objectsWeight[getFlag(tagparams[1])]; else return '';
			break;
		case 'OLOCATION': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			if (objectsLocation[getFlag(tagparams[1])]) return objectsLocation[getFlag(tagparams[1])]; else return '';
			break;
		case 'MESSAGE': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			if (messages[getFlag(tagparams[1])]) return getMessageText(getFlag(tagparams[1])); else return '';
			break;
		case 'SYSMESS': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			if (sysmessages[getFlag(tagparams[1])]) return getSysMessageText(getFlag(tagparams[1])); else return '';
			break;
		case 'LOCATION': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			if (locations[getFlag(tagparams[1])]) return getLocationText(getFlag(tagparams[1])); else return '';
			break;
		case 'PROCESS': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			callProcess(tagparams[1]);
			return "";
			break;
		case 'ACTION': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<a href="javascript: void(0)" onclick="orderEnteredLoop(\'' + tagparams[1] + '\')">' + tagparams[2] + '</a>';
			break;
		case 'RESTART': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<a href="javascript: void(0)" onclick="restart()">' + tagparams[1] + '</a>';
			break;
		case 'EXTERN': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return '<a href="javascript: void(0)" onclick="' + tagparams[1] + ' ">' + tagparams[2] + '</a>';
			break;
		case 'TEXTPIC': if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			var style = '';
			var post = '';
			var pre = '';
			align = tagparams[2];
			switch (align) {
				case 1: style = 'float:left'; break;
				case 2: style = 'float:right'; break;
				case 3: post = '<br />';
				case 4: pre = '<center>'; post = '</center>'; break;
			}
			return pre + "<img class='textpic' style='" + style + "' src='" + RESOURCES_DIR + tagparams[1] + "' />" + post;
			break;
		case 'HTML': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return tagparams[1];
			break;
		case 'FLAG': if (tagparams.length != 2) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			return getFlag(tagparams[1]);
			break;
		case 'OREF': if (tagparams.length != 1) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			if (objects[getFlag(FLAG_REFERRED_OBJECT)]) return getObjectFixArticles(getFlag(FLAG_REFERRED_OBJECT)); else return '';
			break;
		case 'TT':
		case 'TOOLTIP':
			if (tagparams.length != 3) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };
			var title = $('<span>' + tagparams[1] + '</span>').text().replace(/'/g, "&apos;").replace(/\n/g, "&#10;");
			var text = tagparams[2];
			return "<span title='" + title + "'>" + text + "</span>";
			break;
		case 'OPRO': if (tagparams.length != 1) { return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]' };  // returns the pronoun for a given object, used for english start database
			switch (getSimpleGender(getFlag(FLAG_REFERRED_OBJECT))) {
				case 'M': return "him";
				case "F": return "her";
				case "N": return "it";
				case "P": return "them";  // plural returns them
			}
			break;

		default: return '[[[' + STR_INVALID_TAG_SEQUENCE_BADTAG + ' : ' + tagparams[0] + ']]]';
	}
}

function processTags(text) {
	//Apply the {} tags filtering
	var pre, post, innerTag;
	tagfilter:
	while (text.indexOf('{') != -1) {
		if ((text.indexOf('}') == -1) || ((text.indexOf('}') < text.indexOf('{')))) {
			writeWarning(STR_INVALID_TAG_SEQUENCE + text);
			break tagfilter;
		}
		pre = text.substring(0, text.indexOf('{'));
		var openbracketcont = 1;
		pointer = text.indexOf('{') + 1;
		innerTag = ''
		while (openbracketcont > 0) {
			if (text.charAt(pointer) == '{') openbracketcont++;
			if (text.charAt(pointer) == '}') openbracketcont--;
			if (text.length <= pointer) {
				writeWarning(STR_INVALID_TAG_SEQUENCE + text);
				break tagfilter;
			}
			innerTag = innerTag + text.charAt(pointer);
			pointer++;
		}
		innerTag = innerTag.substring(0, innerTag.length - 1);
		post = text.substring(pointer);
		if (innerTag.indexOf('{') != -1) innerTag = processTags(innerTag);
		innerTag = implementTag(innerTag);
		text = pre + innerTag + post;
	}
	return text;
}

function filterText(text) {
	// ngPAWS sequences
	text = processTags(text);


	// Superglus sequences (only \n remains)
	text = text.replace(/\n/g, STR_NEWLINE);

	// PAWS sequences (only underscore)
	objno = getFlag(FLAG_REFERRED_OBJECT);
	if ((objno != EMPTY_OBJECT) && (objects[objno])) text = text.replace(/_/g, objects[objno].firstToLower()); else text = text.replace(/_/g, '');
	text = text.replace(/¬/g, ' ');

	return text;
}

// Text Output functions
let typewriterEffect = getFlag(65);
let isWriting = false;
let writeQueue = [];

function writeText(text, skipAutoComplete) {
	if (getFlag(67) == 1) {
		return new Promise(resolve => {
			writeQueue.push({
				text,
				skipAutoComplete,
				resolve
			});

			if (!isWriting) {
				processQueue();
			}
		});
	} else {
		if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
		text = h_writeText(text); // hook
		$('.text').append(text);
		$('.text').scrollTop($('.text')[0].scrollHeight);
		addToTranscript(text);
		if (!skipAutoComplete) addToAutoComplete(text);
		focusInput();
	}
}

function processQueue() {
	if (writeQueue.length > 0) {
		isWriting = true;
		$(".input").hide();
		const nextWrite = writeQueue.shift();
		typewriteText(nextWrite.text, nextWrite.skipAutoComplete)
			.then(() => {
				nextWrite.resolve();
				processQueue();
			});
	} else {
		isWriting = false;
		if (!inAnykey && !inPause){
			$(".input").show();
			document.getElementById('input').style.display='block';
			focusInput();
		}
	}
}

function typewriteText(text, skipAutoComplete) {
	return new Promise(resolve => {
		if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
		text = h_writeText(text); // hook

		const newTextSpan = document.createElement('span');
		newTextSpan.className = 'typewriter-text';
		$('.text').append(newTextSpan);

		let currentCharacterIndex = 0;
		let currentHtml = '';

		function typeNextCharacter() {
			if (currentCharacterIndex < text.length) {
				$('.text').scrollTop($('.text')[0].scrollHeight);
				currentHtml += text.charAt(currentCharacterIndex);

				// Update the innerHTML with the current HTML, which includes parsed tags
				newTextSpan.innerHTML = currentHtml;

				currentCharacterIndex++;
				setTimeout(typeNextCharacter, 1); // Adjust typing speed here
			} else {
				// Typing finished, remove the typewriter class
				newTextSpan.classList.remove('typewriter-text');
				addToTranscript(text);
				if (!skipAutoComplete) addToAutoComplete(text);
				resolve(); // Resolve the promise to signal completion
			}
		}

		// Start typing the new content
		typeNextCharacter();
	});
}

function writeWarning(text) {
	if (showWarnings) writeText(text)
}

function addToTranscript(text) {
	transcript = transcript + text;
}

function writelnText(text, skipAutoComplete) {
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	writeText(text + STR_NEWLINE, skipAutoComplete);
}

function writeMessage(mesno) {
	if (messages[mesno] != null) writeText(getMessageText(mesno)); else writeWarning(STR_NEWLINE + STR_WRONG_MESSAGE + ' [' + mesno + ']');
}

function writeSysMessage(sysno) {
	if (sysmessages[sysno] != null) writeText(getSysMessageText(sysno)); else writeWarning(STR_NEWLINE + STR_WRONG_SYSMESS + ' [' + sysno + ']');
	$(".text").scrollTop($(".text")[0].scrollHeight);
}

function writeWriteMessage(writeno) {
	writeText(getWriteMessageText(writeno));
}

function writeObject(objno) {
	writeText(getObjectText(objno));
}

function clearTextWindow() {
	$('.text').empty();
}


function clearInputWindow() {
	$('.prompt').val('');
}


function writeLocation(locno) {
	if (locations[locno] != null) writeText(getLocationText(locno) + STR_NEWLINE); else writeWarning(STR_NEWLINE + STR_WRONG_LOCATION + ' [' + locno + ']');
}

// Screen control functions

function clearGraphicsWindow() {
	$('.graphics').empty();
}


function clearScreen() {
	clearInputWindow();
	clearTextWindow();
	clearGraphicsWindow();
}

function copyOrderToTextWindow(player_order) {

	last_player_orders.push(player_order);
	last_player_orders_pointer = 0;
	clearInputWindow();
	writelnText(STR_PROMPT_START + player_order + STR_PROMPT_END, false);
}

function get_prev_player_order() {
	if (!last_player_orders.length) return '';
	var last = last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];
	if (last_player_orders_pointer < last_player_orders.length - 1) last_player_orders_pointer++;
	return last;
}

function get_next_player_order() {
	if (!last_player_orders.length) return '';
	if (last_player_orders_pointer > 0) last_player_orders_pointer--;
	return last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];

}



// Graphics functions


function hideGraphicsWindow() {
	$('.text').removeClass('half_text');
	$('.text').addClass('all_text');
	$('.graphics').removeClass('half_graphics');
	$('.graphics').addClass('hidden');
	if ($('.location_picture')) $('.location_picture').remove();
}



function drawPicture(picno) {
	var pictureDraw = false;
	if (graphicsON) {
		if ((isDarkHere()) && (!lightObjectsPresent())) picno = 0;
		var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
		if (filename) {
			$('.graphics').removeClass('hidden');
			$('.graphics').addClass('half_graphics');
			$('.text').removeClass('all_text');
			$('.text').addClass('half_text');
			$('.graphics').html('<img alt="" class="location_picture" src="' + filename + '" />');
			$('.location_picture').css('height', '100%');
			pictureDraw = true;
		}
	}

	if (!pictureDraw) hideGraphicsWindow();
}




function clearPictureAt() // deletes all pictures drawn by "pictureAT" condact
{
	$.each($('.graphics img'), function () {
		if ($(this)[0].className != 'location_picture') $(this).remove();
	});

}

// Turns functions

function incTurns() {
	turns = getFlag(FLAG_TURNS_LOW) + 256 * getFlag(FLAG_TURNS_HIGH) + 1;
	setFlag(FLAG_TURNS_LOW, turns % 256);
	setFlag(FLAG_TURNS_HIGH, Math.floor(turns / 256));
}

// input box functions

function disableInput() {
	$(".input").prop('disabled', true);
}

function enableInput() {
	$(".input").prop('disabled', false);
}

function focusInput() {
	$(".prompt").focus();
	timeout_progress = 0;
}

// Object default attributes functions

function objectIsNPC(objno) {
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_NPC);
}

function objectIsLight(objno) {
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_LIGHT);
}

function objectIsWearable(objno) {
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_WEARABLE);
}

function objectIsContainer(objno) {
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_CONTAINER);
}

function objectIsSupporter(objno) {
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_SUPPORTER);
}


function objectIsAttr(objno, attrno) {
	if (attrno > 63) return false;
	var attrs = getObjectLowAttributes(objno);
	if (attrno > 31) {
		attrs = getObjectHighAttributes(objno);
		attrno = attrno - 32;
	}
	return bittest(attrs, attrno);
}

function isAccesibleContainer(objno) {
	if (objectIsSupporter(objno)) return true;   // supporter
	if (objectIsContainer(objno) && !objectIsAttr(objno, ATTR_OPENABLE)) return true;  // No openable container
	if (objectIsContainer(objno) && objectIsAttr(objno, ATTR_OPENABLE) && objectIsAttr(objno, ATTR_OPEN)) return true;  // No openable & open container
	return false;
}

//Objects and NPC functions

function findMatchingObject(locno) {
	for (var i = 0; i < num_objects; i++)
		if ((locno == -1) || (getObjectLocation(i) == locno))
			if (((objectsNoun[i]) == getFlag(FLAG_NOUN1)) && (((objectsAdjective[i]) == EMPTY_WORD) || ((objectsAdjective[i]) == getFlag(FLAG_ADJECT1)))) return i;
	return EMPTY_OBJECT;
}

function getReferredObject() {
	var objectfound = EMPTY_OBJECT;
	refobject_search:
	{
		object_id = findMatchingObject(LOCATION_CARRIED);
		if (object_id != EMPTY_OBJECT) { objectfound = object_id; break refobject_search; }

		object_id = findMatchingObject(LOCATION_WORN);
		if (object_id != EMPTY_OBJECT) { objectfound = object_id; break refobject_search; }

		object_id = findMatchingObject(loc_here());
		if (object_id != EMPTY_OBJECT) { objectfound = object_id; break refobject_search; }

		object_id = findMatchingObject(-1);
		if (object_id != EMPTY_OBJECT) { objectfound = object_id; break refobject_search; }
	}
	return objectfound;
}


function getObjectLowAttributes(objno) {
	return objectsAttrLO[objno];
}

function getObjectHighAttributes(objno) {
	return objectsAttrHI[objno]
}


function setObjectLowAttributes(objno, attrs) {
	objectsAttrLO[objno] = attrs;
}

function setObjectHighAttributes(objno, attrs) {
	objectsAttrHI[objno] = attrs;
}


function getObjectLocation(objno) {
	if (objno > last_object_number)
		writeWarning(STR_INVALID_OBJECT + ' [' + objno + ']');
	return objectsLocation[objno];
}

function setObjectLocation(objno, locno) {
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) - 1);
	objectsLocation[objno] = locno;
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) + 1);
}



// Sets all flags associated to  referred object by current LS  
function setReferredObject(objno) {
	if (objno == EMPTY_OBJECT) {
		setFlag(FLAG_REFERRED_OBJECT, EMPTY_OBJECT);
		setFlag(FLAG_REFERRED_OBJECT_LOCATION, LOCATION_NONCREATED);
		setFlag(FLAG_REFERRED_OBJECT_WEIGHT, 0);
		setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, 0);
		setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, 0);
		return;
	}
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setFlag(FLAG_REFERRED_OBJECT_LOCATION, getObjectLocation(objno));
	setFlag(FLAG_REFERRED_OBJECT_WEIGHT, getObjectWeight(objno));
	setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, getObjectLowAttributes(objno));
	setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, getObjectHighAttributes(objno));

}


function getObjectWeight(objno) {
	var weight = objectsWeight[objno];
	if (((objectIsContainer(objno)) || (objectIsSupporter(objno))) && (weight != 0)) // Container with zero weigth are magic boxes, anything you put inside weigths zero
		weight = weight + getLocationObjectsWeight(objno);
	return weight;
}


function getLocationObjectsWeight(locno) {
	var weight = 0;
	for (var i = 0; i < num_objects; i++) {
		if (getObjectLocation(i) == locno) {
			objweight = objectsWeight[i];
			weight += objweight;
			if (objweight > 0) {
				if ((objectIsContainer(i)) || (objectIsSupporter(i))) {
					weight += getLocationObjectsWeight(i);
				}
			}
		}
	}
	return weight;
}

function getObjectCountAt(locno) {
	var count = 0;
	for (i = 0; i < num_objects; i++) {
		if (getObjectLocation(i) == locno) {
			attr = getObjectLowAttributes(i);
			if (!bittest(getFlag(FLAG_PARSER_SETTINGS), 3)) count++;  // Parser settings say we should include NPCs as objects
			else if (!objectIsNPC(i)) count++;     // or object is not an NPC
		}
	}
	return count;
}


function getObjectCountAtWithAttr(locno, attrnoArray) {
	var count = 0;
	for (var i = 0; i < num_objects; i++)
		if (getObjectLocation(i) == locno)
			for (var j = 0; j < attrnoArray.length; j++)
				if (objectIsAttr(i, attrnoArray[j])) count++;
	return count;
}


function getNPCCountAt(locno) {
	var count = 0;
	for (i = 0; i < num_objects; i++)
		if ((getObjectLocation(i) == locno) && (objectIsNPC(i))) count++;
	return count;
}


// Location light function

function lightObjectsAt(locno) {
	return getObjectCountAtWithAttr(locno, [ATTR_LIGHT]) > 0;
}


function lightObjectsPresent() {
	if (lightObjectsAt(LOCATION_CARRIED)) return true;
	if (lightObjectsAt(LOCATION_WORN)) return true;
	if (lightObjectsAt(loc_here())) return true;
	return false;
}


function isDarkHere() {
	return (getFlag(FLAG_LIGHT) != 0);
}

// Sound functions


function preloadsfx() {
	for (var i = 0; i < resources.length; i++)
		if (resources[i][0] == 'RESOURCE_TYPE_SND') {
			var fileparts = resources[i][2].split('.');
			var basename = fileparts[0];
			var mySound = new buzz.sound(basename, { formats: ["ogg", "mp3"], preload: true });
		}
}

function sfxplay(sfxno, channelno, times, method) {

	if (!soundsON) return;
	if ((channelno < 0) || (channelno > MAX_CHANNELS)) return;
	if (times == 0) times = -1; // more than 4000 million times
	var filename = getResourceById(RESOURCE_TYPE_SND, sfxno);
	if (filename) {
		var fileparts = filename.split('.');
		var basename = fileparts[0];
		var mySound = new buzz.sound(basename, { formats: ["ogg", "mp3"] });
		if (soundChannels[channelno]) soundChannels[channelno].stop();
		soundLoopCount[channelno] = times;
		mySound.bind("ended", function (e) {
			for (sndloop = 0; sndloop < MAX_CHANNELS; sndloop++)
				if (soundChannels[sndloop] == this) {
					if (soundLoopCount[sndloop] == -1) { this.play(); return }
					soundLoopCount[sndloop]--;
					if (soundLoopCount[sndloop] > 0) { this.play(); return }
					sfxstop(sndloop);
					return;
				}
		});
		soundChannels[channelno] = mySound;
		if (method == 'play') mySound.play(); else mySound.fadeIn(2000);
	}
}

function playLocationMusic(locno) {
	if (soundsON) {
		sfxstop(0);
		sfxplay(locno, 0, 0, 'play');
	}
}

function musicplay(musicno, times) {
	sfxplay(musicno, 0, times);
}

function channelActive(channelno) {
	if (soundChannels[channelno]) return true; else return false;
}


function sfxstopall() {
	for (channelno = 0; channelno < MAX_CHANNELS; channelno++) sfxstop(channelno);

}


function sfxstop(channelno) {
	if (soundChannels[channelno]) {
		soundChannels[channelno].unbind('ended');
		soundChannels[channelno].stop();
		soundChannels[channelno] = null;
	}
}

function sfxvolume(channelno, value) {
	if (soundChannels[channelno]) soundChannels[channelno].setVolume(Math.floor(value * 100 / 65535)); // Inherited volume condact uses a number among 0 and 65535, buzz library uses 0-100.
}

function isSFXPlaying(channelno) {
	if (!soundChannels[channelno]) return false;
	return true;
}


function sfxfadeout(channelno, value) {
	if (!soundChannels[channelno]) return;
	soundChannels[channelno].fadeOut(value, function () { sfxstop(channelno) });
}

// *** Process functions ***

function callProcess(procno) {
	if (inEND) return;
	current_process = procno;
	var prostr = procno.toString();
	while (prostr.length < 3) prostr = "0" + prostr;
	if (procno == 0) in_response = true;
	if (doall_flag && in_response) done_flag = false;
	if (!in_response) done_flag = false;
	h_preProcess(procno);
	eval("pro" + prostr + "()");
	h_postProcess(procno);
	if (procno == 0) in_response = false;
}

// Bitwise functions

function bittest(value, bitno) {
	mask = 1 << bitno;
	return ((value & mask) != 0);
}

function bitset(value, bitno) {

	mask = 1 << bitno;
	return value | mask;
}

function bitclear(value, bitno) {
	mask = 1 << bitno;
	return value & (~mask);
}


function bitneg(value, bitno) {
	mask = 1 << bitno;
	return value ^ mask;

}

// Savegame functions
function getSaveGameObject() {
	var savegame_object = new Object();
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	savegame_object.flags = flags.slice();
	savegame_object.objectsLocation = objectsLocation.slice();
	savegame_object.objectsWeight = objectsWeight.slice();
	savegame_object.objectsAttrLO = objectsAttrLO.slice();
	savegame_object.objectsAttrHI = objectsAttrHI.slice();
	savegame_object.connections = connections.slice();
	savegame_object.last_player_orders = last_player_orders.slice();
	savegame_object.last_player_orders_pointer = last_player_orders_pointer;
	savegame_object.transcript = transcript;
	savegame_object = h_saveGame(savegame_object);
	return savegame_object;
}

function restoreSaveGameObject(savegame_object) {
	flags = savegame_object.flags;
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	objectsLocation = savegame_object.objectsLocation.slice();
	objectsWeight = savegame_object.objectsWeight.slice();
	objectsAttrLO = savegame_object.objectsAttrLO.slice();
	objectsAttrHI = savegame_object.objectsAttrHI.slice();
	connections = savegame_object.connections.slice();
	last_player_orders = savegame_object.last_player_orders.slice();
	last_player_orders_pointer = savegame_object.last_player_orders_pointer;
	transcript = savegame_object.transcript;
	h_restoreGame(savegame_object);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        The parser                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadPronounSufixes() {

	var swapped;

	for (var j = 0; j < vocabulary.length; j++) if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_PRONOUN)
		pronoun_suffixes.push(vocabulary[j][VOCABULARY_WORD]);
	// Now sort them so the longest are first, so you rather replace SELOS in (COGESELOS=>COGE SELOS == >TAKE THEM) than LOS (COGESELOS==> COGESE LOS ==> TAKExx THEM) that woul not be understood (COGESE is not a verb, COGE is)
	do {
		swapped = false;
		for (var i = 0; i < pronoun_suffixes.length - 1; i++) {
			if (pronoun_suffixes[i].length < pronoun_suffixes[i + 1].length) {
				var temp = pronoun_suffixes[i];
				pronoun_suffixes[i] = pronoun_suffixes[i + 1];
				pronoun_suffixes[i + 1] = temp;
				swapped = true;
			}
		}
	} while (swapped);
}


function findVocabulary(word, forceDisableLevenshtein) {
	// Pending: in general this function is not very efficient. A solution where the vocabulary array is sorted by word so the first search can be binary search
	//          and possible typos are precalculated, so the distance is a lookup table instead of a function, would be much more efficient. On the other hand,
	//          the current solution is fast enough with a 1000+ words game that I don't consider improving this function to have high priority now.

	// Search word in vocabulary
	for (var j = 0; j < vocabulary.length; j++)
		if (vocabulary[j][VOCABULARY_WORD] == word)
			return vocabulary[j];

	if (forceDisableLevenshtein) return null;

	if (word.length <= 4) return null; // Don't try to fix typo for words with less than 5 length

	if (bittest(getFlag(FLAG_PARSER_SETTINGS), 8)) return null; // If matching is disabled, we won't try to use levhenstein distance

	// Search words in vocabulary with a Levenshtein distance of 1
	var distance2_match = null;
	for (var k = 0; k < vocabulary.length; k++) {
		if ([WORDTYPE_VERB, WORDTYPE_NOUN, WORDTYPE_ADJECT, WORDTYPE_ADVERB].indexOf(vocabulary[k][VOCABULARY_TYPE]) != -1) {
			var distance = getLevenshteinDistance(vocabulary[k][VOCABULARY_WORD], word);
			if ((!distance2_match) && (distance == 2)) distance2_match = vocabulary[k]; // Save first word with distance=2, in case we don't find any word with distance 1
			if (distance <= 1) return vocabulary[k];
		}
	}

	// If we found any word with distance 2, return it, only if word was at least 7 characters long
	if ((distance2_match) && (word.length > 6)) return distance2_match;

	// Word not found
	return null;
}

function normalize(player_order)
// Removes accented characters and makes sure every sentence separator (colon, semicolon, quotes, etc.) has one space before and after. Also, all separators are converted to comma
{
	var originalchars = 'áéíóúäëïöüâêîôûàèìòùÁÉÍÓÚÄËÏÖÜÂÊÎÔÛÀÈÌÒÙ';
	var i;
	var output = '';
	var pos;

	for (i = 0; i < player_order.length; i++) {
		pos = originalchars.indexOf(player_order.charAt(i));
		if (pos != -1) output = output + "aeiou".charAt(pos % 5); else {
			ch = player_order.charAt(i);
			if ((ch == '.') || (ch == ',') || (ch == ';') || (ch == '"') || (ch == '«') || (ch == '»')) output = output + ' , '; else output = output + player_order.charAt(i);
		}

	}
	return output;
}

function toParserBuffer(player_order)  // Converts a player order in a list of sentences separated by dot.
{
	player_order = normalize(player_order);
	player_order = player_order.toUpperCase();

	var words = player_order.split(' ');
	for (var q = 0; q < words.length; q++) {
		words[q] = words[q].trim();
		if (words[q] != ',') {
			words[q] = words[q].trim();
			foundWord = findVocabulary(words[q], false);
			if (foundWord) {
				if (foundWord[VOCABULARY_TYPE] == WORDTYPE_CONJUNCTION) {
					words[q] = ','; // Replace conjunctions with commas
				}
			}
		}
	}

	var output = '';
	for (q = 0; q < words.length; q++) {
		if (words[q] == ',') output = output + ','; else output = output + words[q] + ' ';
	}
	output = output.replace(/ ,/g, ',');
	output = output.trim();
	player_order_buffer = output;
}

function getSentencefromBuffer() {
	var sentences = player_order_buffer.split(',');
	var result = sentences[0];
	sentences.splice(0, 1);
	player_order_buffer = sentences.join();
	return result;
}

function processPronounSufixes(words) {
	// This procedure will split pronominal sufixes into separated words, so COGELA will become COGE LA at the end, and work exactly as TAKE IT does.
	// it's only for spanish so if lang is english then it makes no changes
	if (getLang() == 'EN') return words;
	var verbFound = false;
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS), 0)) return words;  // If pronoun sufixes inactive, just do nothing
	// First, we clear the word list from any match with pronouns, cause if we already have something that matches pronouns, probably is just concidence, like in COGE LA LLAVE
	var filtered_words = [];
	for (var q = 0; q < words.length; q++) {
		foundWord = findVocabulary(words[q], false);
		if (foundWord) {
			if (foundWord[VOCABULARY_TYPE] != WORDTYPE_PRONOUN) filtered_words[filtered_words.length] = words[q];
		}
		else filtered_words[filtered_words.length] = words[q];
	}
	words = filtered_words;

	// Now let's start trying to get sufixes
	new_words = [];
	for (var k = 0; k < words.length; k++) {
		words[k] = words[k].trim();
		foundWord = findVocabulary(words[k], true); // true to disable Levenshtein distance applied
		if (foundWord) if (foundWord[VOCABULARY_TYPE] == WORDTYPE_VERB) verbFound = true;  // If we found a verb, we don't look for pronoun sufixes, as they have to come together with verb
		suffixFound = false;
		pronunsufix_search:
		for (var l = 0; (l < pronoun_suffixes.length) && (!suffixFound) && (!verbFound); l++) {

			if (pronoun_suffixes[l] == words[k].rights(pronoun_suffixes[l].length)) {
				var verb_part = words[k].substring(0, words[k].length - pronoun_suffixes[l].length);
				var checkWord = findVocabulary(verb_part, false);
				if ((!checkWord) || (checkWord[VOCABULARY_TYPE] != WORDTYPE_VERB))  // If the part before the supposed-to-be pronoun sufix is not a verb, then is not a pronoun sufix
				{
					new_words.push(words[k]);
					continue pronunsufix_search;
				}
				new_words.push(verb_part);  // split the word in two parts: verb + pronoun. Since that very moment it works like in english (COGERLO ==> COGER LO as of TAKE IT)
				new_words.push(pronoun_suffixes[l]);
				suffixFound = true;
				verbFound = true;
			}
		}
		if (!suffixFound) new_words.push(words[k]);
	}
	return new_words;
}

function getLogicSentence() {
	parser_word_found = false;;
	aux_verb = -1;
	aux_noun1 = -1;
	aux_adject1 = -1;
	aux_adverb = -1;
	aux_pronoun = -1
	aux_pronoun_adject = -1
	aux_preposition = -1;
	aux_noun2 = -1;
	aux_adject2 = -1;
	initializeLSWords();
	SL_found = false;

	var order = getSentencefromBuffer();
	setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 1)); // Initialize flag that says an unknown word was found in the sentence


	words = order.split(" ");
	words = processPronounSufixes(words);
	wordsearch_loop:
	for (var i = 0; i < words.length; i++) {
		original_word = currentword = words[i];
		if (currentword.length > 10) currentword = currentword.substring(0, MAX_WORD_LENGHT);
		foundWord = findVocabulary(currentword, false);
		if (foundWord) {
			wordtype = foundWord[VOCABULARY_TYPE];
			word_id = foundWord[VOCABULARY_ID];

			switch (wordtype) {
				case WORDTYPE_VERB: if (aux_verb == -1) aux_verb = word_id;
					break;

				case WORDTYPE_NOUN: if (aux_noun1 == -1) aux_noun1 = word_id; else if (aux_noun2 == -1) aux_noun2 = word_id;
					break;

				case WORDTYPE_ADJECT: if (aux_adject1 == -1) aux_adject1 = word_id; else if (aux_adject2 == -1) aux_adject2 = word_id;
					break;

				case WORDTYPE_ADVERB: if (aux_adverb == -1) aux_adverb = word_id;
					break;

				case WORDTYPE_PRONOUN: if (aux_pronoun == -1) {
					aux_pronoun = word_id;
					if ((previous_noun != EMPTY_WORD) && (aux_noun1 == -1)) {
						aux_noun1 = previous_noun;
						if (previous_adject != EMPTY_WORD) aux_adject1 = previous_adject;
					}
				}

					break;

				case WORDTYPE_CONJUNCTION: break wordsearch_loop; // conjunction or nexus. Should not appear in this function, just added for security

				case WORDTYPE_PREPOSITION: if (aux_preposition == -1) aux_preposition = word_id;
					if (aux_noun1 != -1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 2));  // Set bit that determines that a preposition word was found after first noun
					break;
			}

			// Nouns that can be converted to verbs
			if ((aux_noun1 != -1) && (aux_verb == -1) && (aux_noun1 < NUM_CONVERTIBLE_NOUNS)) {
				aux_verb = aux_noun1;
				aux_noun1 = -1;
			}

			if ((aux_verb == -1) && (aux_noun1 != -1) && (previous_verb != EMPTY_WORD)) aux_verb = previous_verb;  // Support "TAKE SWORD AND SHIELD" --> "TAKE WORD AND TAKE SHIELD"

			if ((aux_verb != -1) || (aux_noun1 != -1) || (aux_adject1 != -1 || (aux_preposition != -1) || (aux_adverb != -1))) SL_found = true;



		} else if (aux_verb != -1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 1));  // Set bit that determines that an unknown word was found after the verb
	}

	if (SL_found) {
		if (aux_verb != -1) setFlag(FLAG_VERB, aux_verb);
		if (aux_noun1 != -1) setFlag(FLAG_NOUN1, aux_noun1);
		if (aux_adject1 != -1) setFlag(FLAG_ADJECT1, aux_adject1);
		if (aux_adverb != -1) setFlag(FLAG_ADVERB, aux_adverb);
		if (aux_pronoun != -1) {
			setFlag(FLAG_PRONOUN, aux_noun1);
			setFlag(FLAG_PRONOUN_ADJECT, aux_adject1);
		}
		else {
			setFlag(FLAG_PRONOUN, EMPTY_WORD);
			setFlag(FLAG_PRONOUN_ADJECT, EMPTY_WORD);
		}
		if (aux_preposition != -1) setFlag(FLAG_PREP, aux_preposition);
		if (aux_noun2 != -1) setFlag(FLAG_NOUN2, aux_noun2);
		if (aux_adject2 != -1) setFlag(FLAG_ADJECT2, aux_adject2);
		setReferredObject(getReferredObject());
		previous_verb = aux_verb;
		if ((aux_noun1 != -1) && (aux_noun1 >= NUM_PROPER_NOUNS)) {
			previous_noun = aux_noun1;
			if (aux_adject1 != -1) previous_adject = aux_adject1;
		}

	}
	if ((aux_verb + aux_noun1 + aux_adject1 + aux_adverb + aux_pronoun + aux_preposition + aux_noun2 + aux_adject2) != -8) parser_word_found = true;

	return SL_found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        Main functions and main loop                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Interrupt functions

function enableInterrupt() {
	interruptDisabled = false;
}

function disableInterrupt() {
	interruptDisabled = true;
}

function timer() {
	// Timeout control
	timeout_progress = timeout_progress + 1 / 32;  //timer happens every 40 milliseconds, but timeout counter should only increase every 1.28 seconds (according to PAWS documentation)
	timeout_length = getFlag(FLAG_TIMEOUT_LENGTH);
	if ((timeout_length) && (timeout_progress > timeout_length))  // time for timeout
	{
		timeout_progress = 0;
		if (($('.prompt').val() == '') || (($('.prompt').val() != '') && (!bittest(getFlag(FLAG_TIMEOUT_SETTINGS), 0))))  // but first check there is no text type, or is allowed to timeout when text typed already
		{
			setFlag(FLAG_TIMEOUT_SETTINGS, bitset(getFlag(FLAG_TIMEOUT_SETTINGS), 7)); // Clears timeout bit
			writeSysMessage(SYSMESS_TIMEOUT);
			callProcess(PROCESS_TURN);
		}
	}

	// PAUSE condact control
	if (inPause) {
		pauseRemainingTime = pauseRemainingTime - 40; // every tick = 40 milliseconds
		if (pauseRemainingTime <= 0) {
			inPause = false;
			hideAnykeyLayer();
			waitKeyCallback()
		}
	}

	// Interrupt process control
	if (!interruptDisabled)
		if (interruptProcessExists) {
			callProcess(interrupt_proc);
			setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
		}

}

// Initialize and finalize functions

function farewell() {
	writeSysMessage(SYSMESS_FAREWELL);
	ACCnewline();
}


function initializeConnections() {
	connections = [].concat(connections_start);
}

function initializeObjects() {
	for (i = 0; i < objects.length; i++) {
		objectsAttrLO = [].concat(objectsAttrLO_start);
		objectsAttrHI = [].concat(objectsAttrHI_start);
		objectsLocation = [].concat(objectsLocation_start);
		objectsWeight = [].concat(objectsWeight_start);
	}
}

function initializeLSWords() {
	setFlag(FLAG_PREP, EMPTY_WORD);
	setFlag(FLAG_NOUN2, EMPTY_WORD);
	setFlag(FLAG_ADJECT2, EMPTY_WORD);
	setFlag(FLAG_PRONOUN, EMPTY_WORD);
	setFlag(FLAG_ADJECT1, EMPTY_WORD);
	setFlag(FLAG_VERB, EMPTY_WORD);
	setFlag(FLAG_NOUN1, EMPTY_WORD);
	setFlag(FLAG_ADJECT1, EMPTY_WORD);
	setFlag(FLAG_ADVERB, EMPTY_WORD);
}


function initializeFlags() {
	flags = [];
	for (var i = 0; i < FLAG_COUNT; i++) flags.push(0);
	setFlag(FLAG_MAXOBJECTS_CARRIED, 4);
	setFlag(FLAG_PARSER_SETTINGS, 9); // Pronoun sufixes active, DOALL and others ignore NPCs, etc. 00001001
	setFlag(FLAG_MAXWEIGHT_CARRIED, 10);
	initializeLSWords();
	setFlag(FLAG_OBJECT_LIST_FORMAT, 64); // List objects in a single sentence (comma separated)
	setFlag(FLAG_OBJECTS_CARRIED_COUNT, carried_objects);  // FALTA: el compilador genera esta variable, hay que cambiarlo en el compilador, ERA numero_inicial_de_objetos_llevados
}

function initializeInternalVars() {
	num_objects = last_object_number + 1;
	transcript = '';
	timeout_progress = 0;
	previous_noun = EMPTY_WORD;
	previous_verb = EMPTY_WORD;
	previous_adject = EMPTY_WORD;
	player_order_buffer = '';
	last_player_orders = [];
	last_player_orders_pointer = 0;
	graphicsON = true;
	soundsON = true;
	interruptDisabled = false;
	unblock_process = null;
	done_flag = false;
	describe_location_flag = false;
	in_response = false;
	success = false;
	doall_flag = false;
	entry_for_doall = '';
}

function initializeSound() {
	sfxstopall();
}




function initialize() {
	preloadsfx();
	initializeInternalVars();
	initializeSound();
	initializeFlags();
	initializeObjects();
	initializeConnections();
}



// Main loops

function descriptionLoop() {
	do {
		describe_location_flag = false;
		if (!getFlag(FLAG_MODE)) clearTextWindow();
		if ((isDarkHere()) && (!lightObjectsPresent())) writeSysMessage(SYSMESS_ISDARK); else writeLocation(loc_here());
		h_description_init();
		playLocationMusic(loc_here());
		if (loc_here()) drawPicture(loc_here()); else hideGraphicsWindow(); // Don't show picture at location 0
		ACCminus(FLAG_AUTODEC2, 1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC3, 1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here()) == 0)) ACCminus(FLAG_AUTODEC4, 1);
		callProcess(PROCESS_DESCRIPTION);
		h_description_post();
		if (describe_location_flag) continue; // descriptionLoop() again without nesting
		describe_location_flag = false;
		callProcess(PROCESS_TURN);
		if (describe_location_flag) continue;
		describe_location_flag = false;
		focusInput();
		break; // Dirty trick to make this happen just one, but many times if descriptioLoop() should be repeated
	} while (true);

}

function orderEnteredLoop(player_order) {
	previous_verb = EMPTY_WORD;
	setFlag(FLAG_TIMEOUT_SETTINGS, bitclear(getFlag(FLAG_TIMEOUT_SETTINGS), 7)); // Clears timeout bit
	if (player_order == '') { writeSysMessage(SYSMESS_SORRY); ACCnewline(); return; };
	player_order = h_playerOrder(player_order); //hook
	copyOrderToTextWindow(player_order);
	toParserBuffer(player_order);
	do {
		describe_location_flag = false;
		ACCminus(FLAG_AUTODEC5, 1);
		ACCminus(FLAG_AUTODEC6, 1);
		ACCminus(FLAG_AUTODEC7, 1);
		ACCminus(FLAG_AUTODEC8, 1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC9, 1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here()) == 0)) ACCminus(FLAG_AUTODEC10, 1);

		if (describe_location_flag) {
			descriptionLoop();
			return;
		};

		if (getLogicSentence()) {
			incTurns();
			done_flag = false;
			callProcess(PROCESS_RESPONSE); // Response table
			if (describe_location_flag) {
				descriptionLoop();
				return;
			};
			if (!done_flag) {
				if ((getFlag(FLAG_VERB) < NUM_CONNECTION_VERBS) && (CNDmove(FLAG_LOCATION))) {
					descriptionLoop();
					return;
				} else if (getFlag(FLAG_VERB) < NUM_CONNECTION_VERBS) { writeSysMessage(SYSMESS_WRONGDIRECTION); ACCnewline(); } else { writeSysMessage(SYSMESS_CANTDOTHAT); ACCnewline(); };

			}
		} else {
			h_invalidOrder(player_order);
			if (parser_word_found) { writeSysMessage(SYSMESS_IDONTUNDERSTAND); ACCnewline() }
			else { writeSysMessage(SYSMESS_NONSENSE_SENTENCE); ACCnewline() };
		}
		callProcess(PROCESS_TURN);
	} while (player_order_buffer != '');
	previous_verb = ''; // Can't use previous verb if a new order is typed (we keep previous noun though, it can be used)
	focusInput();
}


function restart() {
	location.reload();
}


function hideBlock() {
	clearInputWindow();
	$('.block_layer').hide('slow');
	enableInterrupt();
	$('.input').show();
	focusInput();
}

function hideAnykeyLayer() {
	$('.anykey_layer').hide();
	$('.input').show();
	focusInput();
}

function showAnykeyLayer() {
	$('.anykey_layer').show();
	$('.input').hide();
}

//called when the block layer is closed
function closeBlock() {
	if (!inBlock) return;
	inBlock = false;
	hideBlock();
	var proToCall = unblock_process;
	unblock_process = null;
	callProcess(proToCall);
	if (describe_location_flag) descriptionLoop();
}

function setInputPlaceHolder() {
	var prompt_msg = getFlag(FLAG_PROMPT);
	if (!prompt_msg) {
		var random = Math.floor((Math.random() * 100));
		if (random < 30) prompt_msg = SYSMESS_PROMPT0; else
			if ((random >= 30) && (random < 60)) prompt_msg = SYSMESS_PROMPT1; else
				if ((random >= 60) && (random < 90)) prompt_msg = SYSMESS_PROMPT2; else
					if (random >= 90) prompt_msg = SYSMESS_PROMPT3;
	}
	$('.prompt').attr('placeholder', getSysMessageText(prompt_msg));
}


function divTextScrollUp() {
	var currentPos = $('.text').scrollTop();
	if (currentPos >= DIV_TEXT_SCROLL_STEP) $('.text').scrollTop(currentPos - DIV_TEXT_SCROLL_STEP);
}

function divTextScrollDown() {
	var currentPos = $('.text').scrollTop();
	if (currentPos <= ($('.text')[0].scrollHeight - DIV_TEXT_SCROLL_STEP)) $('.text').scrollTop(currentPos + DIV_TEXT_SCROLL_STEP);
}

// Autocomplete functions

function predictiveText(currentText) {
	if (currentText == '') return currentText;
	var wordToComplete;
	var words = currentText.split(' ');
	if (autocompleteStep != 0) wordToComplete = autocompleteBaseWord; else wordToComplete = words[words.length - 1];
	words[words.length - 1] = completedWord(wordToComplete);
	return words.join(' ');
}


function initAutoComplete() {
	for (var j = 0; j < vocabulary.length; j++)
		if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_VERB)
			if (vocabulary[j][VOCABULARY_WORD].length >= 3)
				autocomplete.push(vocabulary[j][VOCABULARY_WORD].toLowerCase());
}

function addToAutoComplete(sentence) {
	var words = sentence.split(' ');
	for (var i = 0; i < words.length; i++) {
		var finalWord = '';
		for (var j = 0; j < words[i].length; j++) {
			var c = words[i][j].toLowerCase();
			if ("abcdefghijklmnopqrstuvwxyzáéíóúàèìòùçäëïÖüâêîôû".indexOf(c) != -1) finalWord = finalWord + c;
			else break;
		}

		if (finalWord.length >= 3) {
			var index = autocomplete.indexOf(finalWord);
			if (index != -1) autocomplete.splice(index, 1);
			autocomplete.push(finalWord);
		}
	}
}

function completedWord(word) {
	if (word == '') return '';
	autocompleteBaseWord = word;
	var foundCount = 0;
	for (var i = autocomplete.length - 1; i >= 0; i--) {
		if (autocomplete[i].length > word.length)
			if (autocomplete[i].indexOf(word) == 0) {
				foundCount++;
				if (foundCount > autocompleteStep) {
					autocompleteStep++;
					return autocomplete[i];
				}
			}
	}
	return word;
}


// Exacution starts here, called by the html file on document.ready()
function start() {
	h_init(); //hook
	$('.graphics').addClass('half_graphics');
	$('.text').addClass('half_text');
	if (isBadIE()) alert(STR_BADIE)
	loadPronounSufixes();
	setInputPlaceHolder();
	initAutoComplete();

	// Assign keypress action for input box (detect enter key press)
	$('.prompt').keypress(function (e) {
		if (e.which == 13) {
			setInputPlaceHolder();
			player_order = $('.prompt').val();
			if (player_order.charAt(0) == '#') {
				addToTranscript(player_order + STR_NEWLINE);
				clearInputWindow();
			}
			else
				if (player_order != '')
					orderEnteredLoop(player_order);
		}
	});

	// Assign arrow up key press to recover last order
	$('.prompt').keyup(function (e) {
		if (e.which == 38) $('.prompt').val(get_prev_player_order());
		if (e.which == 40) $('.prompt').val(get_next_player_order());
	});


	// Assign tab keydown to complete word
	$('.prompt').keydown(function (e) {
		if (e.which == 9) {
			$('.prompt').val(predictiveText($('.prompt').val()));
			e.preventDefault();
		} else {
			autocompleteStep = 0;
			autocompleteBaseWord = ''; // Any keypress other than tab resets the autocomplete feature
		}
	});

	//Detect resize to change flag 12
	$(window).resize(function () {
		setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
		clearPictureAt();
		return;
	});


	// assign any click on block layer --> close it
	$(document).click(function (e) {
		if (inBlock) {
			closeBlock();
			e.preventDefault();
			return;
		}

		if (inAnykey && !isWriting)  // return for ANYKEY, accepts mouse click
		{
			inAnykey = false;
			hideAnykeyLayer();
			waitKeyCallback();
			e.preventDefault();
			return;
		}

	});

	//Make tap act as click
	document.addEventListener('touchstart', function (e) { $(document).click(); }, false);


	$(document).keydown(function (e) {

		if (!h_keydown(e)) return; // hook

		// if waiting for END response
		if (inEND) {
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N';
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			if (endYESresponseCode == e.keyCode) location.reload();
			if (endNOresponseCode == e.keyCode) {
				inEND = false;
				sfxstopall();
				$('body').hide('slow');
			}
			return;
		}


		// if waiting for QUIT response
		if (inQUIT) {
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N';
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			if (endNOresponseCode == e.keyCode) {
				inQUIT = false;
				waitkey_callback_function.pop();
				hideAnykeyLayer();
				e.preventDefault();
			}

			if (endYESresponseCode == e.keyCode) {
				inQUIT = false;
				e.preventDefault();
				waitKeyCallback();
				return;
			}
		}


		if (inGetkey)  // return for getkey
		{
			setFlag(getkey_return_flag, e.keyCode);
			getkey_return_flag = null;
			inGetkey = false;
			hideAnykeyLayer();
			e.preventDefault();
			waitKeyCallback();
			return;
		}


		if (inAnykey && !isWriting)  // return for anykey
		{
			inAnykey = false;
			hideAnykeyLayer();
			e.preventDefault();
			waitKeyCallback();
			return;
		}

		// if keypress and block displayed, close it
		if (inBlock) {
			closeBlock();
			e.preventDefault();
			return;
		}


		// if ESC pressed and transcript layer visible, close it
		if ((inTranscript) && (e.keyCode == 27)) {
			$('.transcript_layer').hide();
			inTranscript = false;
			e.preventDefault();
			return;
		}

		// Scroll text window using PgUp/PgDown
		if (e.keyCode == 33)  // PgUp
		{
			divTextScrollUp();
			e.preventDefault();
			return;
		}
		if (e.keyCode == 34)  // PgDown
		{
			divTextScrollDown();
			return;
		}

		// focus the input if the user is likely to expect it
		// (but not if they're e.g. ctrl+c'ing some text)
		switch (e.keyCode) {
			case 8: // backspace
			case 9: // tab
			case 13: // enter
				break;
			default:
				if (!e.ctrlKey && !e.altKey) focusInput();
		}

	});


	$(document).bind('mousewheel', function (e) {
		if (e.originalEvent.wheelDelta / 120 > 0) divTextScrollUp(); else divTextScrollDown();
	});


	initialize();
	descriptionLoop();
	focusInput();

	h_post();  //hook

	// Start interrupt process
	setInterval(timer, TIMER_MILLISECONDS);

}

$('document').ready(
	function () {
		start();
	}
);

// VOCABULARY

vocabulary = [];
vocabulary.push([2, "A", 6]);
vocabulary.push([11, "ABAJO", 1]);
vocabulary.push([105, "ABRAZA", 0]);
vocabulary.push([105, "ABRAZAR", 0]);
vocabulary.push([64, "ABRE", 0]);
vocabulary.push([64, "ABRIR", 0]);
vocabulary.push([40, "ACARICIA", 0]);
vocabulary.push([40, "ACARICIAR", 0]);
vocabulary.push([99, "ACERCA", 0]);
vocabulary.push([99, "ACERCAR", 0]);
vocabulary.push([94, "ACORDARSE", 0]);
vocabulary.push([94, "ACORDARTE", 0]);
vocabulary.push([103, "ACTIVA", 0]);
vocabulary.push([103, "ACTIVAR", 0]);
vocabulary.push([94, "ACUERDATE", 0]);
vocabulary.push([106, "ADQUIERE", 0]);
vocabulary.push([106, "ADQUIERESE", 0]);
vocabulary.push([106, "ADQUIRIR", 0]);
vocabulary.push([49, "AGITA", 0]);
vocabulary.push([49, "AGITAR", 0]);
vocabulary.push([46, "AGREDE", 0]);
vocabulary.push([46, "AGREDIR", 0]);
vocabulary.push([2, "AL", 6]);
vocabulary.push([90, "ANDA", 0]);
vocabulary.push([90, "ANDAR", 0]);
vocabulary.push([54, "ANUDA", 0]);
vocabulary.push([54, "ANUDAR", 0]);
vocabulary.push([67, "APAGA", 0]);
vocabulary.push([67, "APAGAR", 0]);
vocabulary.push([111, "APRETAR", 0]);
vocabulary.push([111, "APRIETA", 0]);
vocabulary.push([99, "APROXIMA", 0]);
vocabulary.push([99, "APROXIMAR", 0]);
vocabulary.push([79, "ARRANCA", 0]);
vocabulary.push([79, "ARRANCAR", 0]);
vocabulary.push([83, "ARRASTRA", 0]);
vocabulary.push([83, "ARRASTRAR", 0]);
vocabulary.push([10, "ARRIBA", 1]);
vocabulary.push([99, "ARRIMA", 0]);
vocabulary.push([99, "ARRIMAR", 0]);
vocabulary.push([32, "ARROJA", 0]);
vocabulary.push([32, "ARROJAR", 0]);
vocabulary.push([32, "ARROJARSE", 0]);
vocabulary.push([32, "ARROJASE", 0]);
vocabulary.push([10, "ASCENDER", 0]);
vocabulary.push([10, "ASCIENDE", 0]);
vocabulary.push([46, "ASESINA", 0]);
vocabulary.push([46, "ASESINAR", 0]);
vocabulary.push([54, "ATA", 0]);
vocabulary.push([46, "ATACA", 0]);
vocabulary.push([46, "ATACAR", 0]);
vocabulary.push([54, "ATAR", 0]);
vocabulary.push([34, "AYUDA", 0]);
vocabulary.push([112, "BAILA", 0]);
vocabulary.push([112, "BAILAR", 0]);
vocabulary.push([11, "BAJA", 0]);
vocabulary.push([11, "BAJAR", 0]);
vocabulary.push([11, "BAJARSE", 0]);
vocabulary.push([11, "BAJARTE", 0]);
vocabulary.push([11, "BAJATE", 0]);
vocabulary.push([8, "BAJO", 6]);
vocabulary.push([50, "BALANCEA", 0]);
vocabulary.push([50, "BALANCEAR", 0]);
vocabulary.push([50, "BALANCEARS", 0]);
vocabulary.push([50, "BALANCEART", 0]);
vocabulary.push([50, "BALANCEATE", 0]);
vocabulary.push([39, "BEBE", 0]);
vocabulary.push([39, "BEBER", 0]);
vocabulary.push([39, "BEBERSE", 0]);
vocabulary.push([39, "BEBERTE", 0]);
vocabulary.push([39, "BEBETE", 0]);
vocabulary.push([62, "BESA", 0]);
vocabulary.push([62, "BESAR", 0]);
vocabulary.push([45, "BRINCA", 0]);
vocabulary.push([45, "BRINCAR", 0]);
vocabulary.push([45, "BRINCARSE", 0]);
vocabulary.push([45, "BRINCARTE", 0]);
vocabulary.push([45, "BRINCATE", 0]);
vocabulary.push([47, "CAGA", 0]);
vocabulary.push([47, "CAGAR", 0]);
vocabulary.push([47, "CAGARSE", 0]);
vocabulary.push([47, "CAGARTE", 0]);
vocabulary.push([47, "CAGATE", 0]);
vocabulary.push([90, "CAMINA", 0]);
vocabulary.push([90, "CAMINAR", 0]);
vocabulary.push([44, "CANTA", 0]);
vocabulary.push([44, "CANTAR", 0]);
vocabulary.push([27, "CARGA", 0]);
vocabulary.push([27, "CARGAR", 0]);
vocabulary.push([29, "CARGARAM", 0]);
vocabulary.push([51, "CAVA", 0]);
vocabulary.push([51, "CAVAR", 0]);
vocabulary.push([65, "CERRAR", 0]);
vocabulary.push([77, "CHILLA", 0]);
vocabulary.push([77, "CHILLAR", 0]);
vocabulary.push([63, "CHUPA", 0]);
vocabulary.push([63, "CHUPAR", 0]);
vocabulary.push([65, "CIERRA", 0]);
vocabulary.push([65, "CLOSE", 0]);
vocabulary.push([20, "COGE", 0]);
vocabulary.push([20, "COGER", 0]);
vocabulary.push([50, "COLUMPIARS", 0]);
vocabulary.push([50, "COLUMPIART", 0]);
vocabulary.push([50, "COLUMPIATE", 0]);
vocabulary.push([38, "COME", 0]);
vocabulary.push([38, "COMER", 0]);
vocabulary.push([38, "COMERSE", 0]);
vocabulary.push([38, "COMERTE", 0]);
vocabulary.push([38, "COMETE", 0]);
vocabulary.push([106, "COMPRA", 0]);
vocabulary.push([106, "COMPRAR", 0]);
vocabulary.push([106, "COMPRARSE", 0]);
vocabulary.push([106, "COMPRARTE", 0]);
vocabulary.push([106, "COMPRASE", 0]);
vocabulary.push([106, "COMPRATE", 0]);
vocabulary.push([9, "CON", 6]);
vocabulary.push([103, "CONECTA", 0]);
vocabulary.push([103, "CONECTAR", 0]);
vocabulary.push([107, "CONSULTA", 0]);
vocabulary.push([107, "CONSULTAR", 0]);
vocabulary.push([90, "CORRE", 0]);
vocabulary.push([90, "CORRER", 0]);
vocabulary.push([52, "CORTA", 0]);
vocabulary.push([52, "CORTAR", 0]);
vocabulary.push([51, "COSA", 1]);
vocabulary.push([17, "CREDITOS", 0]);
vocabulary.push([6, "CUIDADOSAM", 3]);
vocabulary.push([11, "D", 1]);
vocabulary.push([73, "DA", 0]);
vocabulary.push([73, "DALE", 0]);
vocabulary.push([112, "DANZA", 0]);
vocabulary.push([112, "DANZAR", 0]);
vocabulary.push([73, "DAR", 0]);
vocabulary.push([73, "DARLE", 0]);
vocabulary.push([73, "DARSE", 0]);
vocabulary.push([73, "DARTE", 0]);
vocabulary.push([73, "DASE", 0]);
vocabulary.push([3, "DE", 6]);
vocabulary.push([8, "DEBAJO", 6]);
vocabulary.push([31, "DECIR", 0]);
vocabulary.push([47, "DEFECA", 0]);
vocabulary.push([47, "DEFECAR", 0]);
vocabulary.push([47, "DEFECARSE", 0]);
vocabulary.push([47, "DEFECARTE", 0]);
vocabulary.push([47, "DEFECATE", 0]);
vocabulary.push([21, "DEJA", 0]);
vocabulary.push([21, "DEJAR", 0]);
vocabulary.push([3, "DEL", 6]);
vocabulary.push([14, "DELANTE", 6]);
vocabulary.push([4, "DENTRO", 6]);
vocabulary.push([104, "DESACTIVA", 0]);
vocabulary.push([104, "DESACTIVAR", 0]);
vocabulary.push([98, "DESATA", 0]);
vocabulary.push([98, "DESATAR", 0]);
vocabulary.push([61, "DESCANSA", 0]);
vocabulary.push([61, "DESCANSAR", 0]);
vocabulary.push([11, "DESCENDER", 0]);
vocabulary.push([11, "DESCIENDE", 0]);
vocabulary.push([104, "DESCONECTA", 0]);
vocabulary.push([5, "DESDE", 6]);
vocabulary.push([97, "DESDOBLA", 0]);
vocabulary.push([97, "DESDOBLAR", 0]);
vocabulary.push([51, "DESENTERRA", 0]);
vocabulary.push([51, "DESENTIERR", 0]);
vocabulary.push([87, "DESPERTAR", 0]);
vocabulary.push([87, "DESPERTARS", 0]);
vocabulary.push([87, "DESPERTART", 0]);
vocabulary.push([87, "DESPIERTA", 0]);
vocabulary.push([87, "DESPIERTAT", 0]);
vocabulary.push([110, "DESPLAZA", 0]);
vocabulary.push([110, "DESPLAZAR", 0]);
vocabulary.push([69, "DESTRUIR", 0]);
vocabulary.push([69, "DESTRUYE", 0]);
vocabulary.push([22, "DESVESTIR", 0]);
vocabulary.push([22, "DESVESTIRS", 0]);
vocabulary.push([22, "DESVESTIRT", 0]);
vocabulary.push([22, "DESVISTE", 0]);
vocabulary.push([7, "DETRAS", 6]);
vocabulary.push([31, "DI", 0]);
vocabulary.push([90, "DIRIGETE", 0]);
vocabulary.push([90, "DIRIGIRSE", 0]);
vocabulary.push([90, "DIRIGIRTE", 0]);
vocabulary.push([96, "DOBLA", 0]);
vocabulary.push([96, "DOBLAR", 0]);
vocabulary.push([61, "DORMIR", 0]);
vocabulary.push([61, "DORMIRSE", 0]);
vocabulary.push([61, "DORMIRTE", 0]);
vocabulary.push([11, "DOWN", 1]);
vocabulary.push([21, "DROP", 0]);
vocabulary.push([61, "DUERME", 0]);
vocabulary.push([6, "DURA", 2]);
vocabulary.push([6, "DURO", 2]);
vocabulary.push([4, "E", 1]);
vocabulary.push([74, "ECHA", 0]);
vocabulary.push([74, "ECHAR", 0]);
vocabulary.push([61, "ECHARSE", 0]);
vocabulary.push([61, "ECHARTE", 0]);
vocabulary.push([61, "ECHATE", 0]);
vocabulary.push([33, "EMPUJA", 0]);
vocabulary.push([33, "EMPUJAR", 0]);
vocabulary.push([4, "EN", 6]);
vocabulary.push([66, "ENCENDER", 0]);
vocabulary.push([66, "ENCIENDE", 0]);
vocabulary.push([16, "ENCIMA", 6]);
vocabulary.push([54, "ENLAZA", 0]);
vocabulary.push([54, "ENLAZAR", 0]);
vocabulary.push([36, "ENSEÑA", 0]);
vocabulary.push([36, "ENSEÑAR", 0]);
vocabulary.push([36, "ENSEÑARSE", 0]);
vocabulary.push([36, "ENSEÑASE", 0]);
vocabulary.push([12, "ENTER", 0]);
vocabulary.push([51, "ENTERRAR", 0]);
vocabulary.push([51, "ENTIERRA", 0]);
vocabulary.push([2, "ENTONCES", 5]);
vocabulary.push([12, "ENTRA", 0]);
vocabulary.push([12, "ENTRAR", 0]);
vocabulary.push([15, "ENTRE", 6]);
vocabulary.push([47, "ERUCTA", 0]);
vocabulary.push([47, "ERUCTAR", 0]);
vocabulary.push([47, "ERUCTARSE", 0]);
vocabulary.push([47, "ERUCTARTE", 0]);
vocabulary.push([47, "ERUCTATE", 0]);
vocabulary.push([57, "ESCALA", 0]);
vocabulary.push([57, "ESCALAR", 0]);
vocabulary.push([91, "ESCONDE", 0]);
vocabulary.push([91, "ESCONDER", 0]);
vocabulary.push([92, "ESCONDERSE", 0]);
vocabulary.push([92, "ESCONDERTE", 0]);
vocabulary.push([92, "ESCONDETE", 0]);
vocabulary.push([37, "ESCUCHA", 0]);
vocabulary.push([37, "ESCUCHAR", 0]);
vocabulary.push([48, "ESCUPE", 0]);
vocabulary.push([48, "ESCUPIR", 0]);
vocabulary.push([87, "ESPABILAR", 0]);
vocabulary.push([87, "ESPABILARS", 0]);
vocabulary.push([87, "ESPABILART", 0]);
vocabulary.push([87, "ESPABILATE", 0]);
vocabulary.push([43, "ESPERA", 0]);
vocabulary.push([43, "ESPERAR", 0]);
vocabulary.push([4, "ESTE", 1]);
vocabulary.push([79, "ESTIRA", 0]);
vocabulary.push([79, "ESTIRAR", 0]);
vocabulary.push([46, "ESTRANGULA", 0]);
vocabulary.push([111, "ESTRUJA", 0]);
vocabulary.push([111, "ESTRUJAR", 0]);
vocabulary.push([30, "EX", 0]);
vocabulary.push([30, "EXAMINA", 0]);
vocabulary.push([30, "EXAMINAR", 0]);
vocabulary.push([85, "EXAMINARSE", 0]);
vocabulary.push([85, "EXAMINARTE", 0]);
vocabulary.push([85, "EXAMINATE", 0]);
vocabulary.push([51, "EXCAVA", 0]);
vocabulary.push([51, "EXCAVAR", 0]);
vocabulary.push([12, "EXCEPTO", 6]);
vocabulary.push([13, "EXIT", 0]);
vocabulary.push([41, "EXITS", 0]);
vocabulary.push([67, "EXTINGUE", 0]);
vocabulary.push([67, "EXTINGUIR", 0]);
vocabulary.push([75, "EXTRAE", 0]);
vocabulary.push([75, "EXTRAER", 0]);
vocabulary.push([25, "FIN", 0]);
vocabulary.push([101, "FREGAR", 0]);
vocabulary.push([101, "FRIEGA", 0]);
vocabulary.push([101, "FROTA", 0]);
vocabulary.push([101, "FROTAR", 0]);
vocabulary.push([13, "FUERA", 6]);
vocabulary.push([35, "GIRA", 0]);
vocabulary.push([35, "GIRAR", 0]);
vocabulary.push([72, "GOLPEA", 0]);
vocabulary.push([72, "GOLPEAR", 0]);
vocabulary.push([26, "GRABA", 0]);
vocabulary.push([26, "GRABAR", 0]);
vocabulary.push([28, "GRABARAM", 0]);
vocabulary.push([3, "GRANDE", 2]);
vocabulary.push([77, "GRITA", 0]);
vocabulary.push([77, "GRITAR", 0]);
vocabulary.push([26, "GUARDA", 0]);
vocabulary.push([26, "GUARDAR", 0]);
vocabulary.push([31, "HABLA", 0]);
vocabulary.push([31, "HABLAR", 0]);
vocabulary.push([6, "HACIA", 6]);
vocabulary.push([34, "HELP", 0]);
vocabulary.push([42, "HUELE", 0]);
vocabulary.push([90, "HUIR", 0]);
vocabulary.push([42, "HUSMEA", 0]);
vocabulary.push([42, "HUSMEAR", 0]);
vocabulary.push([90, "HUYE", 0]);
vocabulary.push([14, "I", 1]);
vocabulary.push([66, "INCENDIA", 0]);
vocabulary.push([66, "INCENDIAR", 0]);
vocabulary.push([89, "INCREPA", 0]);
vocabulary.push([89, "INCREPAR", 0]);
vocabulary.push([38, "INGERIR", 0]);
vocabulary.push([38, "INGIERE", 0]);
vocabulary.push([74, "INSERTA", 0]);
vocabulary.push([74, "INSERTAR", 0]);
vocabulary.push([30, "INSPECCION", 0]);
vocabulary.push([89, "INSULTA", 0]);
vocabulary.push([89, "INSULTAR", 0]);
vocabulary.push([12, "INTERNARTE", 0]);
vocabulary.push([12, "INTERNATE", 0]);
vocabulary.push([74, "INTRODUCE", 0]);
vocabulary.push([12, "INTRODUCET", 0]);
vocabulary.push([74, "INTRODUCIR", 0]);
vocabulary.push([14, "INVEN", 1]);
vocabulary.push([14, "INVENTARIO", 1]);
vocabulary.push([90, "IR", 0]);
vocabulary.push([90, "IRSE", 0]);
vocabulary.push([90, "IRTE", 0]);
vocabulary.push([99, "JUNTA", 0]);
vocabulary.push([99, "JUNTAR", 0]);
vocabulary.push([24, "L", 0]);
vocabulary.push([2, "LA", 4]);
vocabulary.push([63, "LAME", 0]);
vocabulary.push([63, "LAMER", 0]);
vocabulary.push([32, "LANZA", 0]);
vocabulary.push([32, "LANZAR", 0]);
vocabulary.push([32, "LANZARSE", 0]);
vocabulary.push([32, "LANZASE", 0]);
vocabulary.push([9, "LARGA", 2]);
vocabulary.push([9, "LARGO", 2]);
vocabulary.push([2, "LAS", 4]);
vocabulary.push([101, "LAVA", 0]);
vocabulary.push([101, "LAVAR", 0]);
vocabulary.push([88, "LAVARSE", 0]);
vocabulary.push([88, "LAVARTE", 0]);
vocabulary.push([88, "LAVATE", 0]);
vocabulary.push([2, "LE", 4]);
vocabulary.push([109, "LEE", 0]);
vocabulary.push([109, "LEER", 0]);
vocabulary.push([109, "LEERSE", 0]);
vocabulary.push([109, "LEERTE", 0]);
vocabulary.push([109, "LEETE", 0]);
vocabulary.push([3, "LENTAMENTE", 3]);
vocabulary.push([2, "LES", 4]);
vocabulary.push([114, "LEVANTA", 0]);
vocabulary.push([114, "LEVANTAR", 0]);
vocabulary.push([53, "LEVANTARSE", 0]);
vocabulary.push([53, "LEVANTARTE", 0]);
vocabulary.push([53, "LEVANTATE", 0]);
vocabulary.push([101, "LIMPIA", 0]);
vocabulary.push([101, "LIMPIAR", 0]);
vocabulary.push([88, "LIMPIARSE", 0]);
vocabulary.push([88, "LIMPIARTE", 0]);
vocabulary.push([88, "LIMPIATE", 0]);
vocabulary.push([76, "LLAMA", 0]);
vocabulary.push([76, "LLAMAR", 0]);
vocabulary.push([55, "LLENA", 0]);
vocabulary.push([55, "LLENAR", 0]);
vocabulary.push([93, "LLORA", 0]);
vocabulary.push([93, "LLORAR", 0]);
vocabulary.push([93, "LLORIQUEA", 0]);
vocabulary.push([93, "LLORIQUEAR", 0]);
vocabulary.push([2, "LO", 4]);
vocabulary.push([27, "LOAD", 0]);
vocabulary.push([24, "LOOK", 0]);
vocabulary.push([2, "LOS", 4]);
vocabulary.push([46, "LUCHAR", 0]);
vocabulary.push([24, "M", 0]);
vocabulary.push([38, "MASTICA", 0]);
vocabulary.push([38, "MASTICAR", 0]);
vocabulary.push([46, "MATA", 0]);
vocabulary.push([46, "MATAR", 0]);
vocabulary.push([81, "MATARSE", 0]);
vocabulary.push([81, "MATARTE", 0]);
vocabulary.push([81, "MATATE", 0]);
vocabulary.push([47, "MEA", 0]);
vocabulary.push([47, "MEAR", 0]);
vocabulary.push([47, "MEARSE", 0]);
vocabulary.push([47, "MEARTE", 0]);
vocabulary.push([47, "MEATE", 0]);
vocabulary.push([49, "MENEA", 0]);
vocabulary.push([49, "MENEAR", 0]);
vocabulary.push([74, "METE", 0]);
vocabulary.push([74, "METER", 0]);
vocabulary.push([24, "MIRA", 0]);
vocabulary.push([24, "MIRAR", 0]);
vocabulary.push([85, "MIRARSE", 0]);
vocabulary.push([85, "MIRARTE", 0]);
vocabulary.push([85, "MIRATE", 0]);
vocabulary.push([36, "MOSTRAR", 0]);
vocabulary.push([36, "MOSTRARSE", 0]);
vocabulary.push([110, "MOVER", 0]);
vocabulary.push([36, "MUESTRA", 0]);
vocabulary.push([36, "MUESTRASE", 0]);
vocabulary.push([110, "MUEVE", 0]);
vocabulary.push([2, "N", 1]);
vocabulary.push([56, "NADA", 0]);
vocabulary.push([56, "NADAR", 0]);
vocabulary.push([6, "NE", 1]);
vocabulary.push([7, "NO", 1]);
vocabulary.push([46, "NOQUEA", 0]);
vocabulary.push([46, "NOQUEAR", 0]);
vocabulary.push([6, "NORDESTE", 1]);
vocabulary.push([6, "NORESTE", 1]);
vocabulary.push([7, "NOROESTE", 1]);
vocabulary.push([2, "NORTE", 1]);
vocabulary.push([5, "NUEVA", 2]);
vocabulary.push([5, "NUEVO", 2]);
vocabulary.push([7, "NW", 1]);
vocabulary.push([5, "O", 1]);
vocabulary.push([50, "OBJETO", 1]);
vocabulary.push([30, "OBSERVA", 0]);
vocabulary.push([30, "OBSERVAR", 0]);
vocabulary.push([91, "OCULTA", 0]);
vocabulary.push([91, "OCULTAR", 0]);
vocabulary.push([92, "OCULTARSE", 0]);
vocabulary.push([92, "OCULTARTE", 0]);
vocabulary.push([92, "OCULTATE", 0]);
vocabulary.push([5, "OESTE", 1]);
vocabulary.push([73, "OFRECE", 0]);
vocabulary.push([73, "OFRECER", 0]);
vocabulary.push([73, "OFRECERSE", 0]);
vocabulary.push([73, "OFRECESE", 0]);
vocabulary.push([37, "OIR", 0]);
vocabulary.push([42, "OLER", 0]);
vocabulary.push([42, "OLFATEA", 0]);
vocabulary.push([42, "OLFATEAR", 0]);
vocabulary.push([16, "OPCIONES", 0]);
vocabulary.push([64, "OPEN", 0]);
vocabulary.push([59, "ORA", 0]);
vocabulary.push([59, "ORAR", 0]);
vocabulary.push([47, "ORINA", 0]);
vocabulary.push([47, "ORINAR", 0]);
vocabulary.push([47, "ORINARSE", 0]);
vocabulary.push([47, "ORINARTE", 0]);
vocabulary.push([47, "ORINATE", 0]);
vocabulary.push([37, "OYE", 0]);
vocabulary.push([63, "PALADEA", 0]);
vocabulary.push([63, "PALADEAR", 0]);
vocabulary.push([40, "PALPA", 0]);
vocabulary.push([40, "PALPAR", 0]);
vocabulary.push([10, "PARA", 6]);
vocabulary.push([69, "PARTE", 0]);
vocabulary.push([69, "PARTIR", 0]);
vocabulary.push([46, "PATEA", 0]);
vocabulary.push([46, "PATEAR", 0]);
vocabulary.push([60, "PENSAR", 0]);
vocabulary.push([2, "PEQUEÑA", 2]);
vocabulary.push([2, "PEQUEÑO", 2]);
vocabulary.push([60, "PIENSA", 0]);
vocabulary.push([46, "PISOTEA", 0]);
vocabulary.push([46, "PISOTEAR", 0]);
vocabulary.push([71, "PON", 0]);
vocabulary.push([71, "PONER", 0]);
vocabulary.push([71, "PONERSE", 0]);
vocabulary.push([71, "PONERTE", 0]);
vocabulary.push([71, "PONSE", 0]);
vocabulary.push([71, "PONTE", 0]);
vocabulary.push([11, "POR", 6]);
vocabulary.push([108, "PREGUNTA", 0]);
vocabulary.push([108, "PREGUNTAR", 0]);
vocabulary.push([66, "PRENDE", 0]);
vocabulary.push([66, "PRENDER", 0]);
vocabulary.push([63, "PROBAR", 0]);
vocabulary.push([63, "PRUEBA", 0]);
vocabulary.push([101, "PULE", 0]);
vocabulary.push([101, "PULIR", 0]);
vocabulary.push([69, "QUEBRAR", 0]);
vocabulary.push([66, "QUEMA", 0]);
vocabulary.push([66, "QUEMAR", 0]);
vocabulary.push([69, "QUIEBRA", 0]);
vocabulary.push([25, "QUIT", 0]);
vocabulary.push([22, "QUITA", 0]);
vocabulary.push([22, "QUITAR", 0]);
vocabulary.push([22, "QUITARSE", 0]);
vocabulary.push([22, "QUITARTE", 0]);
vocabulary.push([22, "QUITASE", 0]);
vocabulary.push([22, "QUITATE", 0]);
vocabulary.push([29, "RAMLOAD", 0]);
vocabulary.push([28, "RAMSAVE", 0]);
vocabulary.push([2, "RAPIDAMENT", 3]);
vocabulary.push([102, "RASCA", 0]);
vocabulary.push([102, "RASCAR", 0]);
vocabulary.push([52, "RASGA", 0]);
vocabulary.push([52, "RASGAR", 0]);
vocabulary.push([102, "RASPA", 0]);
vocabulary.push([102, "RASPAR", 0]);
vocabulary.push([78, "REBUSCA", 0]);
vocabulary.push([78, "REBUSCAR", 0]);
vocabulary.push([94, "RECORDAR", 0]);
vocabulary.push([94, "RECUERDA", 0]);
vocabulary.push([73, "REGALA", 0]);
vocabulary.push([73, "REGALAR", 0]);
vocabulary.push([73, "REGALARSE", 0]);
vocabulary.push([73, "REGALASE", 0]);
vocabulary.push([78, "REGISTRA", 0]);
vocabulary.push([78, "REGISTRAR", 0]);
vocabulary.push([84, "REGISTRARS", 0]);
vocabulary.push([84, "REGISTRART", 0]);
vocabulary.push([84, "REGISTRATE", 0]);
vocabulary.push([55, "RELLENA", 0]);
vocabulary.push([55, "RELLENAR", 0]);
vocabulary.push([49, "REMOVER", 0]);
vocabulary.push([49, "REMUEVE", 0]);
vocabulary.push([31, "RESPONDE", 0]);
vocabulary.push([31, "RESPONDER", 0]);
vocabulary.push([58, "RETORCER", 0]);
vocabulary.push([58, "RETUERCE", 0]);
vocabulary.push([99, "REUNE", 0]);
vocabulary.push([99, "REUNIR", 0]);
vocabulary.push([59, "REZA", 0]);
vocabulary.push([59, "REZAR", 0]);
vocabulary.push([69, "ROMPE", 0]);
vocabulary.push([69, "ROMPER", 0]);
vocabulary.push([61, "RONCA", 0]);
vocabulary.push([61, "RONCAR", 0]);
vocabulary.push([35, "ROTA", 0]);
vocabulary.push([35, "ROTAR", 0]);
vocabulary.push([5, "RUIDOSAMEN", 3]);
vocabulary.push([3, "S", 1]);
vocabulary.push([63, "SABOREA", 0]);
vocabulary.push([63, "SABOREAR", 0]);
vocabulary.push([75, "SACA", 0]);
vocabulary.push([75, "SACAR", 0]);
vocabulary.push([22, "SACARSE", 0]);
vocabulary.push([22, "SACARTE", 0]);
vocabulary.push([22, "SACASE", 0]);
vocabulary.push([22, "SACATE", 0]);
vocabulary.push([49, "SACUDE", 0]);
vocabulary.push([49, "SACUDIR", 0]);
vocabulary.push([13, "SAL", 0]);
vocabulary.push([41, "SALIDAS", 0]);
vocabulary.push([13, "SALIR", 0]);
vocabulary.push([45, "SALTA", 0]);
vocabulary.push([45, "SALTAR", 0]);
vocabulary.push([45, "SALTARSE", 0]);
vocabulary.push([45, "SALTARTE", 0]);
vocabulary.push([45, "SALTATE", 0]);
vocabulary.push([113, "SALUDA", 0]);
vocabulary.push([113, "SALUDAR", 0]);
vocabulary.push([26, "SALVA", 0]);
vocabulary.push([26, "SALVAR", 0]);
vocabulary.push([26, "SAVE", 0]);
vocabulary.push([8, "SE", 1]);
vocabulary.push([2, "SELA", 4]);
vocabulary.push([2, "SELAS", 4]);
vocabulary.push([2, "SELO", 4]);
vocabulary.push([2, "SELOS", 4]);
vocabulary.push([23, "SENTAR", 0]);
vocabulary.push([23, "SENTARSE", 0]);
vocabulary.push([23, "SENTARTE", 0]);
vocabulary.push([100, "SEPARA", 0]);
vocabulary.push([100, "SEPARAR", 0]);
vocabulary.push([23, "SIENTATE", 0]);
vocabulary.push([4, "SILENCIOSA", 3]);
vocabulary.push([9, "SO", 1]);
vocabulary.push([17, "SOBRE", 6]);
vocabulary.push([67, "SOFOCA", 0]);
vocabulary.push([67, "SOFOCAR", 0]);
vocabulary.push([93, "SOLLOZA", 0]);
vocabulary.push([93, "SOLLOZAR", 0]);
vocabulary.push([21, "SOLTAR", 0]);
vocabulary.push([95, "SOPLA", 0]);
vocabulary.push([95, "SOPLAR", 0]);
vocabulary.push([7, "SUAVE", 2]);
vocabulary.push([10, "SUBE", 0]);
vocabulary.push([10, "SUBETE", 0]);
vocabulary.push([10, "SUBIR", 0]);
vocabulary.push([10, "SUBIRSE", 0]);
vocabulary.push([10, "SUBIRTE", 0]);
vocabulary.push([8, "SUDESTE", 1]);
vocabulary.push([21, "SUELTA", 0]);
vocabulary.push([81, "SUICIDARSE", 0]);
vocabulary.push([81, "SUICIDARTE", 0]);
vocabulary.push([81, "SUICIDATE", 0]);
vocabulary.push([3, "SUR", 1]);
vocabulary.push([8, "SURESTE", 1]);
vocabulary.push([9, "SUROESTE", 1]);
vocabulary.push([9, "SW", 1]);
vocabulary.push([20, "TAKE", 0]);
vocabulary.push([32, "TIRA", 0]);
vocabulary.push([32, "TIRAR", 0]);
vocabulary.push([32, "TIRARSE", 0]);
vocabulary.push([32, "TIRASE", 0]);
vocabulary.push([40, "TOCA", 0]);
vocabulary.push([40, "TOCAR", 0]);
vocabulary.push([20, "TODO", 1]);
vocabulary.push([20, "TOMA", 0]);
vocabulary.push([20, "TOMAR", 0]);
vocabulary.push([58, "TORCER", 0]);
vocabulary.push([46, "TORTURA", 0]);
vocabulary.push([46, "TORTURAR", 0]);
vocabulary.push([38, "TRAGA", 0]);
vocabulary.push([38, "TRAGAR", 0]);
vocabulary.push([38, "TRAGARSE", 0]);
vocabulary.push([38, "TRAGARTE", 0]);
vocabulary.push([38, "TRAGATE", 0]);
vocabulary.push([6, "TRANQUILAM", 3]);
vocabulary.push([68, "TRANSCRIP", 0]);
vocabulary.push([68, "TRANSCRIPC", 0]);
vocabulary.push([68, "TRANSCRIPT", 0]);
vocabulary.push([7, "TRAS", 6]);
vocabulary.push([57, "TREPA", 0]);
vocabulary.push([57, "TREPAR", 0]);
vocabulary.push([58, "TUERCE", 0]);
vocabulary.push([10, "U", 1]);
vocabulary.push([99, "UNE", 0]);
vocabulary.push([99, "UNIR", 0]);
vocabulary.push([10, "UP", 1]);
vocabulary.push([80, "USA", 0]);
vocabulary.push([80, "USAR", 0]);
vocabulary.push([80, "UTILIZA", 0]);
vocabulary.push([80, "UTILIZAR", 0]);
vocabulary.push([86, "VACIA", 0]);
vocabulary.push([86, "VACIAR", 0]);
vocabulary.push([90, "VE", 0]);
vocabulary.push([70, "VERSION", 0]);
vocabulary.push([86, "VERTER", 0]);
vocabulary.push([86, "VERTIR", 0]);
vocabulary.push([71, "VESTIR", 0]);
vocabulary.push([71, "VESTIRSE", 0]);
vocabulary.push([71, "VESTIRTE", 0]);
vocabulary.push([90, "VETE", 0]);
vocabulary.push([4, "VIEJA", 2]);
vocabulary.push([4, "VIEJO", 2]);
vocabulary.push([86, "VIERTE", 0]);
vocabulary.push([71, "VISTE", 0]);
vocabulary.push([71, "VISTESE", 0]);
vocabulary.push([35, "VOLTEA", 0]);
vocabulary.push([35, "VOLTEAR", 0]);
vocabulary.push([15, "VOLVER", 0]);
vocabulary.push([47, "VOMITA", 0]);
vocabulary.push([47, "VOMITAR", 0]);
vocabulary.push([47, "VOMITARSE", 0]);
vocabulary.push([47, "VOMITARTE", 0]);
vocabulary.push([47, "VOMITATE", 0]);
vocabulary.push([15, "VUELVE", 0]);
vocabulary.push([5, "W", 1]);
vocabulary.push([41, "X", 0]);
vocabulary.push([82, "XYZZY", 0]);
vocabulary.push([2, "Y", 5]);
vocabulary.push([43, "Z", 0]);



// SYS MESSAGES

total_sysmessages=68;

sysmessages = [];

sysmessages[0] = "No puedes ver nada, está muy oscuro.\n";
sysmessages[1] = "Puedes ver¬ ";
sysmessages[2] = "¿Qué quieres hacer ahora?      ";
sysmessages[3] = "¿Cuál es tu siguiente orden?";
sysmessages[4] = "¿Qué vas a hacer ahora?       ";
sysmessages[5] = "Teclea tus órdenes";
sysmessages[6] = "¿Cómo? Por favor, prueba con otras palabras.";
sysmessages[7] = "No puedes ir en esa dirección.";
sysmessages[8] = "No entiendo lo que quieres decir...";
sysmessages[9] = "Inventario: ";
sysmessages[10] = " (puesto/a)";
sysmessages[11] = "Nada.";
sysmessages[12] = "¿Seguro? (S/N) \n";
sysmessages[13] = "¿Quieres jugar otra vez? (S/N)\n";
sysmessages[14] = "Adiós...";
sysmessages[15] = "Hecho.\n";
sysmessages[16] = "{class|anykey|Pulsa una tecla para continuar...}\n";
sysmessages[17] = "Has realizado ";
sysmessages[18] = " turno";
sysmessages[19] = "s";
sysmessages[20] = ".\n";
sysmessages[21] = "Tu puntuación es del ";
sysmessages[22] = "%.\n";
sysmessages[23] = "No lo llevas puesto.\n";
sysmessages[24] = "No puedes, ya lo llevas puesto.\n";
sysmessages[25] = "Ya tienes {OREF}.\n";
sysmessages[26] = "No ves eso por aquí.\n";
sysmessages[27] = "No puedes llevar más cosas.\n";
sysmessages[28] = "No lo tienes.\n";
sysmessages[29] = "Pero si ya llevas puesto {OREF}.\n";
sysmessages[30] = "S";
sysmessages[31] = "N";
sysmessages[32] = "Más...";
sysmessages[33] = "> ";
sysmessages[34] = "";
sysmessages[35] = "El tiempo pasa...\n";
sysmessages[36] = "Coges {OREF}.\n";
sysmessages[37] = "Te pones {OREF}.\n";
sysmessages[38] = "Te quitas {OREF}.\n";
sysmessages[39] = "Dejas {OREF} en el suelo.\n";
sysmessages[40] = "No puedes ponerte {OREF}.\n";
sysmessages[41] = "No puedes quitarte {OREF}.\n";
sysmessages[42] = "No puedes quitarte {OREF}.  ¡Ya llevas demasiadas cosas en las manos!\n";
sysmessages[43] = "Desgraciadamente {OREF} pesa demasiado.\n";
sysmessages[44] = "Metes {OREF} en¬";
sysmessages[45] = "Me temo que {OREF} no está en ";
sysmessages[46] = ", ";
sysmessages[47] = " y¬";
sysmessages[48] = ".\n";
sysmessages[49] = "No tienes {OREF}.\n";
sysmessages[50] = "No llevas puesto {OREF}.\n";
sysmessages[51] = ".\n";
sysmessages[52] = "Eso no está en ";
sysmessages[53] = "ningún objeto\n";
sysmessages[54] = "Fichero no encontrado.\n";
sysmessages[55] = "Fichero corrupto.\n";
sysmessages[56] = "Error de lectura/escritura. Fichero no grabado.\n";
sysmessages[57] = "Directorio lleno.\n";
sysmessages[58] = "Introduce el nombre con el que grabaste la partida.";
sysmessages[59] = "No se encontró ninguna partida grabada con ese nombre. Confirma que el nombre es correcto y que realizaste dicha partida en este mismo navegador.\n";
sysmessages[60] = "Introduce el nombre con el que grabar la partida.";
sysmessages[61] = "";
sysmessages[62] = "Por favor, prueba con otras palabras.";
sysmessages[63] = "Aquí ";
sysmessages[64] = "está ";
sysmessages[65] = "están ";
sysmessages[66] = "dentro hay ";
sysmessages[67] = "encima hay ";

// USER MESSAGES

total_messages=31;

messages = [];

messages[0] = "...";
messages[1000] = "Puedes ir¬ ";
messages[1001] = "No hay salidas visibles.\n";
messages[1002] = "";
messages[1003] = "";
messages[1004] = "al norte";
messages[1005] = "al sur";
messages[1006] = "al este";
messages[1007] = "al oeste";
messages[1008] = "al noreste";
messages[1009] = "al noroeste";
messages[1010] = "al sureste";
messages[1011] = "al suroeste";
messages[1012] = "arriba";
messages[1013] = "abajo";
messages[1014] = "dentro";
messages[1015] = "fuera";
messages[1100] = "Título del juego";
messages[1101] = "{ACTION|Guardar|Guardar} | {ACTION|Cargar|Cargar} | {ACTION|Opciones|Opciones}";
messages[1102] = "Inventario";
messages[1103] = "Salidas visibles";
messages[1104] = "Tareas actuales";
messages[1105] = "Salud";
messages[1106] = "Psique";
messages[2000] = "Estadísticas";
messages[2001] = "Progreso principal: {class|data|65%}\n Puntuación actual: {class|data|{flag|60}}\n Turnos jugados: {class|data|{flag|31}} ";
messages[3000] = "Versión: 0.00";
messages[3001] = "Desarrollador por\n {class|data|[autor]}\n \n Creado con ngPaws\n Fecha: dd/mm/aaaa\n";
messages[4101] = "Task 1 (mtx 4101)";
messages[4102] = "Task 2";
messages[4103] = "Task 3";

// WRITE MESSAGES

total_writemessages=540;

writemessages = [];

writemessages[0] = "RESPONSE_START";
writemessages[1] = "Instrucción no permitida en esta pantalla. Vuelve al juego...";
writemessages[2] = "No ves nada.";
writemessages[3] = "No hay nada útil por aquí.";
writemessages[4] = "No llevas nada.";
writemessages[5] = "No ves eso por aquí.";
writemessages[6] = "No ves eso por aquí.";
writemessages[7] = "No hay nada ahí.";
writemessages[8] = "No hay nada ahí.";
writemessages[9] = "No ves eso por aquí.";
writemessages[10] = "No ves eso por aquí.";
writemessages[11] = "No llevas nada.";
writemessages[12] = "No llevas nada.";
writemessages[13] = "No llevas nada.";
writemessages[14] = "No llevas nada";
writemessages[15] = "No llevas nada puesto.";
writemessages[16] = "No llevas nada para ponerte.";
writemessages[17] = "No llevas nada.";
writemessages[18] = "¡No puedes tirarlo todo a la vez!";
writemessages[19] = "Por favor, intenta especificar un poco más.";
writemessages[20] = "RESPONSE_USER";
writemessages[21] = "Un objeto abstracto sin interés.";
writemessages[22] = "showNotification('Esto es una notificación de demostración de 3 segundos.', 3000, 0.5);";
writemessages[23] = "{class|data|¡Acción 1D100!}";
writemessages[24] = "[1D100] - Esta acción tiene un {flag|201}% de probabilidades de fallar. Si falla, no se podra reintentar. ";
writemessages[25] = "¿Quieres intentar la acción ahora? (S/N) ";
writemessages[26] = "sn";
writemessages[27] = "Chance: {flag|201} - Random: {flag|202} - LE# {flag|202} {flag|201}?";
writemessages[28] = "Epsito: Texto del epsito {flag|202}";
writemessages[29] = "Fracaso: {flag|202}";
writemessages[30] = "Cagao";
writemessages[31] = "RESPONSE_DEFAULT_START";
writemessages[32] = "Ahora mismo no te apetece.";
writemessages[33] = "Estás bien así.";
writemessages[34] = "Te miras de arriba a abajo pero no ves nada especial.";
writemessages[35] = "No encuentras nada destacable.";
writemessages[36] = "Esa no es la solución...";
writemessages[37] = "Ahora mismo no te apetece...";
writemessages[38] = "Has jugado a demasiadas aventuras conversacionales...";
writemessages[39] = "Partida guardada correctamente. :)";
writemessages[40] = "Partida cargada correctamente. :)";
writemessages[41] = "No puedes coger eso que dices.";
writemessages[42] = "Por favor, especifica qué quieres coger.";
writemessages[43] = "Mejor no coges {OREF}, está bien donde está.";
writemessages[44] = "Dejas {OREF} sobre {OBJECT|14}.";
writemessages[45] = "Dejas {OREF} sobre {OBJECT|14}.";
writemessages[46] = "No puedes dejar {OREF} sobre {OBJECT|14}.";
writemessages[47] = "No puedes dejar {OREF} en {OBJECT|14}.";
writemessages[48] = "No puedes dejar eso que dices.";
writemessages[49] = "Por favor, especifica qué quieres dejar.";
writemessages[50] = "Por favor, especifica qué quieres sacar.";
writemessages[51] = "No puedes sacar eso que dices.";
writemessages[52] = "No puedes sacar eso.";
writemessages[53] = "No puedes sacar eso.";
writemessages[54] = "No puedes sacar {OREF} de ningún sitio, en todo caso puedes quitártelo porque lo llevas puesto...";
writemessages[55] = "¿De dónde quieres sacar eso?";
writemessages[56] = "No puedes sacar {OREF} de eso.";
writemessages[57] = "¿De dónde dices que quieres sacar {OREF}?";
writemessages[58] = "Antes abre {OBJECT|15}.";
writemessages[59] = "Antes tendrias que abrir {OBJECT|15}.";
writemessages[60] = "No puedes sacar nada de ahí porque no está aquí.";
writemessages[61] = "No puedes coger nada de ahí porque no está aquí.";
writemessages[62] = "No puedes sacar cosas de ahí.";
writemessages[63] = "Por favor, especifica qué quieres meter.";
writemessages[64] = "No puedes meter eso que dices.";
writemessages[65] = "No puedes meter eso.";
writemessages[66] = "No puedes meter eso.";
writemessages[67] = "Primero deberías quitarte {OREF} para poder hacer eso...";
writemessages[68] = "No tienes {OREF}.";
writemessages[69] = "¿Dónde quieres meter eso?";
writemessages[70] = "No puedes meter {OREF} a eso.";
writemessages[71] = "¿Dónde dices que quieres meter {OREF}?";
writemessages[72] = "No puedes meter cosas dentro de eso...";
writemessages[73] = "No puedes meter {OREF} ahí porque no llevas eso.";
writemessages[74] = "No puedes meter {OREF} ahí porque no llevas eso.";
writemessages[75] = "Antes deberias abrir {OBJECT|15}.";
writemessages[76] = "Antes deberias abrir {OBJECT|15}";
writemessages[77] = "No te puedes quitar eso que dices.";
writemessages[78] = "Por favor, especifica qué quieres quitarte.";
writemessages[79] = "No te puedes poner eso que dices.";
writemessages[80] = "Por favor, especifica qué quieres ponerte.";
writemessages[81] = "No puedes tirar de eso que dices.";
writemessages[82] = "Por favor, especifica de qué quieres tirar.";
writemessages[83] = "No sería adecuado.";
writemessages[84] = "No está aquí.";
writemessages[85] = "No ves mucho sentido a tirar de eso.";
writemessages[86] = "No parece que se consiga nada tirando de {OREF}.";
writemessages[87] = "Para tirar de {OREF} tendría que estar aquí.";
writemessages[88] = "No puedes examinar dentro de eso que dices..";
writemessages[89] = "Por favor, especifica dentro de qué quieres examinar.";
writemessages[90] = "No ves nada interesante hacia allí.";
writemessages[91] = "No puedes mirar eso que dices.";
writemessages[92] = "No puedes esconder eso que dices.";
writemessages[93] = "Por favor, especifica qué quieres esconder.";
writemessages[94] = "Piensas que no conseguirás que pase desapercibido.";
writemessages[95] = "No está aquí.";
writemessages[96] = "No crees que eso sirva de algo...";
writemessages[97] = "Escondiendo {OREF} no solucionarás nada.";
writemessages[98] = "No ves {OREF} por aquí.";
writemessages[99] = "La autocompasión no conduce a nada.";
writemessages[100] = "No recuerdas nada que te pueda ser útil.";
writemessages[101] = "No hay respuesta alguna.";
writemessages[102] = "Esa es una opción muy cobarde.";
writemessages[103] = "No puedes empujar eso que dices.";
writemessages[104] = "Por favor, especifica qué quieres empujar.";
writemessages[105] = "No crees que le guste demasiado.";
writemessages[106] = "No se mueve.";
writemessages[107] = "No está aquí.";
writemessages[108] = "No ves eso por aquí.";
writemessages[109] = "No ves mucho sentido a empujar eso.";
writemessages[110] = "No parece que se consiga nada empujando {OREF}.";
writemessages[111] = "Para empujar {OREF} tendría que estar aquí.";
writemessages[112] = "No puedes mover eso que dices.";
writemessages[113] = "Por favor, especifica qué quieres mover.";
writemessages[114] = "No crees que le guste demasiado.";
writemessages[115] = "No está aquí.";
writemessages[116] = "No ves mucho sentido a mover eso.";
writemessages[117] = "No parece que se consiga nada moviendo {OREF}.";
writemessages[118] = "Para mover {OREF} tendría que estar aquí.";
writemessages[119] = "No puedes girar eso que dices.";
writemessages[120] = "Por favor, especifica qué quieres girar.";
writemessages[121] = "No sería apropiado.";
writemessages[122] = "No ves utilidad en girar eso.";
writemessages[123] = "No está aquí.";
writemessages[124] = "No ves eso por aquí.";
writemessages[125] = "No ves mucho sentido a girar eso.";
writemessages[126] = "No ves sentido a girar {OREF}.";
writemessages[127] = "Girar {OREF} requiere su presencia.";
writemessages[128] = "No puedes lanzar eso que dices.";
writemessages[129] = "Por favor, especifica qué quieres lanzar.";
writemessages[130] = "La violencia no es la solución.";
writemessages[131] = "No puedes lanzar eso.";
writemessages[132] = "No está aquí.";
writemessages[133] = "No ves eso por aquí.";
writemessages[134] = "No, no ves sentido a lanzar eso.";
writemessages[135] = "Lanzas {OREF}.";
writemessages[136] = "Desgraciadamente no tienes {OREF}.";
writemessages[137] = "No parece decir nada.";
writemessages[138] = "No está aquí.";
writemessages[139] = "Prestas atención a ver si oyes algo más pero no escuchas nada en especial.";
writemessages[140] = "No puedes comer eso que dices.";
writemessages[141] = "Por favor, especifica qué quieres comer.";
writemessages[142] = "No crees que se deje. En cualquier caso no es muy apropiado.";
writemessages[143] = "No es muy buena idea que te comas eso.";
writemessages[144] = "No está aquí.";
writemessages[145] = "No quieres comer eso.";
writemessages[146] = "No ves sentido a comerte {OREF}.";
writemessages[147] = "Te comes {OREF}.";
writemessages[148] = "Para comerte {OREF} debería estar aquí.";
writemessages[149] = "No puedes beber eso que dices.";
writemessages[150] = "Por favor, especifica qué quieres beber.";
writemessages[151] = "Estás perdiendo el rumbo.";
writemessages[152] = "No está aquí.";
writemessages[153] = "No quieres beber eso.";
writemessages[154] = "No ves sentido a beberte {OREF}.";
writemessages[155] = "Te bebes {OREF}.";
writemessages[156] = "Para beberte {OREF} debería estar aquí.";
writemessages[157] = "No tienes ganas.";
writemessages[158] = "No puedes escupir a eso que dices.";
writemessages[159] = "Por favor, especifica a qué quieres escupir.";
writemessages[160] = "Eso sería de muy mala educación.";
writemessages[161] = "No está aquí.";
writemessages[162] = "No le ves sentido a escupir a eso.";
writemessages[163] = "No quieres escupir a {OREF}.";
writemessages[164] = "No ves {OREF} por aquí.";
writemessages[165] = "No puedes tocar eso que dices.";
writemessages[166] = "Por favor, especifica qué quieres tocar.";
writemessages[167] = "No se deja.";
writemessages[168] = "No está aquí.";
writemessages[169] = "No le ves sentido a tocar eso.";
writemessages[170] = "No quieres tocar {OREF}.";
writemessages[171] = "No ves {OREF} por aquí.";
writemessages[172] = "No puedes oler eso que dices.";
writemessages[173] = "Por favor, especifica qué quieres oler.";
writemessages[174] = "Es de mala educación.";
writemessages[175] = "No está aquí.";
writemessages[176] = "No le ves sentido a oler eso.";
writemessages[177] = "La verdad es que {OREF} huele bien.";
writemessages[178] = "La verdad es que {OREF} huele bien.";
writemessages[179] = "No quieres oler {OREF}.";
writemessages[180] = "La verdad es que {OREF} huele bien.";
writemessages[181] = "No ves {OREF} por aquí.";
writemessages[182] = "No puedes agitar eso que dices.";
writemessages[183] = "Por favor, especifica qué quieres agitar.";
writemessages[184] = "No crees que le guste demasiado.";
writemessages[185] = "No está aquí.";
writemessages[186] = "No le ves sentido a agitar eso.";
writemessages[187] = "No quieres agitar {OREF}.";
writemessages[188] = "No ves {OREF} por aquí.";
writemessages[189] = "No puedes balancearte en eso que dices.";
writemessages[190] = "Por favor, especifica dónde quieres balancearte.";
writemessages[191] = "No crees que le guste demasiado.";
writemessages[192] = "No está aquí.";
writemessages[193] = "No es algo adecuado para balancearse.";
writemessages[194] = "Crees que {OREF} no es algo adecuado para balancearse.";
writemessages[195] = "No ves {OREF} por aquí.";
writemessages[196] = "No tienes sueño.";
writemessages[197] = "Saltas, sin conseguir nada.";
writemessages[198] = "Rezas todo lo que sabes.";
writemessages[199] = "No quieres cavar aquí.";
writemessages[200] = "Pensar siempre es bueno.";
writemessages[201] = "No tienes sueño.";
writemessages[202] = "Cantas fatal.";
writemessages[203] = "La verdad es que bailar no se te da muy bien...";
writemessages[204] = "No puedes atacar a eso que dices.";
writemessages[205] = "Por favor, especifica a qué o a quién quieres atacar.";
writemessages[206] = "La violencia no es buena.";
writemessages[207] = "No está aquí.";
writemessages[208] = "La violencia no es la solución.";
writemessages[209] = "Atacar {OREF} no solucionará nada.";
writemessages[210] = "No ves {OREF} por aquí, en cualquier caso la violencia no es la solución.";
writemessages[211] = "No puedes golpear a eso que dices.";
writemessages[212] = "Por favor, especifica a qué o a quién quieres golpear.";
writemessages[213] = "La violencia no es buena.";
writemessages[214] = "No está aquí.";
writemessages[215] = "La violencia no es la solución.";
writemessages[216] = "Golpear {OREF} no solucionará nada.";
writemessages[217] = "No ves {OREF} por aquí, en cualquier caso la violencia no es la solución.";
writemessages[218] = "USAR es demasiado genérico. Por favor, sé más concreto. Por ejemplo, si quieres barrer el suelo utiliza BARRER SUELO, no USAR ESCOBA.";
writemessages[219] = "No puedes examinar dentro de eso que dices.";
writemessages[220] = "Por favor, especifica dentro de qué quieres examinar.";
writemessages[221] = "No puedes examinar eso que dices.";
writemessages[222] = "Por favor, especifica qué o a quién quieres examinar.";
writemessages[223] = "Vaya ocurrencia...";
writemessages[224] = "No está aquí.";
writemessages[225] = "Es de mala educación.";
writemessages[226] = "No está aquí.";
writemessages[227] = "No le ves sentido a examinar dentro de eso.";
writemessages[228] = "No le ves sentido a examinar eso.";
writemessages[229] = "Examinas {OREF} pero no ves nada especial ";
writemessages[230] = ".";
writemessages[231] = "Examinas {OREF} pero no ves nada especial ";
writemessages[232] = ".";
writemessages[233] = "Examinas {OREF} pero no ves nada especial";
writemessages[234] = ".";
writemessages[235] = "Examinas {OREF} pero no ves nada especial. Si lo abres podrás ver su contenido.";
writemessages[236] = "Examinas {OREF} pero no ves nada especial. Si lo abres podrás ver su contenido.";
writemessages[237] = "Examinas {OREF} pero no ves nada especial ";
writemessages[238] = ".";
writemessages[239] = "Examinas {OREF} pero no ves nada especial.";
writemessages[240] = "No ves {OREF} por aquí.";
writemessages[241] = "No puedes limpiar eso que dices.";
writemessages[242] = "Por favor, especifica qué quieres limpiar.";
writemessages[243] = "No se deja.";
writemessages[244] = "No está aquí.";
writemessages[245] = "No le ves sentido a limpiar eso.";
writemessages[246] = "Limpias {OREF}. No hay efecto alguno.";
writemessages[247] = "No ves {OREF} por aquí.";
writemessages[248] = "No puedes rascar eso que dices.";
writemessages[249] = "Por favor, especifica qué quieres rascar.";
writemessages[250] = "No se deja.";
writemessages[251] = "No está aquí.";
writemessages[252] = "No quieres rascar eso.";
writemessages[253] = "Rascas {OREF}. No hay efecto alguno.";
writemessages[254] = "No ves {OREF}_ por aquí.";
writemessages[255] = "No puedes quemar eso que dices.";
writemessages[256] = "Por favor, especifica qué quieres quemar.";
writemessages[257] = "La barbarie no solucionará tus problemas.";
writemessages[258] = "No está aquí.";
writemessages[259] = "No le ves sentido a quemar eso.";
writemessages[260] = "Quemar {OREF} no es la solución.";
writemessages[261] = "No ves {OREF} por aquí.";
writemessages[262] = "No puedes apagar eso que dices.";
writemessages[263] = "Por favor, especifica qué quieres apagar.";
writemessages[264] = "No le ves sentido a apagar eso.";
writemessages[265] = "No quieres apagar {OREF}.";
writemessages[266] = "No ves {OREF} por aquí.";
writemessages[267] = "No puedes cortar eso que dices.";
writemessages[268] = "Por favor, especifica qué quieres cortar.";
writemessages[269] = "La violencia no es la solución.";
writemessages[270] = "No crees que cortar eso sea de mucha utilidad.";
writemessages[271] = "No está aquí.";
writemessages[272] = "No le ves sentido a cortar eso.";
writemessages[273] = "No ves sentido a cortar {OREF}.";
writemessages[274] = "No ves {OREF} por aquí.";
writemessages[275] = "No puedes atar eso que dices.";
writemessages[276] = "Por favor, especifica qué quieres atar.";
writemessages[277] = "Crees que no se va a dejar.";
writemessages[278] = "No está aquí.";
writemessages[279] = "No le ves sentido a atar eso.";
writemessages[280] = "No quieres atar {OREF}.";
writemessages[281] = "No ves {OREF} por aquí.";
writemessages[282] = "No puedes desatar eso que dices.";
writemessages[283] = "Por favor, especifica qué quieres desatar.";
writemessages[284] = "¿Desatarle?.";
writemessages[285] = "No está aquí.";
writemessages[286] = "No le ves sentido a desatar eso.";
writemessages[287] = "¿Desatar {OREF}?, ¿para qué?";
writemessages[288] = "No ves {OREF} por aquí.";
writemessages[289] = "No puedes llenar eso que dices.";
writemessages[290] = "Por favor, especifica qué quieres llenar.";
writemessages[291] = "No le ves sentido a llenar eso.";
writemessages[292] = "No quieres llenar {OREF}.";
writemessages[293] = "No puedes llenar {OREF}.";
writemessages[294] = "No ves {OREF} por aquí.";
writemessages[295] = "Mejor no.";
writemessages[296] = "No te apetece trepar.";
writemessages[297] = "No puedes retorcer eso que dices.";
writemessages[298] = "Por favor, especifica qué quieres retorcer.";
writemessages[299] = "La violencia no es la solución.";
writemessages[300] = "No está aquí.";
writemessages[301] = "No le ves sentido a retorcer eso.";
writemessages[302] = "No quieres retorcer {OREF}.";
writemessages[303] = "No ves {OREF} por aquí.";
writemessages[304] = "No puedes apretar eso que dices.";
writemessages[305] = "Por favor, especifica qué quieres apretar.";
writemessages[306] = "La violencia no es la solución.";
writemessages[307] = "No está aquí.";
writemessages[308] = "No le ves sentido a apretar eso.";
writemessages[309] = "No quieres apretar {OREF}.";
writemessages[310] = "No ves {OREF} por aquí.";
writemessages[311] = "No puedes besar eso que dices.";
writemessages[312] = "Por favor, especifica a qué o a quién quieres besar.";
writemessages[313] = "No se deja.";
writemessages[314] = "No está aquí.";
writemessages[315] = "No quieres besar eso.";
writemessages[316] = "No quieres besar {OREF}.";
writemessages[317] = "No ves {OREF} por aquí.";
writemessages[318] = "No puedes abrazar eso que dices.";
writemessages[319] = "Por favor, especifica a qué o a quién quieres abrazar.";
writemessages[320] = "Ni se deja, ni crees que sea muy apropiado.";
writemessages[321] = "No está aquí.";
writemessages[322] = "No quieres abrazar eso.";
writemessages[323] = "No quieres abrazar {OREF}.";
writemessages[324] = "No ves {OREF} por aquí.";
writemessages[325] = "No puedes conectar eso que dices.";
writemessages[326] = "Por favor, especifica qué quieres conectar.";
writemessages[327] = "No ves la manera de conectar eso.";
writemessages[328] = "Activas {OREF}.";
writemessages[329] = "Ya lo está.";
writemessages[330] = "No ves la manera de activar {OREF}.";
writemessages[331] = "No ves {OREF} por aquí.";
writemessages[332] = "No puedes desconectar eso que dices.";
writemessages[333] = "Por favor, especifica qué quieres desconectar.";
writemessages[334] = "No le ves sentido a desconectar eso.";
writemessages[335] = "Desactivas {OREF}.";
writemessages[336] = "Ya lo está.";
writemessages[337] = "No puedes desconectar {OREF}.";
writemessages[338] = "No ves {OREF} por aquí.";
writemessages[339] = "No puedes abrir eso que dices.";
writemessages[340] = "Por favor, especifica qué quieres abrir.";
writemessages[341] = "No le ves sentido a abrir eso.";
writemessages[342] = "Abres {OREF}.";
writemessages[343] = "Está cerrado con llave.";
writemessages[344] = "Ya lo está.";
writemessages[345] = "Ya lo está.";
writemessages[346] = "No puedes abrir {OREF}.";
writemessages[347] = "No ves {OREF} por aquí.";
writemessages[348] = "No puedes cerrar eso que dices.";
writemessages[349] = "Por favor, especifica qué quieres cerrar.";
writemessages[350] = "No le ves sentido a cerrar eso.";
writemessages[351] = "Cierras {OREF}.";
writemessages[352] = "Ya lo está.";
writemessages[353] = "No puedes cerrar {OREF}.";
writemessages[354] = "No ves {OREF} por aquí.";
writemessages[355] = "No puedes leer eso que dices.";
writemessages[356] = "Por favor, especifica qué quieres leer.";
writemessages[357] = "No le ves sentido a leer eso.";
writemessages[358] = "No puedes leer {OREF}.";
writemessages[359] = "No ves {OREF} por aquí.";
writemessages[360] = "No puedes chupar eso que dices.";
writemessages[361] = "Por favor, especifica qué quieres chupar.";
writemessages[362] = "Eso sería muy poco apropiado.";
writemessages[363] = "Piensas en comerte {OREF} pero te contienes.";
writemessages[364] = "No está aquí.";
writemessages[365] = "No te imaginas chupando eso.";
writemessages[366] = "No quieres chupar {OREF}.";
writemessages[367] = "No ves {OREF} por aquí.";
writemessages[368] = "No puedes romper eso que dices.";
writemessages[369] = "Por favor, especifica qué quieres romper.";
writemessages[370] = "La violencia no es buena.";
writemessages[371] = "No está aquí.";
writemessages[372] = "La violencia no es la solución.";
writemessages[373] = "Romper {OREF} no solucionará nada.";
writemessages[374] = "No ves {OREF} por aquí; en cualquier caso, romper eso no es la solución.";
writemessages[375] = "No puedes comprar eso que dices.";
writemessages[376] = "Por favor, especifica qué quieres comprar.";
writemessages[377] = "Todo el mundo tiene un precio... pero no está en venta.";
writemessages[378] = "No está aquí.";
writemessages[379] = "No hay nada en venta.";
writemessages[380] = "No hay nada en venta.";
writemessages[381] = "No ves {OREF} por aquí.";
writemessages[382] = "No puedes doblar eso que dices.";
writemessages[383] = "Por favor, especifica qué quieres doblar.";
writemessages[384] = "Piensas que ya está bien como está...";
writemessages[385] = "No está aquí.";
writemessages[386] = "Prefieres no doblar eso.";
writemessages[387] = "Doblar {OREF} no solucionará nada.";
writemessages[388] = "No ves {OREF} por aquí.";
writemessages[389] = "No puedes desdoblar eso que dices.";
writemessages[390] = "Por favor, especifica qué quieres desdoblar.";
writemessages[391] = "Piensas que ya está bien como está...";
writemessages[392] = "No está aquí.";
writemessages[393] = "¿Para qué quieres desdoblar eso?";
writemessages[394] = "¿Desdoblar {OREF}?, ¿para qué?";
writemessages[395] = "No ves {OREF} por aquí.";
writemessages[396] = "No puedes dar eso.";
writemessages[397] = "Por favor, especifica qué quieres dar.";
writemessages[398] = "No puedes dar eso que dices.";
writemessages[399] = "No puedes dar eso.";
writemessages[400] = "Primero deberías quitarte {OREF} para poder hacer eso...";
writemessages[401] = "No tienes {OREF}.";
writemessages[402] = "¿A quién quieres dar eso?";
writemessages[403] = "No puedes darle {OREF} a eso.";
writemessages[404] = "No puedes darle {OREF} a eso.";
writemessages[405] = "Estás perdiendo la cabeza...";
writemessages[406] = "No le puedes dar {OREF} porque no está aquí.";
writemessages[407] = "Le ofreces {OREF} pero no le hace ni caso.";
writemessages[408] = "No puedes mostrar eso.";
writemessages[409] = "Por favor, especifica qué quieres mostrar.";
writemessages[410] = "No puedes mostrar eso que dices.";
writemessages[411] = "No puedes mostrar eso.";
writemessages[412] = "Primero deberías quitarte {OREF} para poder hacer eso...";
writemessages[413] = "No llevas {OREF}.";
writemessages[414] = "¿A quién quieres mostrar eso?";
writemessages[415] = "No puedes mostrarle {OREF} a eso.";
writemessages[416] = "No puedes mostrarle {OREF} a eso.";
writemessages[417] = "Estás perdiendo la cabeza...";
writemessages[418] = "No le puedes mostrar {OREF} porque no está aquí.";
writemessages[419] = "Le muestras {OREF} pero no le hace ni caso.";
writemessages[420] = "No puedes soplar eso que dices.";
writemessages[421] = "Por favor, especifica a qué quieres soplar.";
writemessages[422] = "No crees que le guste demasiado.";
writemessages[423] = "No está aquí.";
writemessages[424] = "No le ves sentido a soplar eso.";
writemessages[425] = "No quieres soplar {OREF}.";
writemessages[426] = "No ves eso que dices.";
writemessages[427] = "Mejor no, puede enfadarse.";
writemessages[428] = "Gritas con fuerza pero no te oye.";
writemessages[429] = "Gritas lo más fuerte que puedes y... no sucede nada.";
writemessages[430] = "No crees que haga falta.";
writemessages[431] = "No está aquí.";
writemessages[432] = "Pellizcas tu mejilla creyendo que despertarás pero... no estás soñando...¿o sí?";
writemessages[433] = "No puedes levantar eso que dices.";
writemessages[434] = "Por favor, especifica qué quieres levantar.";
writemessages[435] = "No crees que le guste demasiado.";
writemessages[436] = "No está aquí.";
writemessages[437] = "No ves mucho sentido a levantar eso.";
writemessages[438] = "No puedes levantar {OREF}. Pesa demasiado.";
writemessages[439] = "No conseguirás nada levantando eso.";
writemessages[440] = "Para levantar {OREF} tendría que estar aquí.";
writemessages[441] = "No puedes arrastrar eso que dices.";
writemessages[442] = "Por favor, especifica qué quieres arrastrar.";
writemessages[443] = "No crees que le guste demasiado.";
writemessages[444] = "No está aquí.";
writemessages[445] = "No ves mucho sentido a arrastrar eso.";
writemessages[446] = "No parece que se consiga nada arrastrando {OREF}.";
writemessages[447] = "Para arrastrar {OREF} tendría que estar aquí.";
writemessages[448] = "No puedes vaciar eso que dices.";
writemessages[449] = "Por favor, especifica qué quieres vaciar.";
writemessages[450] = "No es lo más apropiado.";
writemessages[451] = "No está aquí.";
writemessages[452] = "No le ves sentido a vaciar eso.";
writemessages[453] = "No quieres vaciar {OREF}.";
writemessages[454] = "No ves {OREF} por aquí.";
writemessages[455] = "No puedes llamar a eso que dices.";
writemessages[456] = "Por favor, especifica a qué o a quién quieres llamar.";
writemessages[457] = "No hay respuesta...";
writemessages[458] = "No está aquí.";
writemessages[459] = "No le ves sentido a llamar a eso.";
writemessages[460] = "No quieres llamar a {OREF}.";
writemessages[461] = "No ves {OREF} por aquí.";
writemessages[462] = "No puedes preguntar a eso que dices.";
writemessages[463] = "Por favor, especifica a quién quieres preguntar.";
writemessages[464] = "No hay respuesta alguna...";
writemessages[465] = "No está aquí.";
writemessages[466] = "No le ves sentido a preguntar a eso.";
writemessages[467] = "No quieres preguntar a {OREF}.";
writemessages[468] = "No ves {OREF} por aquí.";
writemessages[469] = "No puedes consultar eso que dices.";
writemessages[470] = "Por favor, especifica qué quieres consultar.";
writemessages[471] = "No le ves sentido a consultar eso.";
writemessages[472] = "No quieres consultar {OREF}.";
writemessages[473] = "No ves {OREF} por aquí.";
writemessages[474] = "No puedes insultar a eso que dices.";
writemessages[475] = "Por favor, especifica a quién quieres insultar.";
writemessages[476] = "No parece hacerte ni caso...";
writemessages[477] = "No está aquí.";
writemessages[478] = "No conseguirás nada insultando a eso.";
writemessages[479] = "Estás perdiendo la cabeza...";
writemessages[480] = "Ni ves eso que dices, ni crees que sea útil insultarle.";
writemessages[481] = "No puedes unir eso que dices.";
writemessages[482] = "Por favor, especifica qué quieres unir.";
writemessages[483] = "No crees que le guste demasiado.";
writemessages[484] = "No está aquí.";
writemessages[485] = "No ves mucho sentido a unir eso.";
writemessages[486] = "No parece que se consiga nada uniendo {OREF}.";
writemessages[487] = "Para unir {OREF} tendría que estar aquí.";
writemessages[488] = "No puedes separar eso que dices.";
writemessages[489] = "Por favor, especifica qué quieres separar.";
writemessages[490] = "No crees que le guste demasiado.";
writemessages[491] = "No está aquí.";
writemessages[492] = "No ves mucho sentido a separar eso.";
writemessages[493] = "No parece que se consiga nada separando {OREF}.";
writemessages[494] = "Para separar {OREF}, tendría que estar aquí.";
writemessages[495] = "No puedes hablar con eso que dices.";
writemessages[496] = "Por favor, especifica con quién quieres hablar.";
writemessages[497] = "No parece interesarse por tu charla.";
writemessages[498] = "No está aquí.";
writemessages[499] = "No quieres hablarle a eso.";
writemessages[500] = "Estás perdiendo la cabeza...";
writemessages[501] = "Ni ves eso que dices, ni crees que sea útil hablarle.";
writemessages[502] = "No puedes ir ahí donde dices.";
writemessages[503] = "Por favor, especifica dónde quieres ir.";
writemessages[504] = "Prefieres no acercarte más...";
writemessages[505] = "No sabes dónde está.";
writemessages[506] = "No le ves sentido a ir hacia eso.";
writemessages[507] = "No quieres ir a {OREF}.";
writemessages[508] = "No ves eso que dices.";
writemessages[509] = "No puedes ir ";
writemessages[510] = ". ";
writemessages[511] = "RESPONSE_DEFAULT_END";
writemessages[512] = "PRO1";
writemessages[513] = "Otro juego con ngPaws - @teksait";
writemessages[514] = "Eres Frankie Rizzo, un músico de jazz que un día fue la estrella de Swingville. Ahora pasas los días tocando tu saxofón desafinado en las esquinas de las calles, tratando de ganar unas monedas para sobrevivir. Tu instrumento, más desafinado que las promesas políticas en época de elecciones, emite sonidos que podrían hacer temblar los cimientos de un edificio...";
writemessages[515] = "Pero con cada nota mal tocada, sonríes con ese optimismo imperturbable que solo los verdaderos soñadores poseen. Los transeúntes, acostumbrados ya a tus serenatas cacofónicas, te lanzan monedas como si fueran sacrificios a un dios del caos musical.";
writemessages[516] = "Así es tu vida ahora, la del hombre que desafina al destino y se ríe en la cara del fracaso. Tu melodía retorcida es tu firma, tu legado en una ciudad que ya olvidó lo que significa el verdadero ritmo.";
writemessages[517] = "Y así, como cualquier otro día, tu aventura comienza en el callejón de los recuerdos, interpretando unas notas chirriantes por apenas unas monedas. Es como si el universo te estuviera susurrando: 'Hoy será otro día de esos', pero algo en el aire te dice que hoy será diferente…";
writemessages[518] = "{class|voice|- ¿Luciérnagas borrachas? Eso sí que sería un espectáculo para recordar. Pero, ¿sabes qué sería aún mejor? ¡Que tú tocaras algo que no desafinara tanto como esa vez en el concierto de la boda de tu prima!}";
writemessages[519] = "PRO2";
writemessages[520] = "refreshScreen();";
writemessages[521] = "showOverlayDialog(1000, 'Opciones del juego', '1: Música','2: Efectos de sonido','3: Efecto de escritura','','4: Cargar partida','5: Guardar partida','','ESC: Volver',);";
writemessages[522] = "La música se ha activado.";
writemessages[523] = "closeOverlay();";
writemessages[524] = "La música se ha desactivado.";
writemessages[525] = "closeOverlay();";
writemessages[526] = "Los efectos de sonido se han activado.";
writemessages[527] = "closeOverlay();";
writemessages[528] = "Los efectos de sonido se han desactivado.";
writemessages[529] = "closeOverlay();";
writemessages[530] = "El efecto de escritura se ha activado.";
writemessages[531] = "closeOverlay();";
writemessages[532] = "El efecto de escritura se ha desactivado.";
writemessages[533] = "closeOverlay();";
writemessages[534] = "Partida cargada correctamente. :)";
writemessages[535] = "closeOverlay();";
writemessages[536] = "Partida guardada correctamente. :)";
writemessages[537] = "closeOverlay();";
writemessages[538] = "Ok.";
writemessages[539] = "closeOverlay();";

// LOCATION MESSAGES

total_location_messages=23;

locations = [];

locations[0] = "{class|loc|Swingville, 2 de abril de 1985}\n La bruma de los buenos tiempos del jazz se disipa lentamente en las calles de esta ciudad una vez más vibrante. Donde antes resonaban acordes frenéticos e improvisación desenfrenada, ahora solo queda un eco melancólico, como una trompeta sin aire.\n";
locations[1] = "OPCIONES {action|volver|volver}";
locations[2] = "CREDITOS {action|volver|volver}";
locations[3] = "AYUDA {action|volver|volver}";
locations[4] = "";
locations[5] = "";
locations[6] = "";
locations[7] = "";
locations[8] = "";
locations[9] = "";
locations[10] = "";
locations[11] = "";
locations[12] = "";
locations[13] = "";
locations[14] = "";
locations[15] = "";
locations[16] = "";
locations[17] = "";
locations[18] = "";
locations[19] = "";
locations[20] = "";
locations[21] = "{class|loc|La calle de los recuerdos}\n La calle se extiende ante ti como una serpiente de adoquines desafinados, retorcida y enredada en sí misma como un solo de saxofón mal tocado. Los edificios, con sus fachadas gastadas y ventanas oscurecidas, parecen susurrar historias de tiempos pasados, cuando el jazz fluía como el bourbon en un bar de mala muerte. <br/><br/> Cerca, una cabina telefónica está ocupada, lo cual es una rareza en esta calle desolada. Una tenue luz brilla desde su interior, filtrándose por las grietas entre las puertas. <br/><br/> El aire está cargado de una extraña mezcla de nostalgia y desesperación, como si cada nota que emanaba de tu saxofón desafinado fuera un recordatorio de los días de gloria perdidos. A lo lejos, las farolas parpadeaban como luciérnagas borrachas, guiándote hacia los callejones oscuros donde los secretos de Swingville se escondían entre las sombras.";
locations[22] = "{class|loc|Localitat 22}\n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud {CLASS|hidden|exercitation ullamco} laboris nisi ut {class|hidden|aliquip} ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.";

// CONNECTIONS

connections = [];
connections_start = [];

connections[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[1] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[2] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[3] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[4] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[5] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[6] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[7] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[8] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[9] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[10] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[11] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[12] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[13] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[14] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[16] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[17] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[18] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[19] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[20] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[21] = [ -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[22] = [ -1, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];

connections_start[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[1] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[2] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[3] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[4] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[5] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[6] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[7] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[8] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[9] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[10] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[11] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[12] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[13] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[14] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[16] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[17] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[18] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[19] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[20] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[21] = [ -1, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[22] = [ -1, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];


resources=[];
resources.push([RESOURCE_TYPE_IMG, 21, "dat/loc11.jpg"]);


 //OBJECTS

objects = [];
objectsAttrLO = [];
objectsAttrHI = [];
objectsLocation = [];
objectsNoun = [];
objectsAdjective = [];
objectsWeight = [];
objectsAttrLO_start = [];
objectsAttrHI_start = [];
objectsLocation_start = [];
objectsWeight_start = [];

objects[0] = "un objeto";
objectsNoun[0] = 50;
objectsAdjective[0] = 255;
objectsLocation[0] = 21;
objectsLocation_start[0] = 21;
objectsWeight[0] = 20;
objectsWeight_start[0] = 20;
objectsAttrLO[0] = 2048;
objectsAttrLO_start[0] = 2048;
objectsAttrHI[0] = 0;
objectsAttrHI_start[0] = 0;

objects[1] = "una cosa";
objectsNoun[1] = 51;
objectsAdjective[1] = 255;
objectsLocation[1] = 254;
objectsLocation_start[1] = 254;
objectsWeight[1] = 2;
objectsWeight_start[1] = 2;
objectsAttrLO[1] = 258;
objectsAttrLO_start[1] = 258;
objectsAttrHI[1] = 0;
objectsAttrHI_start[1] = 0;

last_object_number =  1; 
carried_objects = 1;
total_object_messages=2;

